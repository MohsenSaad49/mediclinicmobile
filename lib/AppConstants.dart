import 'dart:ui';

class AppConstants {
  static final topPadding = window.viewPadding.top > 70 ? window.viewPadding.top : window.viewPadding.top + 20; //70.0;
  static final altTopPadding = window.viewPadding.top > 70 ? window.viewPadding.top + 20:window.viewPadding.top + 40;
  static final homePagePadding = window.viewPadding.top > 70 ? 30.0 : 0.0;
  static final callNotify = window.viewPadding.top > 70 ? 60.0 : 40.0;
  static final pageLimit = 10;
}