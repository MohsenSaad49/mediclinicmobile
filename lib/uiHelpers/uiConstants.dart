import 'package:flutter/material.dart';
import 'colorConstants.dart';

class UiConstants {
  static final defaultBoxDecoration = BoxDecoration(
    color: ColorConstants.defaultBoxColor,
    border: Border.all(
      color: Colors.transparent,
      width: 0,
    ),
    borderRadius: BorderRadius.circular(10),
  );

  static final String segeoRegularFont = 'segoeRegular';
  static final String segeoLightFont = 'segoeUiLight';
  static final String segeoSemiBoldFont = 'segoeUiSemiBold';

  static final TextStyle hintTextStyle = TextStyle(
      fontFamily: UiConstants.segeoLightFont,
      color: ColorConstants.hintStyleColor,
      fontSize: 16);

  static final heroBoxDecoration = BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.topRight,
      stops: [0, 0.6],
      colors: [
        ColorConstants.gradientBoxColorStart,
        ColorConstants.gradientBoxColorStop,
      ],
    ),
  );

  static final mainBoxDecoration = BoxDecoration(
    borderRadius: new BorderRadius.only(
      topLeft: Radius.circular(25),
      topRight: Radius.circular(25),
    ),
    color: Colors.white,
  );

  static final whiteContainerWithShadowDecoration = BoxDecoration(
      borderRadius: new BorderRadius.only(
        topLeft: Radius.circular(25),
        topRight: Radius.circular(25),
      ),
      boxShadow: [
        new BoxShadow(
          color: Colors.black.withOpacity(0.2),
          blurRadius: 5.0,
        ),
      ],
      color: Colors.white);

  static final defaultInputDecorations = InputDecorationTheme(
    filled: true,
    fillColor: ColorConstants.defaultBoxColor,
    border: InputBorder.none,
    hintStyle: TextStyle(color: Color(0xFFCCCCCC), fontStyle: FontStyle.normal),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      borderSide: const BorderSide(
        color: Colors.transparent,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      borderSide: const BorderSide(
        color: Colors.transparent,
      ),
    ),
  );

  static final buttonThemeData = ButtonThemeData(
      padding: EdgeInsets.fromLTRB(40, 15, 40, 15),
      buttonColor: Color(0XFF861DFF),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5.0),
      ),
      textTheme: ButtonTextTheme.primary);

  static final defaultTextThemeData = TextTheme(
    button: TextStyle(color: Colors.white),
    headline: TextStyle(
        color: ColorConstants.primaryColor,
        fontSize: 20,
        fontStyle: FontStyle.normal),
    title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    body1: TextStyle(
        color: Colors.black, fontSize: 15, fontStyle: FontStyle.normal),
    caption: TextStyle(
        color: Colors.black, fontSize: 15, fontStyle: FontStyle.normal),
  );
}
