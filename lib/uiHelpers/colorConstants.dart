import 'package:flutter/material.dart';


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class ColorConstants {
  static final Map<int, Color> color = {
    50: Color.fromRGBO(134, 29, 255, .1),
    100: Color.fromRGBO(134, 29, 255, .2),
    200: Color.fromRGBO(134, 29, 255, .3),
    300: Color.fromRGBO(134, 29, 255, .4),
    400: Color.fromRGBO(134, 29, 255, .5),
    500: Color.fromRGBO(134, 29, 255, .6),
    600: Color.fromRGBO(134, 29, 255, .7),
    700: Color.fromRGBO(134, 29, 255, .8),
    800: Color.fromRGBO(134, 29, 255, .9),
    900: Color.fromRGBO(134, 29, 255, 1),
  };


  static final primarySwatch = MaterialColor(0xFF861DFF, color);
  static final primaryColor = Color(0xFF6900e0);
  static final lightPrimaryColor = Color(0xFFA9ACFF);
  static final Color grayColor =Colors.grey;
  static final Color gray80color = HexColor("#F9F9F9");
  static final Color graylightcolor = HexColor("#CCCCCC");
  static final Color purpleColor = HexColor("#861DFF");
  static final Color lightgray = HexColor("#F9F9F9");
  static final Color mainTextColor = HexColor("#6900e0");
  static final Color hintStyleColor = HexColor("#CCCCCC");
  static final Color inputBackgroundColor = HexColor("#F9F9F9");
  static final defaultBoxColor = Color(0xFFF9F9F9);
  static final gradientBoxColorStart = Color(0xFFFA2FDD);
  static final gradientBoxColorStop = Color(0xFF6679F3);
  static final darkGrayColor = Color(0xFFCCCCCC);
}
