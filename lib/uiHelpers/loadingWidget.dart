import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/loading.dart';
import 'package:mediclinic/uiHelpers/colorConstants.dart';

class LoadingWidget extends StatelessWidget {
  String Loadingtext;
  LoadingWidget({this.Loadingtext = ""}) : super();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Loading(
            indicator: BallBeatIndicator(),
            size: 50.0,
            color: ColorConstants.primaryColor),
        margin: EdgeInsets.only(bottom: 0, left: 0, right: 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
