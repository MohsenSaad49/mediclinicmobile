import 'package:flutter/material.dart';

import 'colorConstants.dart';
import 'uiConstants.dart';

class AppThemeData {
  static ThemeData get defaultTheme {
    return ThemeData(
        primarySwatch: ColorConstants.primarySwatch,
        textTheme: UiConstants.defaultTextThemeData,
        buttonTheme: UiConstants.buttonThemeData,
        inputDecorationTheme: UiConstants.defaultInputDecorations);
  }
}
