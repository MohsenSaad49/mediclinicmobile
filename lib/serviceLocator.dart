import 'package:get_it/get_it.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';
import 'package:mediclinic/services/appServices/dialogService.dart';
import 'package:mediclinic/services/appServices/loadingService.dart';
import 'package:mediclinic/services/appServices/navigationService.dart';
import 'package:mediclinic/services/appServices/networkService.dart';
import 'package:mediclinic/services/appServices/settingsService.dart';

GetIt locator = GetIt.I;
bool isSetup = false;
void setupLocator() {
  if (isSetup) return;
  isSetup = true;
  //App Services
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => LoadingService());
  locator.registerLazySingleton(() => NetworkService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => SettingsService());


  //API Services
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => PatientService());

}