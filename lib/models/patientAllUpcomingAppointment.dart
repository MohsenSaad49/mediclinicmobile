import '../LogicUtilities.dart';

class PatientAllUpcomingAppointment {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  int requestId;
  bool isSuccess;
  List<UpcomingBooking> data;

  PatientAllUpcomingAppointment(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientAllUpcomingAppointment.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    if (json['data'] != null) {
      data = new List<UpcomingBooking>();
      json['data'].forEach((v) {
        data.add(new UpcomingBooking.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UpcomingBooking {
  int bookingId;
  String bookingDateStart;
  String bookingDateEnd;
  int providerId;
  String providerFirstName;
  String providerSurname;
  int clinicId;
  String clinicName;
  int bookingStatusId;
  String bookingStatus;
  String clinicBusinessAddress1;
  String clinicBusinessAddress2;
  double clinicLongitude;
  double clinicLatitude;
  bool isRecurringBooking;
  int recurringWeekdayId;
  String serviceName;
  String serviceShortName;
  String serviceDescription;
  String patientFirstName;
  String patientSurname;
  String providerFullName() => "$providerFirstName $providerSurname";
  String patientFullName() => "$patientFirstName $patientSurname";

  UpcomingBooking(
      {this.bookingId,
        this.bookingDateStart,
        this.bookingDateEnd,
        this.providerId,
        this.providerFirstName,
        this.providerSurname,
        this.clinicId,
        this.clinicName,
        this.bookingStatusId,
        this.bookingStatus,
        this.clinicBusinessAddress1,
        this.clinicBusinessAddress2,
        this.clinicLongitude,
        this.clinicLatitude,
        this.isRecurringBooking,
        this.recurringWeekdayId,
        this.serviceName,
        this.serviceShortName,
        this.serviceDescription,
        this.patientFirstName,
        this.patientSurname});

  UpcomingBooking.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    bookingDateStart = json['bookingDateStart']==null?"":LogicUtilities.dateToStringFomatted(json['bookingDateStart']);
    bookingDateEnd = json['bookingDateEnd']==null?"":LogicUtilities.dateToStringFomatted(json['bookingDateEnd']);
    providerId = json['providerId'];
    providerFirstName = LogicUtilities.ensureString(json['providerFirstName']);
    providerSurname = json['providerSurname'];
    clinicId = json['clinicId'];
    clinicName = json['clinicName'];
    bookingStatusId = json['bookingStatusId'];
    bookingStatus = json['bookingStatus'];
    clinicBusinessAddress1 = json['clinicBusinessAddress1'];
    clinicBusinessAddress2 = json['clinicBusinessAddress2'];
    clinicLongitude = json['clinicLongitude'];
    clinicLatitude = json['clinicLatitude'];
    isRecurringBooking = json['isRecurringBooking'];
    recurringWeekdayId = json['recurringWeekdayId'];
    serviceName = LogicUtilities.ensureString(json['serviceName']);
    serviceShortName = LogicUtilities.ensureString(json['serviceShortName']);
    serviceDescription = LogicUtilities.ensureString(json['serviceDescription']);
    patientFirstName = LogicUtilities.ensureString(json['patientFirstName']);
    patientSurname = LogicUtilities.ensureString(json['patientSurname']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['bookingDateStart'] = this.bookingDateStart;
    data['bookingDateEnd'] = this.bookingDateEnd;
    data['providerId'] = this.providerId;
    data['providerFirstName'] = this.providerFirstName;
    data['providerSurname'] = this.providerSurname;
    data['clinicId'] = this.clinicId;
    data['clinicName'] = this.clinicName;
    data['bookingStatusId'] = this.bookingStatusId;
    data['bookingStatus'] = this.bookingStatus;
    data['clinicBusinessAddress1'] = this.clinicBusinessAddress1;
    data['clinicBusinessAddress2'] = this.clinicBusinessAddress2;
    data['clinicLongitude'] = this.clinicLongitude;
    data['clinicLatitude'] = this.clinicLatitude;
    data['isRecurringBooking'] = this.isRecurringBooking;
    data['recurringWeekdayId'] = this.recurringWeekdayId;
    data['serviceName'] = this.serviceName;
    data['serviceShortName'] = this.serviceShortName;
    data['serviceDescription'] = this.serviceDescription;
    data['patientFirstName'] = this.patientFirstName;
    data['patientSurname'] = this.patientSurname;
    return data;
  }
}
