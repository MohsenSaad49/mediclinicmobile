import '../LogicUtilities.dart';

class PatientUpcomingBooking {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  String requestId;
  bool isSuccess;
  Booking data;


  PatientUpcomingBooking(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientUpcomingBooking.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    data = json['data'] != null ? new Booking.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Booking {
  int bookingId;
  String bookingDateStart;
  String bookingDateEnd;
  int providerId;
  String providerFirstName;
  String providerSurname;
  int clinicId;
  String clinicName;
  int bookingStatusId;
  String bookingStatus;
  String clinicBusinessAddress1;
  String clinicBusinessAddress2;
  double clinicLongitude;
  double clinicLatitude;
  bool isRecurringBooking;
  int recurringWeekdayId;
  String serviceName;
  String serviceShortName;
  String serviceDescription;
  String patientFirstName;
  String patientSurname;
  String fullName() => "$providerFirstName $providerSurname";

  Booking(
      {this.bookingId,
        this.bookingDateStart,
        this.bookingDateEnd,
        this.providerId,
        this.providerFirstName,
        this.providerSurname,
        this.clinicId,
        this.clinicName,
        this.bookingStatusId,
        this.bookingStatus,
        this.clinicBusinessAddress1,
        this.clinicBusinessAddress2,
        this.clinicLongitude,
        this.clinicLatitude,
        this.isRecurringBooking,
        this.recurringWeekdayId,
        this.serviceName,
        this.serviceShortName,
        this.serviceDescription,
        this.patientFirstName,
        this.patientSurname});

  Booking.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    bookingDateStart = json['bookingDateStart']==null?"":LogicUtilities.dateToString(json['bookingDateStart']);
    bookingDateEnd = json['bookingDateEnd'];
    providerId = json['providerId'];
    providerFirstName = json['providerFirstName'];
    providerSurname = json['providerSurname'];
    clinicId = json['clinicId'];
    clinicName = json['clinicName']==null?"clinic name":json['clinicName'];
    bookingStatusId = json['bookingStatusId'];
    bookingStatus = json['bookingStatus'];
    clinicBusinessAddress1 = json['clinicBusinessAddress1']==null?"50 Walder Crescent GOOVIGEN QLD 4702":json['clinicBusinessAddress1'];
    clinicBusinessAddress2 = json['clinicBusinessAddress2']==null?"50 Walder Crescent GOOVIGEN QLD 4702":json['clinicBusinessAddress2'];
    clinicLongitude = json['clinicLongitude'];
    clinicLatitude = json['clinicLatitude'];
    isRecurringBooking = json['isRecurringBooking'];
    recurringWeekdayId = json['recurringWeekdayId'];
    serviceName = json['serviceName'];
    serviceShortName = json['serviceShortName'];
    serviceDescription = json['serviceDescription'];
    patientFirstName = json['patientFirstName'];
    patientSurname = json['patientSurname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['bookingDateStart'] = this.bookingDateStart;
    data['bookingDateEnd'] = this.bookingDateEnd;
    data['providerId'] = this.providerId;
    data['providerFirstName'] = this.providerFirstName;
    data['providerSurname'] = this.providerSurname;
    data['clinicId'] = this.clinicId;
    data['clinicName'] = this.clinicName;
    data['bookingStatusId'] = this.bookingStatusId;
    data['bookingStatus'] = this.bookingStatus;
    data['clinicBusinessAddress1'] = this.clinicBusinessAddress1;
    data['clinicBusinessAddress2'] = this.clinicBusinessAddress2;
    data['clinicLongitude'] = this.clinicLongitude;
    data['clinicLatitude'] = this.clinicLatitude;
    data['isRecurringBooking'] = this.isRecurringBooking;
    data['recurringWeekdayId'] = this.recurringWeekdayId;
    data['serviceName'] = this.serviceName;
    data['serviceShortName'] = this.serviceShortName;
    data['serviceDescription'] = this.serviceDescription;
    data['patientFirstName'] = this.patientFirstName;
    data['patientSurname'] = this.patientSurname;
    return data;
  }
}
