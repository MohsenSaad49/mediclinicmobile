import 'package:mediclinic/LogicUtilities.dart';

class PatientInvoices {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  String requestId;
  bool isSuccess;
    List<PatientInvoice> data;

  PatientInvoices(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientInvoices.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    if (json['data'] != null) {
      data = new List<PatientInvoice>();
      json['data'].forEach((v) {
        data.add(new PatientInvoice.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PatientInvoice {
  int invoiceId;
  int entityId;
  int invoiceTypeId;
  int bookingId;
  int payerOrganisationId;
  int payerPatientId;
  int nonBookingInvoiceOrganisationId;
  int healthcareClaimNumber;
  int rejectLetterId;
  String message;
  int staffId;
  int siteId;
  String invoiceDateAdded;
  double total;
  double gst;
  bool isPaid;
  bool isRefund;
  bool isBatched;
  String reversedBy;
  String reversedDate;
  String lastDateEmailed;
  bool medicareSendRequired;
  String medicareSentDate;
  String freeText;
  int nonBookingInvoiceProviderId;
  String addedByCcName;
  double preDiscountPrice;
  double discountAmount;
  double discountPercent;
  String discountReason;
  int patientId;
  String patientFirstName;
  String patientSurname;
  bool isPaidByCreditCard;
  int creditCardNumber;
  String bookingDate;
  List<InvoiceLines> invoiceLines;

  String patientFullName() => "$patientFirstName $patientSurname";

  PatientInvoice(
      {this.invoiceId,
        this.entityId,
        this.invoiceTypeId,
        this.bookingId,
        this.payerOrganisationId,
        this.payerPatientId,
        this.nonBookingInvoiceOrganisationId,
        this.healthcareClaimNumber,
        this.rejectLetterId,
        this.message,
        this.staffId,
        this.siteId,
        this.invoiceDateAdded,
        this.total,
        this.gst,
        this.isPaid,
        this.isRefund,
        this.isBatched,
        this.reversedBy,
        this.reversedDate,
        this.lastDateEmailed,
        this.medicareSendRequired,
        this.medicareSentDate,
        this.freeText,
        this.nonBookingInvoiceProviderId,
        this.addedByCcName,
        this.preDiscountPrice,
        this.discountAmount,
        this.discountPercent,
        this.discountReason,
        this.patientId,
        this.patientFirstName,
        this.patientSurname,
        this.isPaidByCreditCard,
        this.creditCardNumber,
        this.bookingDate,
        this.invoiceLines});

  PatientInvoice.fromJson(Map<String, dynamic> json) {
    invoiceId = json['invoiceId'];
    entityId = json['entityId'];
    invoiceTypeId = json['invoiceTypeId'];
    bookingId = json['bookingId'];
    payerOrganisationId = json['payerOrganisationId'];
    payerPatientId = json['payerPatientId'];
    nonBookingInvoiceOrganisationId = json['nonBookingInvoiceOrganisationId'];
    healthcareClaimNumber = json['healthcareClaimNumber'];
    rejectLetterId = json['rejectLetterId'];
    message = json['message'];
    staffId = json['staffId'];
    siteId = json['siteId'];
    invoiceDateAdded = json['invoiceDateAdded']==null?"":LogicUtilities.dateToStringForInvoice(json['invoiceDateAdded']);
    total = json['total'];
    gst = json['gst'];
    isPaid = json['isPaid'];
    isRefund = json['isRefund'];
    isBatched = json['isBatched'];
    reversedBy = json['reversedBy'];
    reversedDate = json['reversedDate'];
    lastDateEmailed = json['lastDateEmailed']==null?"":LogicUtilities.dateToStringForInvoice(json['lastDateEmailed']);
    medicareSendRequired = json['medicareSendRequired'];
    medicareSentDate = json['medicareSentDate'];
    freeText = json['freeText'];
    nonBookingInvoiceProviderId = json['nonBookingInvoiceProviderId'];
    addedByCcName = json['addedByCcName'];
    preDiscountPrice = json['preDiscountPrice'];
    discountAmount = json['discountAmount'];
    discountPercent = json['discountPercent'];
    discountReason = json['discountReason'];
    patientId = json['patientId'];
    patientFirstName = json['patientFirstName'];
    patientSurname = json['patientSurname'];
    isPaidByCreditCard = json['isPaidByCreditCard'];
    creditCardNumber = json['creditCardNumber'];
    bookingDate = json['bookingDate']==null?"":LogicUtilities.dateToStringFomatted(json['bookingDate']);
    if (json['invoiceLines'] != null) {
      invoiceLines = new List<InvoiceLines>();
      json['invoiceLines'].forEach((v) {
        invoiceLines.add(new InvoiceLines.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['invoiceId'] = this.invoiceId;
    data['entityId'] = this.entityId;
    data['invoiceTypeId'] = this.invoiceTypeId;
    data['bookingId'] = this.bookingId;
    data['payerOrganisationId'] = this.payerOrganisationId;
    data['payerPatientId'] = this.payerPatientId;
    data['nonBookingInvoiceOrganisationId'] =
        this.nonBookingInvoiceOrganisationId;
    data['healthcareClaimNumber'] = this.healthcareClaimNumber;
    data['rejectLetterId'] = this.rejectLetterId;
    data['message'] = this.message;
    data['staffId'] = this.staffId;
    data['siteId'] = this.siteId;
    data['invoiceDateAdded'] = this.invoiceDateAdded;
    data['total'] = this.total;
    data['gst'] = this.gst;
    data['isPaid'] = this.isPaid;
    data['isRefund'] = this.isRefund;
    data['isBatched'] = this.isBatched;
    data['reversedBy'] = this.reversedBy;
    data['reversedDate'] = this.reversedDate;
    data['lastDateEmailed'] = this.lastDateEmailed;
    data['medicareSendRequired'] = this.medicareSendRequired;
    data['medicareSentDate'] = this.medicareSentDate;
    data['freeText'] = this.freeText;
    data['nonBookingInvoiceProviderId'] = this.nonBookingInvoiceProviderId;
    data['addedByCcName'] = this.addedByCcName;
    data['preDiscountPrice'] = this.preDiscountPrice;
    data['discountAmount'] = this.discountAmount;
    data['discountPercent'] = this.discountPercent;
    data['discountReason'] = this.discountReason;
    data['patientId'] = this.patientId;
    data['patientFirstName'] = this.patientFirstName;
    data['patientSurname'] = this.patientSurname;
    data['isPaidByCreditCard'] = this.isPaidByCreditCard;
    data['creditCardNumber'] = this.creditCardNumber;
    data['bookingDate'] = this.bookingDate;
    if (this.invoiceLines != null) {
      data['invoiceLines'] = this.invoiceLines.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InvoiceLines {
  int invoiceLineId;
  int invoiceId;
  int patientId;
  int serviceId;
  String serviceName;
  String serviceShortName;
  String serviceDescription;
  double quantity;
  double price;
  double tax;
  String areaTreated;
  int offeringOrderId;
  String serviceReference;
  int creditId;
  double medicarePrice;
  int referralRemainingId;

  InvoiceLines(
      {this.invoiceLineId,
        this.invoiceId,
        this.patientId,
        this.serviceId,
        this.serviceName,
        this.serviceShortName,
        this.serviceDescription,
        this.quantity,
        this.price,
        this.tax,
        this.areaTreated,
        this.offeringOrderId,
        this.serviceReference,
        this.creditId,
        this.medicarePrice,
        this.referralRemainingId});

  InvoiceLines.fromJson(Map<String, dynamic> json) {
    invoiceLineId = json['invoiceLineId'];
    invoiceId = json['invoiceId'];
    patientId = json['patientId'];
    serviceId = json['serviceId'];
    serviceName = json['serviceName'];
    serviceShortName = json['serviceShortName'];
    serviceDescription = json['serviceDescription'];
    quantity = LogicUtilities.ensureDouble( json['quantity']);
    price = LogicUtilities.ensureDouble( json['price']);
    tax = LogicUtilities.ensureDouble( json['tax']);
    areaTreated = json['areaTreated'];
    offeringOrderId = json['offeringOrderId'];
    serviceReference = json['serviceReference'];
    creditId = json['creditId'];
    medicarePrice = json['medicarePrice'];
    referralRemainingId = json['referralRemainingId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['invoiceLineId'] = this.invoiceLineId;
    data['invoiceId'] = this.invoiceId;
    data['patientId'] = this.patientId;
    data['serviceId'] = this.serviceId;
    data['serviceName'] = this.serviceName;
    data['serviceShortName'] = this.serviceShortName;
    data['serviceDescription'] = this.serviceDescription;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['tax'] = this.tax;
    data['areaTreated'] = this.areaTreated;
    data['offeringOrderId'] = this.offeringOrderId;
    data['serviceReference'] = this.serviceReference;
    data['creditId'] = this.creditId;
    data['medicarePrice'] = this.medicarePrice;
    data['referralRemainingId'] = this.referralRemainingId;
    return data;
  }
}
