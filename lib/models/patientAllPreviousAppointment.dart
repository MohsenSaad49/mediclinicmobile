import 'package:mediclinic/models/patientAllUpcomingAppointment.dart';

class PatientAllPreviousAppointment {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  int requestId;
  bool isSuccess;
  List<UpcomingBooking> data;

  PatientAllPreviousAppointment(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientAllPreviousAppointment.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    if (json['data'] != null) {
      data = new List<UpcomingBooking>();
      json['data'].forEach((v) {
        data.add(new UpcomingBooking.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


