class PatientLoginRequest {

  String firstname;
  String surname;
  String dob;
  String mobile_phone;
  String home_phone;
  String email;
  String device_id;

  PatientLoginRequest({this.firstname, this.surname,this.dob,this.mobile_phone,this.home_phone,this.email,this.device_id});

  PatientLoginRequest.fromJson(Map<String, dynamic> json) {

    firstname = json['firstname'];
    device_id = json['device_id'];
    surname = json['surname'];
    dob = json['dob'];
    mobile_phone = json['mobile_phone'];
    home_phone = json['home_phone'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname ;
    data['surname'] = this.surname ;
    data['dob'] = this.dob ;
    data['mobile_phone'] = this.mobile_phone ;
    data['device_id'] = this.device_id ;
    data['home_phone'] = this.home_phone ;
    data['email'] = this.email ;
    return data;
  }
}