class PatientClinicsModel {
  String errorMessage;
  String exception;
  List<Clinic> clinics;

  PatientClinicsModel({this.errorMessage, this.exception, this.clinics});

  PatientClinicsModel.fromJson(Map<String, dynamic> json) {
    errorMessage = json['ErrorMessage'];
    exception = json['Exception'];
    if (json['Clinics'] != null) {
      clinics = new List<Clinic>();
      json['Clinics'].forEach((v) {
        clinics.add(new Clinic.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessage'] = this.errorMessage;
    data['Exception'] = this.exception;
    if (this.clinics != null) {
      data['Clinics'] = this.clinics.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Clinic {
  int iD;
  String name;

  Clinic({this.iD, this.name});

  Clinic.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['Name'] = this.name;
    return data;
  }
}