class AuthModel {
  String errorMessage;
  String exception;
  List<PatientSites> patientSites;

  AuthModel({this.errorMessage, this.exception, this.patientSites});

  AuthModel.fromJson(Map<String, dynamic> json) {
    errorMessage = json['ErrorMessage'];
    exception = json['Exception'];
    if (json['PatientSites'] != null) {
      patientSites = new List<PatientSites>();
      json['PatientSites'].forEach((v) {
        patientSites.add(new PatientSites.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessage'] = this.errorMessage;
    data['Exception'] = this.exception;
    if (this.patientSites != null) {
      data['PatientSites'] = this.patientSites.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PatientSites {
  String iD;
  String siteName;
  bool isSiteMultiTimezone;
  List<Clinics> clinics;

  PatientSites(
      {this.iD, this.siteName, this.isSiteMultiTimezone, this.clinics});

  PatientSites.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    siteName = json['SiteName'];
    isSiteMultiTimezone = json['IsSiteMultiTimezone'];
    if (json['Clinics'] != null) {
      clinics = new List<Clinics>();
      json['Clinics'].forEach((v) {
        clinics.add(new Clinics.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['SiteName'] = this.siteName;
    data['IsSiteMultiTimezone'] = this.isSiteMultiTimezone;
    if (this.clinics != null) {
      data['Clinics'] = this.clinics.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Clinics {
  int iD;
  String name;

  Clinics({this.iD, this.name});

  Clinics.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['Name'] = this.name;
    return data;
  }
}