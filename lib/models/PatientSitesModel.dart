class PatientSitesModel {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  String requestId;
  bool isSuccess;
  List<SiteModel> data;

  PatientSitesModel(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientSitesModel.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    if (json['data'] != null) {
      data = new List<SiteModel>();
      json['data'].forEach((v) {
        data.add(new SiteModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SiteModel {
  int siteId;
  int entityId;
  String name;
  String abn;
  String acn;
  String tfn;
  String asic;
  bool isProvider;
  String bankPay;
  String bankBsb;
  String bankAccount;
  String bankDirectDebitUserid;
  String bankUsername;
  double outstandingBalanceWarning;
  bool printEpc;
  bool exclSun;
  bool exclMon;
  bool exclTue;
  bool exclWed;
  bool exclThu;
  bool exclFri;
  bool exclSat;
  String dayStartTime;
  String lunchStartTime;
  String lunchEndTime;
  String dayEndTime;
  String fiscalYrEnd;
  int numBookingMonthsToGet;
  int siteTypeId;
  String siteFriendlyName;
  String dbName;
  String webUrl;
  String phone;
  String email;

  SiteModel(
      {this.siteId,
        this.entityId,
        this.name,
        this.abn,
        this.acn,
        this.tfn,
        this.asic,
        this.isProvider,
        this.bankPay,
        this.bankBsb,
        this.bankAccount,
        this.bankDirectDebitUserid,
        this.bankUsername,
        this.outstandingBalanceWarning,
        this.printEpc,
        this.exclSun,
        this.exclMon,
        this.exclTue,
        this.exclWed,
        this.exclThu,
        this.exclFri,
        this.exclSat,
        this.dayStartTime,
        this.lunchStartTime,
        this.lunchEndTime,
        this.dayEndTime,
        this.fiscalYrEnd,
        this.numBookingMonthsToGet,
        this.siteTypeId,
        this.siteFriendlyName,
        this.dbName,
        this.webUrl,
        this.phone,
        this.email});

  SiteModel.fromJson(Map<String, dynamic> json) {
    siteId = json['siteId'];
    entityId = json['entityId'];
    name = json['name']==null?"Site Name":json['name'];
    abn = json['abn'];
    acn = json['acn'];
    tfn = json['tfn'];
    asic = json['asic'];
    isProvider = json['isProvider'];
    bankPay = json['bankPay'];
    bankBsb = json['bankBsb'];
    bankAccount = json['bankAccount'];
    bankDirectDebitUserid = json['bankDirectDebitUserid'];
    bankUsername = json['bankUsername'];
    outstandingBalanceWarning = json['outstandingBalanceWarning'];
    printEpc = json['printEpc'];
    exclSun = json['exclSun'];
    exclMon = json['exclMon'];
    exclTue = json['exclTue'];
    exclWed = json['exclWed'];
    exclThu = json['exclThu'];
    exclFri = json['exclFri'];
    exclSat = json['exclSat'];
    dayStartTime = json['dayStartTime'];
    lunchStartTime = json['lunchStartTime'];
    lunchEndTime = json['lunchEndTime'];
    dayEndTime = json['dayEndTime'];
    fiscalYrEnd = json['fiscalYrEnd'];
    numBookingMonthsToGet = json['numBookingMonthsToGet'];
    siteTypeId = json['siteTypeId'];
    siteFriendlyName = json['siteFriendlyName']==null?"Site Friendly name":json['siteFriendlyName'];
    dbName = json['dbName'];
    webUrl = json['webUrl']==null?"https:\\mediclinic.com":json['webUrl'];
    phone = json['phone']==null?"09258467485":json['phone'];
    email = json['email']==null?"mail@mediclinic.com":json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['siteId'] = this.siteId;
    data['entityId'] = this.entityId;
    data['name'] = this.name;
    data['abn'] = this.abn;
    data['acn'] = this.acn;
    data['tfn'] = this.tfn;
    data['asic'] = this.asic;
    data['isProvider'] = this.isProvider;
    data['bankPay'] = this.bankPay;
    data['bankBsb'] = this.bankBsb;
    data['bankAccount'] = this.bankAccount;
    data['bankDirectDebitUserid'] = this.bankDirectDebitUserid;
    data['bankUsername'] = this.bankUsername;
    data['outstandingBalanceWarning'] = this.outstandingBalanceWarning;
    data['printEpc'] = this.printEpc;
    data['exclSun'] = this.exclSun;
    data['exclMon'] = this.exclMon;
    data['exclTue'] = this.exclTue;
    data['exclWed'] = this.exclWed;
    data['exclThu'] = this.exclThu;
    data['exclFri'] = this.exclFri;
    data['exclSat'] = this.exclSat;
    data['dayStartTime'] = this.dayStartTime;
    data['lunchStartTime'] = this.lunchStartTime;
    data['lunchEndTime'] = this.lunchEndTime;
    data['dayEndTime'] = this.dayEndTime;
    data['fiscalYrEnd'] = this.fiscalYrEnd;
    data['numBookingMonthsToGet'] = this.numBookingMonthsToGet;
    data['siteTypeId'] = this.siteTypeId;
    data['siteFriendlyName'] = this.siteFriendlyName;
    data['dbName'] = this.dbName;
    data['webUrl'] = this.webUrl;
    data['phone'] = this.phone;
    data['email'] = this.email;
    return data;
  }
}
