class PatientContacts {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  int requestId;
  bool isSuccess;
  List<PatientClinic> data;

  PatientContacts(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientContacts.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    if (json['data'] != null) {
      data = new List<PatientClinic>();
      json['data'].forEach((v) {
        data.add(new PatientClinic.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PatientClinic {
  int registerPatientId;
  int organisationId;
  int organisationEntityId;
  int patientId;
  String registerPatientDateAdded;
  String clinicName;
  Null parentName;
  int typeOrganisationTypeId;
  Null typeDescription;
  Null organisationTypeGroupId;
  int typeGroupOrganisationTypeGroupId;
  Null typeGroupDescription;
  int ctOrganisationCustomerTypeId;
  Null ctDescription;
  String addressLine1;
  String addressLine2;
  String street;
  String subUrb;
  String postCode;
  String state;
  String phone;
  String webUrl;
  String email;
  String fax;
  double longitude;
  double latitude;
  Null contactType;

  PatientClinic(
      {this.registerPatientId,
        this.organisationId,
        this.organisationEntityId,
        this.patientId,
        this.registerPatientDateAdded,
        this.clinicName,
        this.parentName,
        this.typeOrganisationTypeId,
        this.typeDescription,
        this.organisationTypeGroupId,
        this.typeGroupOrganisationTypeGroupId,
        this.typeGroupDescription,
        this.ctOrganisationCustomerTypeId,
        this.ctDescription,
        this.addressLine1,
        this.addressLine2,
        this.street,
        this.subUrb,
        this.postCode,
        this.state,
        this.phone,
        this.webUrl,
        this.email,
        this.fax,
        this.longitude,
        this.latitude,
        this.contactType});

  PatientClinic.fromJson(Map<String, dynamic> json) {
    registerPatientId = json['registerPatientId'];
    organisationId = json['organisationId'];
    organisationEntityId = json['organisationEntityId'];
    patientId = json['patientId'];
    registerPatientDateAdded = json['registerPatientDateAdded'];
    clinicName = json['clinicName'];
    parentName = json['parentName'];
    typeOrganisationTypeId = json['typeOrganisationTypeId'];
    typeDescription = json['typeDescription'];
    organisationTypeGroupId = json['organisationTypeGroupId'];
    typeGroupOrganisationTypeGroupId = json['typeGroupOrganisationTypeGroupId'];
    typeGroupDescription = json['typeGroupDescription'];
    ctOrganisationCustomerTypeId = json['ctOrganisationCustomerTypeId'];
    ctDescription = json['ctDescription'];
    addressLine1 = json['addressLine1'];
    addressLine2 = json['addressLine2'];
    street = json['street'];
    subUrb = json['subUrb'];
    postCode = json['postCode'];
    state = json['state'];
    phone = json['phone'];
    webUrl = json['webUrl'];
    email = json['email'];
    fax = json['fax'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    contactType = json['contactType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registerPatientId'] = this.registerPatientId;
    data['organisationId'] = this.organisationId;
    data['organisationEntityId'] = this.organisationEntityId;
    data['patientId'] = this.patientId;
    data['registerPatientDateAdded'] = this.registerPatientDateAdded;
    data['clinicName'] = this.clinicName;
    data['parentName'] = this.parentName;
    data['typeOrganisationTypeId'] = this.typeOrganisationTypeId;
    data['typeDescription'] = this.typeDescription;
    data['organisationTypeGroupId'] = this.organisationTypeGroupId;
    data['typeGroupOrganisationTypeGroupId'] =
        this.typeGroupOrganisationTypeGroupId;
    data['typeGroupDescription'] = this.typeGroupDescription;
    data['ctOrganisationCustomerTypeId'] = this.ctOrganisationCustomerTypeId;
    data['ctDescription'] = this.ctDescription;
    data['addressLine1'] = this.addressLine1;
    data['addressLine2'] = this.addressLine2;
    data['street'] = this.street;
    data['subUrb'] = this.subUrb;
    data['postCode'] = this.postCode;
    data['state'] = this.state;
    data['phone'] = this.phone;
    data['webUrl'] = this.webUrl;
    data['email'] = this.email;
    data['fax'] = this.fax;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['contactType'] = this.contactType;
    return data;
  }
}
