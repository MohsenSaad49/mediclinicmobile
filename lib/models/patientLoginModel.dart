class PatientLoginModel {
  String errorMessage;
  String errorMessageArabic;
  String exception;
  String path;
  String requestId;
  bool isSuccess;
  Data data;

  PatientLoginModel(
      {this.errorMessage,
        this.errorMessageArabic,
        this.exception,
        this.path,
        this.requestId,
        this.isSuccess,
        this.data});

  PatientLoginModel.fromJson(Map<String, dynamic> json) {
    errorMessage = json['errorMessage'];
    errorMessageArabic = json['errorMessageArabic'];
    exception = json['exception'];
    path = json['path'];
    requestId = json['requestId'];
    isSuccess = json['isSuccess'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorMessage'] = this.errorMessage;
    data['errorMessageArabic'] = this.errorMessageArabic;
    data['exception'] = this.exception;
    data['path'] = this.path;
    data['requestId'] = this.requestId;
    data['isSuccess'] = this.isSuccess;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String accessToken;

  Data({this.accessToken});

  Data.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    return data;
  }
}
