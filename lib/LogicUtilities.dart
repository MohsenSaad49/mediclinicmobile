


import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';

class LogicUtilities {
  static String ensureString(object, {defaultText: ''}) {
    if (object == null) {
      return defaultText;
    }
    return object.toString();
  }


  static double ensureDouble(object) {
    double parsed = 0;
    if (object is String) parsed = double.tryParse(object);
    if (object is int) parsed = object.toDouble();
    if (object is int) parsed = object.toDouble();
    if (object is double) parsed = object.toDouble();
    return parsed == null ? 0.0 : parsed;
  }

  static String ensureStringAsFixed(String stringAsFixed) {}
  static String ensureDoubleToStringAsFixed(double stringAsFixed) {
    if (stringAsFixed == null) return "0";
    return stringAsFixed.toStringAsFixed(0);
  }



  static durationToString(int duration) {
    if (duration == null || duration == 0) return "Missed call";
    var durtaionFormat = Duration(milliseconds: duration);
    return "${durtaionFormat.inMinutes.remainder(60)}:${(durtaionFormat.inSeconds.remainder(60))}";
  }


  static dateToString(String startedAt) {
    var dateParsed = DateTime.parse(startedAt);
    var date=   DateFormat('EEEE, d MMMM | h:mm a').format(dateParsed);
    return date.toString();
  }


  static dateToStringFomatted(String startedAt) {
    var dateParsed = DateTime.parse(startedAt);
    var date=   DateFormat('dd/MM/yyyy - h:mm a').format(dateParsed);
    return date.toString();
  }
  static dateToStringForInvoice(String startedAt) {
    var dateParsed = DateTime.parse(startedAt);
    var date=   DateFormat('dd/MM/yyyy').format(dateParsed);
    return date.toString();
  }

  static int calculateDifference(DateTime date) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day)
        .difference(DateTime(now.year, now.month, now.day))
        .inDays;
  }

  static String getRouteFromURI(route) {
    if (route.contains("?")) {
      var entry = route.split("?");
      return entry[0];
    }
    return route;
  }

  static bool hasParamFromURI(route) {
    return route.contains("?");
  }

  static Map getParamFromURI(route) {
    if (route.contains("?")) {
      var entry = route.split("?");
      var paramsList = new List<String>();
      if (entry[1].contains("&")) {
        paramsList = entry[1].split("&");

        var paramsToPass = new Map();
        paramsList.forEach((f) {
          var values = f.split("=");
          paramsToPass[values[0]] = values[1];
        });

        return paramsToPass;
      } else {
        var values = entry[1].split("=");
        return {values[0]: values[1]};
      }
    }
    return null;
  }

  static int ensureInt(object) {
    int parsed = 0;
    if (object is String) parsed = int.tryParse(object);
    if (object is double) parsed = object.round();
    if (object is int) parsed = object;
    return parsed;
  }

  static String formatCurrency(double price) {
    if (price == null) return "\$0";
    double priceDouble = (price / 100);
    if (priceDouble % 1 == 0)
      return "\$" + priceDouble.toStringAsFixed(0);
    else
      return "\$" + priceDouble.toStringAsFixed(2);
  }
}
