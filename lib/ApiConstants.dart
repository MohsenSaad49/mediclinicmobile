

class ApiConstants {
  static final imageBaseUri =
      "https://storage.googleapis.com/pic-store-bucket/";
  static final rootUrl =
      "http://20.37.241.111/";
  static final avatarPlacholder =
      "https://cdn.pixabay.com/photo/2016/08/20/05/38/avatar-1606916_960_720.png";

  static final aboutUsPage = rootUrl + "about";
  static final patientToken ="patientID";
  static final patientDeviceID = "patientDeviceID";
  static final patientLogin = rootUrl + "patient/login";
  static final patientSites = rootUrl + "api/Sites/get-sites-by-patient";
  static final patientUpcomingAppointment = rootUrl + "api/Bookings/get-patient-upcoming-booking";
  static final patientAllUpcomingAppointment = rootUrl + "api/Bookings/get-patient-all-upcoming-booking";
  static final patientAllPreviousAppointment = rootUrl + "api/Bookings/get-patient-all-previous-booking";
  static final getPatientInvoices = rootUrl + "api/Invoices/get-current-patient-invoices";
  static final getPatientContacts = rootUrl + "api/Clinics/get-all-by-patient-id";
  static final patientLastAppointment = rootUrl + "api/Bookings/get-patient-last-booking";
  static final termsPage = rootUrl + "terms-conditions";

}
