import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/models/authModel.dart';
import 'package:mediclinic/models/patientLoginModel.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/appServices/networkService.dart';
import 'package:http/http.dart' as http;

class AuthService {
  //final settingsService = locator<SettingsService>();
  final networkService = locator<NetworkService>();


/*
  Future<AuthModel> patientLogin(PatientLoginRequest request) async {
    try {
      final body = {
        "firstname": request.firstname,
        "surname": request.surname,
        "device_id":request.device_id,
        "dob": request.dob,
        "mobile_phone":request.mobile_phone==null ||request.mobile_phone.isEmpty?"":request.mobile_phone,
        "home_phone": "",
        "email": request.email
      };

      http.Response response = await http.post (
          'https://testportal.mediclinic.com.au/AppService.asmx/LoginPatientJSON',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: body
      );

      if (response.statusCode == 200) {
       return AuthModel.fromJson(jsonDecode(response.body));
      } else {
        throw new HttpException(AuthModel.fromJson(jsonDecode(response.body)).errorMessage);
      }
    } on DioError  catch (ex) {
      debugPrint(ex.toString());
      throw ex.response.data["message"];
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    }catch ( ex) {
      debugPrint(ex);
      throw new Exception("patientLogin_API_UNKNOWN_ERROR" );
    }
  }
*/

  Future<PatientLoginModel> patientLogin(PatientLoginRequest request) async {
    try {
      final body = {
        "firstname": request.firstname,
        "surname": request.surname,
        "deviceId":request.device_id,
        "dob": request.dob,
        "mobilePhone":request.mobile_phone==null ||request.mobile_phone.isEmpty?"":request.mobile_phone,
        "homePhone":request.home_phone==null ||request.home_phone.isEmpty?"":request.home_phone,
        "email": request.email
      };
      final response =
      await networkService.post(ApiConstants.patientLogin, body: body,headers: {
        HttpHeaders.contentTypeHeader: "application/json"
      });
      if (response.statusCode == 201 ||response.statusCode == 200) {
        return PatientLoginModel.fromJson(response.data);
      } else {
        throw new HttpException(PatientLoginModel.fromJson(response.data).errorMessage);
      }
    } on DioError  catch (ex) {
      debugPrint(ex.toString());
      throw ex.response.data["errors"];
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    }catch ( ex) {
      debugPrint(ex);
      throw new Exception("userLogin_API_UNKNOWN_ERROR" );
    }
  }


}
