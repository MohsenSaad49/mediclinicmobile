import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/models/PatientSitesModel.dart';
import 'package:mediclinic/models/patientAllPreviousAppointment.dart';
import 'package:mediclinic/models/patientAllUpcomingAppointment.dart';
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/patientContacts.dart';
import 'package:mediclinic/models/patientInvoices.dart';
import 'package:mediclinic/models/patientLastBooking.dart';
import 'package:mediclinic/models/patientUpcomingBooking.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/appServices/networkService.dart';
import 'package:http/http.dart' as http;
import 'package:mediclinic/services/appServices/settingsService.dart';

class PatientService {
  final settingsService = locator<SettingsService>();
  final networkService = locator<NetworkService>();


  Future<PatientClinicsModel> getPatientClinics(String id,String deviceId) async {
    try {
      final body = {
        "id": id,
        "device_id":deviceId,
      };
      http.Response response = await http.post (
          'https://testportal.mediclinic.com.au/AppService.asmx/GetClinicListForPatientJSON',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: body
      );

      if (response.statusCode == 200) {
       return PatientClinicsModel.fromJson(jsonDecode(response.body));
      } else {
        throw new HttpException(PatientClinicsModel.fromJson(jsonDecode(response.body)).errorMessage);
      }
    } on DioError  catch (ex) {
      debugPrint(ex.toString());
      throw ex.response.data["message"];
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    }catch ( ex) {
      debugPrint(ex);
      throw new Exception("patientLogin_API_UNKNOWN_ERROR" );
    }
  }

  Future<PatientSitesModel> getPatientSites() async {
    try {
      final response = await networkService
          .get(ApiConstants.patientSites , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}"
      });
      if (response.statusCode == 200) {
        return PatientSitesModel.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientSitesModel.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('getThreadForUser_API_UNKNOWN_ERROR');
    }
  }

  Future<PatientUpcomingBooking> getPatientUpcomingBooking() async {
    try {

      final response = await networkService
          .get(ApiConstants.patientUpcomingAppointment , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientUpcomingBooking.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientUpcomingBooking.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('getPatientUpcomingBooking_API_UNKNOWN_ERROR');
    }
  }

  Future<PatientLastBooking> getPatientLastBooking() async {
    try {

      final response = await networkService
          .get(ApiConstants.patientUpcomingAppointment , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientLastBooking.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientLastBooking.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('getPatientLastBooking_API_UNKNOWN_ERROR');
    }
  }

  Future<PatientAllUpcomingAppointment> getPatientAllUpcommingBooking() async {
    try {

      final response = await networkService
          .get(ApiConstants.patientAllUpcomingAppointment , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientAllUpcomingAppointment.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientAllUpcomingAppointment.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('PatientAllUpcomingAppointment_API_UNKNOWN_ERROR');
    }
  }


  Future<PatientAllPreviousAppointment> getPatientAllPreviousBooking() async {
    try {

      final response = await networkService
          .get(ApiConstants.patientAllPreviousAppointment , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientAllPreviousAppointment.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientAllPreviousAppointment.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('PatientAllPreviousAppointment_API_UNKNOWN_ERROR');
    }
  }

  Future<PatientInvoices> getPatientInvoices() async {
    try {

      final response = await networkService
          .get(ApiConstants.getPatientInvoices , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientInvoices.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientInvoices.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('getPatientInvoices_API_UNKNOWN_ERROR');
    }
  }

  Future<PatientContacts> getPatientContacts() async {
    try {

      final response = await networkService
          .get(ApiConstants.getPatientContacts , headers: {
        HttpHeaders.authorizationHeader: "Bearer ${settingsService.patientAccessToken}",
        "X-DB-NAME":settingsService.xDbName
      });
      if (response.statusCode == 200) {
        return PatientContacts.fromJson(response.data);
      }
      if (response.statusCode == 404) {
        return null;
      } else {
        throw new HttpException(PatientContacts.fromJson(response.data).errorMessage);
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      if (ex.response.statusCode == 404) {
        return null;
      }
    } on HttpException catch (ex) {
      debugPrint(ex.toString());
      throw ex;
    } catch (ex) {
      debugPrint(ex.toString());
      throw new Exception('getPatientContacts_API_UNKNOWN_ERROR');
    }
  }



}
