import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/uiHelpers/colorConstants.dart';

import 'navigationService.dart';

class SettingsService {
  final navigationService = locator<NavigationService>();
  String firstname;
  String surname;
  String patientAccessToken;
  String xDbName;

  String fullName() => "$firstname $surname";



  setUser(String username,String lastname){
    firstname=username;
    surname=lastname;
  }

  setPatientToken(String token){
    patientAccessToken=token;
  }

  setDBName(String dbName){
    xDbName=dbName;

  }

}