import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/appServices/navigationService.dart';
import 'package:mediclinic/uiHelpers/colorConstants.dart';

class DialogService {
  var navService = locator<NavigationService>();
  void showError(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 4,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void showSuccess(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIos: 4,
        backgroundColor: Color(0xFF28a745),
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void showNotify(message) {
    showGeneralDialog(
      barrierLabel: "Dismiss",
      barrierDismissible: true,
      barrierColor: null,
      useRootNavigator: true,
      transitionDuration: Duration(milliseconds: 200),
      context: navService.context,
      pageBuilder: (context, anim1, anim2) {
        Timer timer = new Timer(Duration(seconds: 2), () {
          Navigator.of(context, rootNavigator: true).pop();
        });
        return Align(
          alignment: Alignment.topCenter,
          child: Container(
            padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
            height: 100,
            width: double.infinity,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.notifications,
                  color: ColorConstants.primaryColor,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  message,
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                      decoration: TextDecoration.none,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.normal),
                ),
              ],
            ),
            margin: EdgeInsets.only(bottom: 0, left: 0, right: 0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(0),
            ),
          ),
        );
      },
      transitionBuilder: (context, anim1, anim2, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0, -1), end: Offset(0, 0)).animate(anim1),
          child: child,
        );
      },
    );
  }

  dynamic showPopup(Widget widget, String title,
      {context,
      double height = 0,
      double radius = 0,
      double padding = 0}) async {
    if (context == null && navService.context == null)
      throw new Exception(
          "A context must be passed for showPopup in dialog service.");

    return await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        elevation: 1,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) => GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                height: height == 0 ? 650 : height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  height: 800,
                  child: Material(
                      color: Colors.transparent,
                      child: Column(
                        children: <Widget>[widget],
                      )),
                  margin: EdgeInsets.only(bottom: 0, left: 0, right: 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.circular(radius == 0 ? 20 : radius),
                  ),
                ),
                padding: EdgeInsets.all(padding == 0 ? 20.0 : padding),
              ),
            ));
  }
}
