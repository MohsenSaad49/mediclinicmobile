import 'dart:async';

import 'package:flutter/material.dart';

class NavigationService {
  final StreamController<String> navServiceStream =
  new StreamController.broadcast();

  final GlobalKey<NavigatorState> navigatorKey =
  new GlobalKey<NavigatorState>();

  BuildContext context;
  String route;
  Object params;
  Future<dynamic> navigateTo(String routeName, {argument}) {
    try {
      params = argument;
      return navigatorKey.currentState
          .pushNamed(routeName, arguments: argument);
    } catch (ex) {
      print(ex);
    }
  }

  Future<dynamic> navigateToReplacementNamed(String routeName, {argument}) {
    try {
      params = argument;
      return navigatorKey.currentState
          .pushReplacementNamed(routeName, arguments: argument);
    } catch (ex) {
      print(ex);
    }
  }

  void pop({argument}) {
    try {
      if (!navigatorKey.currentState.canPop()) return;
      navigatorKey.currentState.pop(argument);
    } catch (ex) {
      print(ex);
    }
  }

  Future<dynamic> navigateToReplacementNamedAsRoot(String route, {argument}) {
    try {
      params = argument;
      return navigatorKey.currentState.pushNamedAndRemoveUntil(
          route, (Route<dynamic> route) => false,
          arguments: argument);
    } catch (ex) {
      print(ex);
    }
  }
}
