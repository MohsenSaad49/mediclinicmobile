import 'dart:ffi';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'navigationService.dart';

class NetworkService {
  final navService = locator<NavigationService>();
  Dio _dio;


  NetworkService() {
    _dio = Dio(BaseOptions(
     // baseUrl: AppConfig.rootUrl,
      // connectTimeout: 1000000,
      // receiveTimeout: 1000000,
    ));
  }



  Future<Response> post(url, {body, headers, query}) async {
    return await _dio.post(url,
        data: body, queryParameters: query, options: Options(headers: headers));
  }

  Future<Response> put(url, {body, headers, query}) async {
    return await _dio.put(url,
        data: body, queryParameters: query, options: Options(headers: headers));
  }

  Future<Response> get(url, {headers, query}) async {
    return await _dio.get(url,
        queryParameters: query, options: Options(headers: headers));
  }


  Future<Response> delete(url, {headers, query}) async {
    return await _dio.delete(url,
        queryParameters: query, options: Options(headers: headers));
  }

}
