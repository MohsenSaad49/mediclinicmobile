import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/uiHelpers/colorConstants.dart';

import 'navigationService.dart';

class LoadingService {
  final navigationService = locator<NavigationService>();
  void hideLoading() {
    navigationService.pop();
  }
  void showLoading(String text) async {
    return await showGeneralDialog(
      barrierLabel: "Dismiss",
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      context: navigationService.context,
      useRootNavigator: true,
      pageBuilder: (context, anim1, anim2) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            padding: EdgeInsets.all(20),
            child: Loading(
                indicator: BallBeatIndicator(),
                size: 50.0,
                color: Colors.blueAccent),
            margin: EdgeInsets.only(bottom: 0, left: 0, right: 0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        );
      },
    );
  }
}