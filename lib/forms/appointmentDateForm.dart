import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:table_calendar/table_calendar.dart';

class AppointmentDateForm extends StatelessWidget {
 final Function(String,String,String) onDateSelected;
  AppointmentDateForm({this.onDateSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.blue,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.white, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.white,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(20, 5, 15, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Provider",style: TextStyle(fontSize: 13,color: Colors.grey,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.person,color:Colors.grey)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.graylightcolor.withOpacity(0.2),
                      border: Border.all(
                        color: ColorConstants.graylightcolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
              SizedBox(width: 50,),

              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(20, 5, 15, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Date & Time",style: TextStyle(fontSize: 13,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.calendar_today,color: ColorConstants.darkBluecolor,)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.mainContainersColor,
                      border: Border.all(
                        color: ColorConstants.darkBluecolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Container(
          height: 400,
          child: TableCalendar(
            selectedColor: Colors.blue,
            todayColor: Colors.blue,
            iconColor: Colors.blue,
            eventMarkerColor: Colors.blue,
            calendarFormat: CalendarFormat.month,
            centerHeaderTitle: false,
            formatToggleVisible: false,
            formatToggleTextStyle: TextStyle().copyWith(color: Colors.white, fontSize: 13.0),
            formatToggleDecoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(16.0),
            ),
            //events: _events,
            onDaySelected: (day) {
              onDateSelected(day.year.toString()+"/"+day.month.toString()+"/"+day.day.toString(),DateFormat.yMMMMEEEEd().format(day),DateFormat.yMMMMd().format(day));
            },
          ),
        ),

        ],
      ),
    );
  }
}
