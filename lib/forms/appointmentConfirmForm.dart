import 'package:flutter/material.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:table_calendar/table_calendar.dart';

class AppointmentConfirmForm extends StatelessWidget {
  final Function(bool) onConfirmSelected;
  AppointmentConfirmForm({this.onConfirmSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.fromLTRB(15,40,15,0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                       Text('Please check appointment summery', maxLines:2,style: TextStyle(color: Colors.red,fontSize: 20,fontWeight: FontWeight.bold,),),

                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('above before clicking confirm', maxLines:2,style: TextStyle(color: Colors.red,fontSize: 20,fontWeight: FontWeight.bold,),),

                    ],
                  )
                ],
              ),
            ],),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(15,60,15,10),
                child:     Container(

                  child: RaisedButton(
                    color:Colors.blueAccent,
                    textColor: ColorConstants.primaryColor,
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                        side: BorderSide(
                            color: Colors.blueAccent)),
                    child: Text(
                      "Confirm Booking",
                      style: TextStyle( color: Colors.white,
                          fontFamily: UiConstants.segeoRegularFont,
                          fontSize: 20, fontWeight: FontWeight.normal
                      ),
                    ),
                    onPressed: () {

                      onConfirmSelected(true);
                    },
                  ),
                )),
            ],
          ),


        ],
      ),
    );
  }
}
