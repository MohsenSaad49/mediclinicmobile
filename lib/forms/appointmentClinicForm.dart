import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';

class AppointmentClinicsForm extends StatelessWidget {
  final Function(String) onClinicSelected;
  AppointmentClinicsForm({this.onClinicSelected});

  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.blue,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.white, size: 15,),
                    ),
                    Text("Select Clinic",style: TextStyle(color: Colors.white,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
          SizedBox(height: 5,),
          Container(
          height: 280,
          child: ListView.builder(
          itemCount: 3,
          itemBuilder: (context, index) {
            return   GestureDetector(
              onTap: (){
                onClinicSelected("clinic name");
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8,8,8,0),
                child: Container(
height: 135,
                  padding: const EdgeInsets.fromLTRB(0,8,0,0),
                  decoration:BoxDecoration(
                    color:Colors.white,
                    border: Border.all(
                      color: ColorConstants.actionsColor,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: SvgPicture.asset(
                              "assets/iconfinder-icon.svg", width: 150, height: 100,
                            ),
                          )
                        ],
                      ),
                     Container(
                       width: 190,
                       child: Padding(
                         padding: const EdgeInsets.fromLTRB(15,0,0,0),
                         child: Column(
                           children: <Widget>[
                                 Row(children: <Widget>[
                                  Icon(Icons.location_on,color: Colors.blue,size: 18,),
                                   SizedBox(width: 5,),
                                   Padding(
                                     padding: const EdgeInsets.only(top:5),
                                     child: Container( width: 130, child: Text('494 WHITEHOUSE ROAD SURREY HILLS VIC,3127 ',style: TextStyle(fontSize: 10),maxLines: 2,)),
                                   )
                                 ],),
                                SizedBox(height: 5,),

                                 Row(children: <Widget>[
                               Icon(Icons.mail_outline,color: Colors.blue,size: 18,),
                               SizedBox(width: 5,),
                               Container( width: 150, child: Text('info@MediClinic.com.au',style: TextStyle(fontSize: 10),maxLines: 2,))
                             ],),
                             SizedBox(height: 5,),
                             Row(children: <Widget>[
                               Icon(Icons.phone_in_talk,color: Colors.blue,size: 18,),
                               SizedBox(width: 5,),
                               Container( width: 150, child: Text('03 9947 1981',style: TextStyle(fontSize: 10),maxLines: 2,))
                             ],),
                             Row(
                               mainAxisAlignment: MainAxisAlignment.end,

                               children: <Widget>[
                                 Padding(
                                   padding: const EdgeInsets.fromLTRB(0,15,0,0),
                                   child: Container(
                                     height: 30,
                                     width:80,
                                     child: RaisedButton(
                                       color:Colors.blueAccent,
                                       textColor: ColorConstants.primaryColor,
                                       elevation: 1,
                                       shape: RoundedRectangleBorder(
                                           borderRadius: new BorderRadius.circular(20.0),
                                           side: BorderSide(
                                               color: Colors.blueAccent)),
                                       child: Text(
                                         "Select",
                                         style: TextStyle( color: Colors.white,
                                             fontFamily: UiConstants.segeoLightFont,
                                             fontSize: 15, fontWeight: FontWeight.normal
                                         ),
                                       ),
                                       onPressed: () {

                                         onClinicSelected("clinic name");
                                       },
                                     ),
                                   ),
                                 )
                             ],),

                           ],
                         ),
                       ),
                     )


                    ],
                  ),

                ),
              ),
            );
          }),
        ),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.grey.withOpacity(0.3),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.3),
                    width: 0,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.transparent, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.grey,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
        ],
      ),
    );
  }
}
