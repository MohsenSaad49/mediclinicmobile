import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/bookNewAppointmentProvider.dart';
import 'package:table_calendar/table_calendar.dart';

class AppointmentBookingDetailsForm extends StatelessWidget {
  final Function(bool) onMainMenuSelected;
  BookNewAppointmentProvider  bookNewAppointmentProvider;
  AppointmentBookingDetailsForm(this.bookNewAppointmentProvider,{this.onMainMenuSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,0),
            child: Row(children: <Widget>[
              Text('Booking Details',style: TextStyle(color: ColorConstants.darkBluecolor,fontSize: 17,fontWeight: FontWeight.bold,fontFamily: UiConstants.segeoRegularFont),),
            ],),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(15,0,15,0),
              child:  Divider(thickness: 1,)),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,5),
            child: Row(children: <Widget>[
              Text(bookNewAppointmentProvider.formatDate+" - "+bookNewAppointmentProvider.time,style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold,fontFamily: UiConstants.segeoRegularFont),),
            ],),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,5),
            child: Row(children: <Widget>[
              Text('Provider: '+bookNewAppointmentProvider.provider,style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold,fontFamily: UiConstants.segeoRegularFont),),
            ],),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,5),
            child: Row(children: <Widget>[
              Text('Service: '+bookNewAppointmentProvider.service,style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold,fontFamily: UiConstants.segeoRegularFont),),
            ],),
          ),
        Padding(
            padding: const EdgeInsets.fromLTRB(8,8,8,0),
            child: Container(
              height: 135,
              padding: const EdgeInsets.fromLTRB(0,8,0,0),
              decoration:BoxDecoration(
                color:Colors.white,
                border: Border.all(
                  color: ColorConstants.actionsColor,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: SvgPicture.asset(
                          "assets/iconfinder-icon.svg", width: 150, height: 100,
                        ),
                      )
                    ],
                  ),
                  Container(
                    width: 190,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15,0,0,0),
                      child: Column(
                        children: <Widget>[
                          Row(children: <Widget>[
                            Icon(Icons.location_on,color: Colors.blue,size: 18,),
                            SizedBox(width: 5,),
                            Padding(
                              padding: const EdgeInsets.only(top:5),
                              child: Container( width: 130, child: Text('494 WHITEHOUSE ROAD SURREY HILLS VIC,3127 ',style: TextStyle(fontSize: 10),maxLines: 2,)),
                            )
                          ],),
                          SizedBox(height: 5,),

                          Row(children: <Widget>[
                            Icon(Icons.mail_outline,color: Colors.blue,size: 18,),
                            SizedBox(width: 5,),
                            Container( width: 150, child: Text('info@MediClinic.com.au',style: TextStyle(fontSize: 10),maxLines: 2,))
                          ],),
                          SizedBox(height: 5,),
                          Row(children: <Widget>[
                            Icon(Icons.phone_in_talk,color: Colors.blue,size: 18,),
                            SizedBox(width: 5,),
                            Container( width: 150, child: Text('03 9947 1981',style: TextStyle(fontSize: 10),maxLines: 2,))
                          ],),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,

                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,15,0,0),
                                child: Container(
                                  height: 30,
                                  width:85,
                                  child: RaisedButton(
                                    color:Colors.blueAccent,
                                    textColor: ColorConstants.primaryColor,
                                    elevation: 1,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(20.0),
                                        side: BorderSide(
                                            color: Colors.blueAccent)),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.location_on,color: Colors.white,size: 20,),
                                        Text(
                                          " Map",
                                          style: TextStyle( color: Colors.white,
                                              fontFamily: UiConstants.segeoLightFont,
                                              fontSize: 15, fontWeight: FontWeight.normal
                                          ),
                                        ),
                                      ],
                                    ),
                                    onPressed: () {


                                    },
                                  ),
                                ),
                              )
                            ],),

                        ],
                      ),
                    ),
                  )


                ],
              ),

            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.fromLTRB(15,20,15,0),
                  child:     Container(

                    child: RaisedButton(
                      color:Colors.blueAccent,
                      textColor: ColorConstants.primaryColor,
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          side: BorderSide(
                              color: Colors.blueAccent)),
                      child: Text(
                        "Main Menu",
                        style: TextStyle( color: Colors.white,
                            fontFamily: UiConstants.segeoRegularFont,
                            fontSize: 15, fontWeight: FontWeight.normal
                        ),
                      ),
                      onPressed: () {

                        onMainMenuSelected(true);
                      },
                    ),
                  )),
            ],
          ),


        ],
      ),
    );
  }
}
