import 'package:flutter/material.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/bookNewAppointmentProvider.dart';
import 'package:table_calendar/table_calendar.dart';

class AppointmentTimeForm extends StatelessWidget {
  final Function(String) onTimeSelected;
  BookNewAppointmentProvider  bookNewAppointmentProvider;
  AppointmentTimeForm(this.bookNewAppointmentProvider,{this.onTimeSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.blue,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.white, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.white,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(20, 5, 15, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Provider",style: TextStyle(fontSize: 13,color: Colors.grey,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.person,color:Colors.grey)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.graylightcolor.withOpacity(0.2),
                      border: Border.all(
                        color: ColorConstants.graylightcolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
              SizedBox(width: 50,),

              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(20, 5, 15, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Date & Time",style: TextStyle(fontSize: 13,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.calendar_today,color: ColorConstants.darkBluecolor,)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.mainContainersColor,
                      border: Border.all(
                        color: ColorConstants.darkBluecolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,0),
            child: Row(children: <Widget>[
              Text(bookNewAppointmentProvider.formatDate,style: TextStyle(color: ColorConstants.darkBluecolor,fontSize: 17,fontWeight: FontWeight.bold,fontFamily: UiConstants.segeoLightFont),),
            ],),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,0,15,0),
            child:  Divider(thickness: 1,)),
          Container(
          height: 300,
          child:  Padding(
            padding: const EdgeInsets.fromLTRB(15,5,15,5),
            child: GridView.count(
              // Create a grid with 2  columns. If you change the scrollDirection to
              // horizontal, this produces 2 rows.
              crossAxisCount: 4, crossAxisSpacing: 20, mainAxisSpacing: 15,
              // Generate 100 widgets that display their index in the List.
              children: List.generate(8, (index) {
                return GestureDetector(
                  onTap: (){
                    onTimeSelected("9:15 AM");
                  },
                  child: Container(


                      padding:EdgeInsets.fromLTRB(0, 0, 0, 0),

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Text("9:15 AM",style: TextStyle(fontSize: 13,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color:(index!=2)? ColorConstants.graylightcolor.withOpacity(0.2):ColorConstants.graylightcolor,
                        border: Border.all(
                          color: ColorConstants.darkBluecolor,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(5),
                      )
                  ),
                );
              }),
            ),
          ),
        ),

        ],
      ),
    );
  }
}
