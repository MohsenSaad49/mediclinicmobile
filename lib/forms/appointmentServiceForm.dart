import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/bookNewAppointmentProvider.dart';


class AppointmentServiceForm extends StatelessWidget {

 final Function(String) onServiceSelected;
 final Function(String) onSubServiceSelected;
 BookNewAppointmentProvider  bookNewAppointmentProvider;
  AppointmentServiceForm(this.bookNewAppointmentProvider,{this.onServiceSelected,this.onSubServiceSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(

      child: Column(

        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[



          ExpandableNotifier( initialExpanded: true,
              child: Padding(
                padding: const EdgeInsets.all(0),
                child: ScrollOnExpand(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 20,),
                        Row(
                          children: <Widget>[
                            Container(
                              width:250,
                              decoration:BoxDecoration(
                                color: Colors.blue,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                                    child: Icon(Icons.radio_button_checked,color: Colors.white, size: 15,),
                                  ),
                                  Text("Select Profession & Service",style: TextStyle(color: Colors.white,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                                ],
                              ),

                            ),
                          ],
                        ),

                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Builder(
                                builder: (context) {
                                  var controller =
                                  ExpandableController.of(context);
                                  return FlatButton(
                                    padding: EdgeInsets.all(15),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context)
                                              .size
                                              .width -
                                              30,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              border: Border.all(
                                                color: Colors.blueAccent,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceBetween,
                                              mainAxisSize: MainAxisSize.max,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.only(left: 15,top:15,bottom: 15,right: 15),
                                                  child: Text(
                                                    "Physiotherapy",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(right:15),
                                                  child: Icon(
                                                    controller.expanded
                                                        ? Icons
                                                        .keyboard_arrow_up
                                                        : Icons
                                                        .keyboard_arrow_down,
                                                    color: ColorConstants
                                                        .darkBluecolor,
                                                    size: 25,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    onPressed: () {
                                      controller.toggle();
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        Expandable(
                          collapsed:new Container(),
                          expanded: Container(
                            height: 100,
                            child: ListView.builder(
                                padding: EdgeInsets.fromLTRB(25, 5, 20, 5),
                                itemCount:4,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: (){
                                      onServiceSelected("Service "+index.toString());
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(0.0),
                                        child: ListTile( dense: true,
                                          contentPadding: EdgeInsets.all(0),
                                          leading: Icon(Icons.info_outline , color: Colors.blueAccent,size: 20,),
                                          title:    Text(
                                            "Service "+index.toString(),
                                            style: TextStyle( color: Colors.blueGrey[800],
                                                fontFamily: UiConstants.segeoLightFont,
                                                fontSize: 15, fontWeight: FontWeight.normal
                                            ),
                                          ),
                                        ),
                                          /*child: Row(
                                            children: <Widget>[
                                              Icon(Icons.info_outline , color: Colors.blueAccent,size: 20,),
                                              SizedBox(width: 10,),
                                              Text(
                                                " initial consultation",
                                                style: TextStyle( color: Colors.blueGrey[800],
                                                    fontFamily: UiConstants.segeoLightFont,
                                                    fontSize: 15, fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            ],
                                          ),*/
                                        ),

                                        Divider(color: Colors.grey, height: 1,)
                                      ],
                                    ),
                                  );
                                })    ,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          ExpandableNotifier( initialExpanded: bookNewAppointmentProvider.expandSubService,
              child: Padding(
                padding: const EdgeInsets.all(0),
                child: ScrollOnExpand(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Builder(
                                builder: (context) {
                                  var controller =
                                  ExpandableController.of(context);
                                  return FlatButton(
                                    padding: EdgeInsets.all(15),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context)
                                              .size
                                              .width -
                                              30,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              border: Border.all(
                                                color: Colors.blueAccent,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceBetween,
                                              mainAxisSize: MainAxisSize.max,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.only(left: 15,top:15,bottom: 15,right: 15),
                                                  child: Text(
                                                    "Podiatry",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(right:15),
                                                  child: Icon(
                                                    controller.expanded
                                                        ? Icons
                                                        .keyboard_arrow_up
                                                        : Icons
                                                        .keyboard_arrow_down,
                                                    color: ColorConstants
                                                        .darkBluecolor,
                                                    size: 25,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    onPressed: () {
                                      controller.toggle();
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        Expandable(
                          collapsed:new Container(),
                          expanded: Container(
                            height: 100,
                            child: ListView.builder(
                                padding: EdgeInsets.fromLTRB(25, 5, 20, 5),
                                itemCount:4,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: (){
                                      onSubServiceSelected( "Sub Service "+index.toString());
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(0.0),
                                          child: ListTile( dense: true,
                                            contentPadding: EdgeInsets.all(0),
                                            leading: Icon(Icons.info_outline , color: Colors.blueAccent,size: 20,),
                                            title:    Text(
                                              "Sub Service "+index.toString(),
                                              style: TextStyle( color: Colors.blueGrey[800],
                                                  fontFamily: UiConstants.segeoLightFont,
                                                  fontSize: 15, fontWeight: FontWeight.normal
                                              ),
                                            ),
                                          ),
                                          /*child: Row(
                                            children: <Widget>[
                                              Icon(Icons.info_outline , color: Colors.blueAccent,size: 20,),
                                              SizedBox(width: 10,),
                                              Text(
                                                " initial consultation",
                                                style: TextStyle( color: Colors.blueGrey[800],
                                                    fontFamily: UiConstants.segeoLightFont,
                                                    fontSize: 15, fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            ],
                                          ),*/
                                        ),

                                        Divider(color: Colors.grey, height: 1,)
                                      ],
                                    ),
                                  );
                                })    ,
                            /* child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.all(15),
                                                                    child: Column(
                                                                      crossAxisAlignment:
                                                                      CrossAxisAlignment.start,
                                                                      children: <Widget>[
                                                                        Row(
                                                                          children: <Widget>[
                                                                            Icon(Icons.info_outline , color: Colors.blueAccent,),
                                                                            Text('Intial conslutation'),
                                                                          ],
                                                                        )
                                                                        ,
                                                                       *//* Container(
                                                                          decoration: BoxDecoration(
                                                                            color: ColorConstants
                                                                                .defaultBoxColor,
                                                                            border: Border.all(
                                                                              color: Colors.transparent,
                                                                              width: 0,
                                                                            ),
                                                                            borderRadius:
                                                                            BorderRadius.circular(0),
                                                                          ),
                                                                          child: Padding(
                                                                            padding: EdgeInsets.only(
                                                                                left: 0, right: 0),
                                                                            child:  Column(
                                                                              children: <Widget>[
                                                                                Row(
                                                                                  children: <Widget>[
                                                                                    Icon(Icons.info_outline , color: Colors.blueAccent,),
                                                                                    Text('Intial conslutation'),
                                                                                  ],
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),*//*
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ]),*/
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),

          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.grey.withOpacity(0.3),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.3),
                    width: 0,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.transparent, size: 15,),
                    ),
                    Text("Select Clinic",style: TextStyle(color: Colors.grey,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
          SizedBox(height: 15,),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.grey.withOpacity(0.3),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.3),
                    width: 0,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.transparent, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.grey,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
        ],
      ),
    );
  }
}
