import 'package:flutter/material.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';

class AppointmentProviderForm extends StatelessWidget {
 final Function(String) onProviderSelected;
  AppointmentProviderForm({this.onProviderSelected});
  @override
  Widget build(BuildContext context) {
    //final accessCodeProvider = Provider.of<AccessCodeProvider>(context);
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.blue,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.white, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.white,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(20, 5, 15, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Provider",style: TextStyle(fontSize: 13,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.person_outline,color: ColorConstants.darkBluecolor,)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.mainContainersColor,
                      border: Border.all(
                        color: ColorConstants.darkBluecolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
              SizedBox(width: 50,),
              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                child: Container(
                    padding:EdgeInsets.fromLTRB(10, 5, 5, 5),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Date & Time",style: TextStyle(fontSize: 13,color: Colors.grey,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Icon(Icons.calendar_today,color:Colors.grey)
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: ColorConstants.graylightcolor.withOpacity(0.2),
                      border: Border.all(
                        color: ColorConstants.graylightcolor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    )
                ),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Container(
          height: 280,
          child: ListView.builder(
            padding: const EdgeInsets.only(top:15),
          itemCount: 3,
          itemBuilder: (context, index) {
            return   GestureDetector(
              onTap: (){
                onProviderSelected("Physiotherapy-john");
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30,0,20,5),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                Text('Physiotherapy    -',
                  style: TextStyle( color: Colors.blueGrey[700],
                      fontFamily: UiConstants.segeoLightFont,
                      fontSize: 15, fontWeight: FontWeight.normal
                  )),
                        SizedBox(width: 15,),
                        Text(
                          " john smith",
                          style: TextStyle( color: Colors.blueGrey[700],
                              fontFamily: UiConstants.segeoLightFont,
                              fontSize: 15, fontWeight: FontWeight.normal
                          ),
                        ),
                      ],
                    ),

                    SizedBox(height: 5,),
                    Divider(color: ColorConstants.graylightcolor,thickness: 1,),
                  ],
                ),
              ),
            );
          }),
        ),
          Row(
            children: <Widget>[
              Container(
                width:250,
                decoration:BoxDecoration(
                  color: Colors.grey.withOpacity(0.3),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.3),
                    width: 0,
                  ),
                  borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0,8,8,8),
                      child: Icon(Icons.radio_button_checked,color: Colors.transparent, size: 15,),
                    ),
                    Text("Select Provider & Time",style: TextStyle(color: Colors.grey,fontSize: 15,fontFamily: UiConstants.segeoRegularFont),)
                  ],
                ),

              ),
            ],
          ),
        ],
      ),
    );
  }
}
