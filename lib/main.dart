import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mediclinic/LogicUtilities.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/pages/appRegConfirmPage.dart';
import 'package:mediclinic/pages/bookNewAppointmentPage.dart';
import 'package:mediclinic/pages/createPatientProfilePage.dart';
import 'package:mediclinic/pages/patientBookingPage.dart';
import 'package:mediclinic/pages/patientContactPage.dart';
import 'package:mediclinic/pages/patientInvoicePage.dart';
import 'package:mediclinic/pages/patientMainMenuPage.dart';
import 'package:mediclinic/pages/patientProfileMain.dart';
import 'package:mediclinic/pages/patientSharedDocumentPage.dart';
import 'package:mediclinic/pages/providerContactPage.dart';
import 'package:mediclinic/pages/providerInvoicePage.dart';
import 'package:mediclinic/pages/providerLoginPage.dart';
import 'package:mediclinic/pages/patientClinicsPage.dart';
import 'package:mediclinic/pages/patientLoginPage.dart';
import 'package:mediclinic/pages/providerMainMenuPage.dart';
import 'package:mediclinic/pages/providerPatientProfilePage.dart';
import 'package:mediclinic/pages/providerProfileMain.dart';
import 'package:mediclinic/pages/registerNewPatientPage.dart';
import 'package:mediclinic/pages/registerPatientWithoutDate.dart';
import 'package:mediclinic/pages/siteSelectionPage.dart';
import 'package:mediclinic/pages/termsConditionsPage.dart';
import 'package:mediclinic/pages/welcomePage.dart';
import 'package:mediclinic/providers/appRegConfirmProvider.dart';
import 'package:mediclinic/providers/bookNewAppointmentProvider.dart';
import 'package:mediclinic/providers/createPatientProfileProvider.dart';
import 'package:mediclinic/providers/patientBookingProvider.dart';
import 'package:mediclinic/providers/patientContactProvider.dart';
import 'package:mediclinic/providers/patientInvoiceProvider.dart';
import 'package:mediclinic/providers/patientMainMenuProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/patientSharedDocumentProvider.dart';
import 'package:mediclinic/providers/providerContactProvider.dart';
import 'package:mediclinic/providers/providerInvoiceProvider.dart';
import 'package:mediclinic/providers/providerLoginProvider.dart';
import 'package:mediclinic/providers/MainAppProvider.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/patientClinicsProvider.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/providers/providerPatientProfileProvider.dart';
import 'package:mediclinic/providers/providerProfileMainProvider.dart';
import 'package:mediclinic/providers/registerNewPatientProvider.dart';
import 'package:mediclinic/providers/registerPatientWithoutDateProvider.dart';
import 'package:mediclinic/providers/siteSelectionProvider.dart';
import 'package:mediclinic/providers/termsConditionsProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/appServices/navigationService.dart';
import 'package:mediclinic/widgets/statefull/AppLifeCycle.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
void main() {

  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget
{
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      child: MaterialApp(
        navigatorObservers: [routeObserver],
        navigatorKey: locator<NavigationService>().navigatorKey,
        title: 'MediClinic',
        initialRoute: RoutesConstants.welcomePage,
        onGenerateRoute: onGenerateRoute,
       // theme: AppThemeData.defaultTheme,
        builder: (context, widget) {
          return AppLifeCycle(
            onResume: () {
              Provider.of<MainAppProvider>(context, listen: false).didResume();
            },
            child: Consumer<MainAppProvider>(
                builder: (BuildContext context, value, Widget child) {
                  return Stack(
                    children: <Widget>[
                      Scaffold(
                        extendBodyBehindAppBar: true,
                        extendBody: true,
                        body: Container(
                          height: double.infinity,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                            /*  SizedBox(
                                height: 40 ,
                              ),*/
                              Expanded(child: widget)
                            ],
                          ),
                        ),
                      ),

                    ],
                  );
                }),
          );
        },
      ),
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<MainAppProvider>(
            create: (_) => MainAppProvider()),
      ],
    );
  }

  Route onGenerateRoute(RouteSettings settings) {
    var routes = {
      RoutesConstants.providerLoginPage: ChangeNotifierProvider<ProviderLoginProvider>(
          create: (context) => ProviderLoginProvider(), child: ProviderLoginPage()),

      RoutesConstants.patientLoginPage: ChangeNotifierProvider<LoginProvider>(
          create: (context) => LoginProvider(), child: PatientLoginPage()),

      RoutesConstants.patientMainMenuPage: ChangeNotifierProvider<PatientMainMenuProvider>(
          create: (context) => PatientMainMenuProvider(), child: PatientMainMenuPage()),

      RoutesConstants.patientBookingPage: ChangeNotifierProvider<PatientBookingProvider>(
          create: (context) => PatientBookingProvider(), child: PatientBookingPage()),

      RoutesConstants.providerMainMenuPage: ChangeNotifierProvider<ProviderMainMenuProvider>(
          create: (context) => ProviderMainMenuProvider(), child: ProviderMainMenuPage()),

      RoutesConstants.welcomePage: ChangeNotifierProvider<LoginProvider>(
          create: (context) => LoginProvider(), child: WelcomePage()),

      RoutesConstants.patientClinicsPage: ChangeNotifierProvider<PatientClinicsProvider>(
          create: (context) => PatientClinicsProvider(), child: PatientClinicsPage()),

      RoutesConstants.createPatientProfilePage: ChangeNotifierProvider<CreatePatientProfileProvider>(
          create: (context) => CreatePatientProfileProvider(), child: CreatePatientProfilePage()),

      RoutesConstants.bookNewAppointmentPage: ChangeNotifierProvider<BookNewAppointmentProvider>(
          create: (context) => BookNewAppointmentProvider(), child: BookNewAppointmentPage()),

      RoutesConstants.patientProfileMainPage: ChangeNotifierProvider<PatientProfileMainProvider>(
          create: (context) => PatientProfileMainProvider(), child: PatientProfileMainPage()),


      RoutesConstants.siteSelectionPage: ChangeNotifierProvider<SiteSelectionProvider>(
          create: (context) => SiteSelectionProvider(), child: SiteSelectionPage()),



      RoutesConstants.patientContactPage: ChangeNotifierProvider<PatientContactProvider>(
          create: (context) => PatientContactProvider(), child: PatientContactPage()),


      RoutesConstants.providerContactPage: ChangeNotifierProvider<ProviderContactProvider>(
          create: (context) => ProviderContactProvider(), child: ProviderContactPage()),

      RoutesConstants.providerPatientProfilePage: ChangeNotifierProvider<ProviderPatientProfileProvider>(
          create: (context) => ProviderPatientProfileProvider(), child: ProviderPatientProfilePage()),

      RoutesConstants.termsConditionsPage: ChangeNotifierProvider<TermsConditionsProvider>(
          create: (context) => TermsConditionsProvider(), child: TermsConditionsPage()),

      RoutesConstants.providerProfileMainPage: ChangeNotifierProvider<ProviderProfileMainProvider>(
          create: (context) => ProviderProfileMainProvider(), child: ProviderProfileMainPage()),

    RoutesConstants.providerInvoicePage: ChangeNotifierProvider<ProviderInvoiceProvider>(
          create: (context) => ProviderInvoiceProvider(), child: ProviderInvoicePage()),

      RoutesConstants.patientInvoicePage: ChangeNotifierProvider<PatientInvoiceProvider>(
          create: (context) => PatientInvoiceProvider(), child: PatientInvoicePage()),

      RoutesConstants.registerNewPatientPage: ChangeNotifierProvider<RegisterNewPatientProvider>(
          create: (context) => RegisterNewPatientProvider(), child: RegisterNewPatientPage()),

      RoutesConstants.appRegConfirmPage: ChangeNotifierProvider<AppRegConfirmProvider>(
          create: (context) => AppRegConfirmProvider(), child: AppRegConfirmPage()),

      RoutesConstants.registerPatientWithoutDataPage: ChangeNotifierProvider<RegisterPatientWithoutDateProvider>(
          create: (context) => RegisterPatientWithoutDateProvider(), child: RegisterPatientWithoutDatePage()),

      RoutesConstants.patientSharedDocumentPage: ChangeNotifierProvider<PatientSharedDocumentProvider>(
          create: (context) => PatientSharedDocumentProvider(), child: PatientSharedDocumentPage()),


    };

    var route = LogicUtilities.getRouteFromURI(settings.name);
    var param = settings.arguments;
    if (LogicUtilities.hasParamFromURI(settings.name)) {
      param = LogicUtilities.getParamFromURI(settings.name);
    }

    if (routes.containsKey(route)) {
      return MaterialPageRoute(builder: (context) {
        locator<NavigationService>().params = param;
        locator<NavigationService>().route = route;
        locator<NavigationService>().navServiceStream.add("navigate");
        return RouteAwareWidget(child: routes[route]);
      });
    }
  }

}

class RouteAwareWidget extends StatefulWidget {
  final Widget child;
  RouteAwareWidget({this.child});
  State<RouteAwareWidget> createState() => RouteAwareWidgetState();
}

class RouteAwareWidgetState extends State<RouteAwareWidget> with RouteAware {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    locator<NavigationService>().context = context;
    locator<NavigationService>().navServiceStream.add("navigate");
  }

  @override
  void didPopNext() {
    locator<NavigationService>().context = context;
    locator<NavigationService>().navServiceStream.add("navigate");
  }

  @override
  Widget build(BuildContext context) => widget.child;
}

