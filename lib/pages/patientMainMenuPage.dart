import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:mediclinic/LogicUtilities.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/patientClinicsProvider.dart';
import 'package:mediclinic/providers/patientMainMenuProvider.dart';
import 'package:mediclinic/shimmers/shimmerDashboardLayout.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/AppBarWidget.dart';
import 'package:mediclinic/widgets/timeline.dart';
import 'package:provider/provider.dart';

class PatientMainMenuPage extends StatefulWidget {
  @override
  PatientMainMenuPageState createState() {
    return PatientMainMenuPageState();
  }
}
class PatientMainMenuPageState extends  State<PatientMainMenuPage> {
  PatientMainMenuProvider  patientMainMenuProvider;

  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();



  void initState() {


      tiles = new List<Widget>();

      tiles.add(new Row(
        children: <Widget>[
           Text("Clinic",style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
           SizedBox(width: 12,),
           Text(patientMainMenuProvider?.patientUpcomingBooking?.clinicName==null?"":patientMainMenuProvider?.patientUpcomingBooking?.clinicName,style: TextStyle(fontSize: 15,color: Colors.black54, fontFamily: UiConstants.segeoRegularFont),),
        ],

      ));
      tiles.add(new Row(
        children: <Widget>[
          Text(patientMainMenuProvider?.patientUpcomingBooking?.bookingDateStart==null?"":patientMainMenuProvider?.patientUpcomingBooking?.bookingDateStart.split(',')[0],style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
          SizedBox(width: 12,),

          Text(patientMainMenuProvider?.patientUpcomingBooking?.bookingDateStart==null?"":patientMainMenuProvider?.patientUpcomingBooking?.bookingDateStart.split(',')[1],style: TextStyle(fontSize: 15,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],

      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Provider",style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
          SizedBox(width: 12,),
          Text(":"+patientMainMenuProvider?.patientUpcomingBooking?.fullName().toString(),style: TextStyle(fontSize: 15,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),

        ],
      ));


      indicators = new List<Padding>();

        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));

  }


  @override
  Widget build(BuildContext context) {
    patientMainMenuProvider = Provider.of<PatientMainMenuProvider>(context, listen: true);
    if(patientMainMenuProvider.loadingState==LoadingState.done)
      initState();
    return  Container(
      color: ColorConstants.graylightcolor,
      child:  Scaffold(
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:AppStateHandlerWidget(
                  state: patientMainMenuProvider.loadingState,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.fromLTRB(10,20,10,10),
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                              ),
                              child: AppStateHandlerWidget(
                                state: patientMainMenuProvider.loadingState,
                                child: Column(
                                  children: <Widget>[

                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(12.0,0,8,8),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text("Upcoming appointment",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                          GestureDetector(
                                            child: Container(
                                              width: 40, height: 40,
                                              child: IconButton(

                                                icon: Image.asset(
                                                  'assets/images/userprofile.png',width: 20, height: 20,),
                                                color: ColorConstants.primaryColor,
                                              ),
                                              decoration: BoxDecoration(

                                                color: Colors.blueAccent,

                                                borderRadius: BorderRadius.circular(25),
                                              ),
                                            ),
                                            onTap: (){
                                              Navigator.pushNamed(context, RoutesConstants.patientProfileMainPage);
                                            },
                                          )

                                        ],
                                      ),
                                    ),
                                    Timeline(
                                      padding:EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      children:tiles,
                                      indicators: indicators,
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          child: Padding(
                                            padding: const EdgeInsets.only(left:10.0),
                                            child: GestureDetector(
                                              onTap: ()async{

                                                final availableMaps = await MapLauncher.installedMaps;
                                                await availableMaps.first.showMarker(
                                                  coords: Coords(patientMainMenuProvider.patientUpcomingBooking.clinicLatitude, patientMainMenuProvider.patientUpcomingBooking.clinicLongitude),
                                                  title: patientMainMenuProvider.patientUpcomingBooking.clinicName,
                                                );

                                                Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                              },
                                              child: Container(
                                                width:100,
                                                padding:EdgeInsets.fromLTRB(5, 5, 5, 5),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Icon(Icons.location_on,color: Colors.white,),
                                                    Text("Map",style: TextStyle(fontSize: 15,color: Colors.white,fontFamily: UiConstants.segeoLightFont),),
                                                  ],
                                                ),
                                                decoration: BoxDecoration(

                                                  color: Colors.blueAccent,

                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                            ),
                                          ),
                                          onTap: () {
                                            // Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                          },
                                        ),
                                        SizedBox(width: 15,),
                                        Container(
                                            width: 150,
                                            child: Text(patientMainMenuProvider?.patientUpcomingBooking?.clinicBusinessAddress1==null?"":patientMainMenuProvider?.patientUpcomingBooking?.clinicBusinessAddress1,style: TextStyle(fontSize: 14, color:Colors.black54,fontFamily: UiConstants.segeoRegularFont),)),


                                      ],
                                    ),

                                    SizedBox(height: 10,)
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 25,),
                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(

                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset(
                                                    'assets/images/calender.png',width: 40, height: 40,),


                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,15,0),
                                                      child: Text("ALL BOOKINGS",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),
                                                    ),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View booking histories",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View all appointments ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                            width: 4,thickness: 0.75,
                                                            color: ColorConstants.arrowColor
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),
                            SizedBox(height: 10,),
                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.patientInvoicePage);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(

                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(

                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset(
                                                    'assets/images/invoice.png',width: 40, height: 40,),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(

                                                  children: <Widget>[
                                                    Text("INVOICES                        ",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View invoice status and histories",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Request invoice                        ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                          width: 4, thickness: 0.75,
                                                          color: ColorConstants.arrowColor,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),

                            SizedBox(height: 10,),
                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.patientContactPage);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(

                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(

                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset(
                                                    'assets/images/comment.png',width: 40, height: 40,),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(

                                                  children: <Widget>[
                                                    Text("CONTACT                        ",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Contact clinic staff                       ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Inbox messages                        ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                          width: 4, thickness: 0.75,
                                                          color: ColorConstants.arrowColor,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),
                            SizedBox(height: 20,),
                            GestureDetector(
                              onTap: (){
                                patientMainMenuProvider.navService.navigateTo(RoutesConstants.bookNewAppointmentPage);
                                //Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                              },
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Container(

                                    padding:EdgeInsets.fromLTRB(15, 5, 15, 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[

                                        Text("Book new appointment",style: TextStyle(fontSize: 15,color: Colors.white,fontFamily: UiConstants.segeoRegularFont),),
                                        SizedBox(width: 10,),



                                        Container(

                                          padding:EdgeInsets.fromLTRB(5,5,5,5),
                                          decoration: BoxDecoration(
                                            color: ColorConstants.defaultBoxColor,
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 0,
                                            ),
                                            borderRadius: BorderRadius.circular(25),
                                          ),
                                          child: Image.asset(
                                            'assets/images/calender.png',width: 16, height: 18,),
                                        )
                                      ],
                                    ),
                                    decoration: BoxDecoration(

                                      color: Colors.blueAccent,

                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ],

                  ),
                ),
              ),
            ),
          )),
    );
  }




}
