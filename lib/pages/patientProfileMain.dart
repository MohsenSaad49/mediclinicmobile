import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/services/appServices/settingsService.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:provider/provider.dart';

import '../serviceLocator.dart';

class PatientProfileMainPage extends StatefulWidget {
  @override
  PatientProfileMainPageState createState() {
    return PatientProfileMainPageState();
  }
}

class PatientProfileMainPageState extends  State<PatientProfileMainPage> {
  PatientProfileMainProvider patientProfileMainProvider   ;
  final settingService = locator<SettingsService>();
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();



  @override
  Widget build(BuildContext context) {
    patientProfileMainProvider = Provider.of<PatientProfileMainProvider>(context, listen: true);

    return Container(
      child:  AppStateHandlerWidget(
        state: patientProfileMainProvider.loadingState,
        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.fromLTRB(10,20,10,10),
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(10.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        GestureDetector(
                                          child: Container(
                                            width: 35, height: 35,
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                            child:IconButton(

                                              icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                              color: ColorConstants.primaryColor,
                                            ),
                                            ),
                                            decoration: BoxDecoration(

                                              color: Colors.blueAccent,

                                              borderRadius: BorderRadius.circular(25),
                                            ),
                                          ),
                                          onTap: (){
                                            Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                          },
                                        ),
                                       patientProfileMainProvider.fromUnRegisteredUser?new Container(): GestureDetector(
                                          child:       Container(

                                            child: RaisedButton(
                                              color:Colors.red,
                                              textColor: ColorConstants.primaryColor,
                                              elevation: 0.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: new BorderRadius.circular(30.0),
                                                  side: BorderSide(
                                                      color: Colors.red)),

                                              child: Text(
                                                "Switch Preference",
                                                style: TextStyle( color: Colors.white,
                                                    fontFamily: UiConstants.segeoRegularFont,
                                                    fontSize: 13, fontWeight: FontWeight.normal
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.pushNamed(context, RoutesConstants.siteSelectionPage,
                                                    arguments: {"fromProvider":false});
                                              },
                                            ),
                                          ),
                                          onTap: (){
                                            patientProfileMainProvider.showDialog();
                                          },
                                        )

                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Patient Profile",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                        GestureDetector(
                                          child: Container(

                                            child: RaisedButton(
                                              color:Colors.transparent,
                                              textColor: ColorConstants.primaryColor,
                                              elevation: 0.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: new BorderRadius.circular(30.0),
                                                  side: BorderSide(
                                                      color: Colors.red)),

                                              child: Text(
                                                "LogOut",
                                                style: TextStyle( color: Colors.red,
                                                    fontFamily: UiConstants.segeoRegularFont,
                                                    fontSize: 13, fontWeight: FontWeight.normal
                                                ),
                                              ),
                                              onPressed: () {

                                                Navigator.pushNamed(context, RoutesConstants.patientLoginPage );
                                              },
                                            ),
                                          ),
                                          onTap: (){

                                          },
                                        )

                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,0,130,0),
                                    child: Divider(color: Colors.grey, thickness: 0.5,),
                                  ),

                                ],
                              ),
                            ),

                            SizedBox(height: 25,),
                            Row(
                              children: <Widget>[
                                Container(
                                  width:250,
                                  decoration:BoxDecoration(
                                    color: Colors.blue,
                                    border: Border.all(
                                      color: Colors.blue,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                        child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                      ),
                                      Text("Login as",style: TextStyle(color: Colors.white,fontSize: 15),)
                                    ],
                                  ),

                                ),
                              ],
                            ),
                            SizedBox(height: 15,),

                            Padding(
                              padding: const EdgeInsets.only(left: 15,right: 15),
                              child:           Column(
                                children: <Widget>[

                                  Row(
                                    children: <Widget>[
                                      Text('First name',
                                          style: TextStyle( color: Colors.blueGrey[700],
                                              fontFamily: UiConstants.segeoLightFont,
                                              fontSize: 12, fontWeight: FontWeight.normal
                                          )),
                                      SizedBox(width: 100,),
                                      Text(
                                        settingService.firstname==null?"":settingService.firstname,
                                        style: TextStyle( color: Colors.blueGrey[700],
                                            fontFamily: UiConstants.segeoLightFont,
                                            fontSize: 13, fontWeight: FontWeight.bold
                                        ),
                                      ),
                                    ],
                                  ),

                                  SizedBox(height: 5,),
                                  Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15,right: 15),
                              child:           Column(
                                children: <Widget>[

                                  Row(
                                    children: <Widget>[
                                      Text('Surname  ',
                                          style: TextStyle( color: Colors.blueGrey[700],
                                              fontFamily: UiConstants.segeoLightFont,
                                              fontSize: 12, fontWeight: FontWeight.normal
                                          )),
                                      SizedBox(width: 100,),
                                      Text(
                                        settingService.surname==null?"":settingService.surname,
                                        style: TextStyle( color: Colors.blueGrey[700],
                                            fontFamily: UiConstants.segeoLightFont,
                                            fontSize: 13, fontWeight: FontWeight.bold
                                        ),
                                      ),
                                    ],
                                  ),

                                  SizedBox(height: 5,),
                                  Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                ],
                              ),
                            ),


                            SizedBox(height: 30,),
                            patientProfileMainProvider.fromUnRegisteredUser?new Container():   Row(
                              children: <Widget>[
                                Container(
                                  width:250,
                                  decoration:BoxDecoration(
                                    color: Colors.blue,
                                    border: Border.all(
                                      color: Colors.blue,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                        child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                      ),
                                      Text("Registered Clinic",style: TextStyle(color: Colors.white,fontSize: 15),)
                                    ],
                                  ),

                                ),
                              ],
                            ),
                            patientProfileMainProvider.fromUnRegisteredUser?new Container():  SizedBox(height: 30,),
                            patientProfileMainProvider.fromUnRegisteredUser?new Container():   Container(
                              height: 150,
                              child: AppStateHandlerWidget(
                                state: patientProfileMainProvider.loadingState,
                                child: ListView.builder(
                                    padding: EdgeInsets.only(top: 10,bottom: 10),
                                    itemCount: 3,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                            child:           Column(
                                              children: <Widget>[

                                                Row(
                                                  children: <Widget>[

                                                    Text(
                                                      "Healthy Life Clinic West",
                                                      style: TextStyle( color: Colors.blueGrey[700],
                                                          fontFamily: UiConstants.segeoLightFont,
                                                          fontSize: 13, fontWeight: FontWeight.bold
                                                      ),
                                                    ),
                                                  ],
                                                ),

                                                SizedBox(height: 5,),
                                                Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
