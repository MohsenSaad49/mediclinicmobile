import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/patientContactProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';

class PatientContactPage extends StatefulWidget {
  @override
  PatientContactPageState createState() {
    return PatientContactPageState();
  }
}

class PatientContactPageState extends  State<PatientContactPage> {
  PatientContactProvider  patientContactProvider   ;
  bool show=false;
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  @override
  Widget build(BuildContext context) {
    patientContactProvider = Provider.of<PatientContactProvider>(context, listen: true);

    return Scaffold(
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:ListView(
                children: <Widget>[
                  Container(
                    decoration:BoxDecoration(
                      color: Colors.white30,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding:EdgeInsets.fromLTRB(10,20,10,10),
                          decoration:BoxDecoration(
                            color: ColorConstants.mainContainersColor,
                            border: Border.all(
                              color: Colors.blue,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15,0,0,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    GestureDetector(
                                      child:Container(
                                        width: 35, height: 35,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                          child:IconButton(

                                            icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                            color: ColorConstants.primaryColor,
                                          ),
                                        ),
                                        decoration: BoxDecoration(

                                          color: Colors.blueAccent,

                                          borderRadius: BorderRadius.circular(25),
                                        ),
                                      ),
                                      onTap: (){
                                        Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                      },
                                    ),


                                  ],
                                ),
                              ),
                              SizedBox(height: 10,),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Contact",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),


                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,130,0),
                                child: Divider(color: Colors.grey, thickness: 0.5,),
                              ),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        width:300,
                                        child: Text("You can contact the clinic staff directly from ouraction button.",maxLines: 2,style: TextStyle(fontSize: 13, color:Colors.black,fontWeight: FontWeight.normal, fontFamily: UiConstants.segeoRegularFont),)),


                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),

                        SizedBox(height: 25,),
                        Container(
                          height: 400,
                          child: AppStateHandlerWidget(
                            state: patientContactProvider.loadingState,
                            child: ListView.builder(
                                itemCount: patientContactProvider.allContacts==null?0:patientContactProvider.allContacts.length,
                                itemBuilder: (context, index) {
                                  return   GestureDetector(
                                    onTap: (){
                                      // Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                    },


                                    child:  Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        decoration:BoxDecoration(
                                          color:Colors.white,
                                          border: Border.all(
                                            color: ColorConstants.actionsColor,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Text(patientContactProvider.allContacts[index].clinicName,style: TextStyle(fontSize: 14,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),maxLines: 2,),

                                                        ],
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[
                                                            Row(children: <Widget>[
                                                              Icon(Icons.location_city,color: ColorConstants.actionsColor,size: 18,),
                                                              SizedBox(width: 5,),
                                                              Padding(
                                                                padding: const EdgeInsets.only(top:5),
                                                                child: Container( width: 130, child: Text((patientContactProvider.allContacts[index].addressLine1==null?"":patientContactProvider.allContacts[index].addressLine1)+patientContactProvider.allContacts[index].street,style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                              )
                                                            ],),

                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[
                                                            Row(children: <Widget>[
                                                              Image.asset(
                                                                "assets/images/global.png", height: 18, width: 18,
                                                              ),
                                                              SizedBox(width: 5,),
                                                              Padding(
                                                                padding: const EdgeInsets.only(top:5),
                                                                child: Container( width: 130, child: Text(patientContactProvider.allContacts[index].webUrl==null?"":patientContactProvider.allContacts[index].webUrl,style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                              )
                                                            ],),

                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    children: <Widget>[

                                                      SizedBox(height: 30,),
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[

                                                            Row(children: <Widget>[
                                                              Icon(Icons.phone_in_talk,color: ColorConstants.actionsColor,size: 18,),
                                                              SizedBox(width: 5,),
                                                              Container( width: 120, child: Text(patientContactProvider.allContacts[index].phone==null?"":patientContactProvider.allContacts[index].phone,style: TextStyle(fontSize: 10),maxLines: 2,))
                                                            ],),

                                                            SizedBox(height: 5,),


                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: 15,),
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[

                                                            Row(children: <Widget>[
                                                              Icon(Icons.mail_outline,color: ColorConstants.actionsColor,size: 18,),
                                                              SizedBox(width: 5,),
                                                              Container(width: 120, child: Text(patientContactProvider.allContacts[index].email==null?"":patientContactProvider.allContacts[index].email,style: TextStyle(fontSize: 10),maxLines: 2,))
                                                            ],),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),


                                                ],
                                              ),
                                            ),

                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                              child: Divider(color: Colors.blueAccent, thickness: 0.5,),
                                            ),
                                            GestureDetector(
                                              onTap: (){

                                                showModalBottomSheet(
                                                    context: context,
                                                    isScrollControlled:
                                                    true,
                                                    elevation: 1,
                                                    isDismissible: true,
                                                    backgroundColor:
                                                    Colors
                                                        .white,
                                                    builder: (builder) {

                                                      return Container(
                                                        height: 300,
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                              child:           Column(
                                                                children: <Widget>[

                                                                  Row(
                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: ()async{
                                                                         patientContactProvider.getDirection(patientContactProvider.allContacts[index].latitude, patientContactProvider.allContacts[index].longitude, patientContactProvider.allContacts[index].clinicName);
                                                                        },
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: Text(
                                                                            "GetDirection",
                                                                            style: TextStyle( color: Colors.blueGrey[700],
                                                                                fontFamily: UiConstants.segeoLightFont,
                                                                                fontSize: 18, fontWeight: FontWeight.bold
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      GestureDetector(
                                                                          onTap: (){Navigator.pop(context);},
                                                                          child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                    ],
                                                                  ),

                                                                  SizedBox(height: 5,),
                                                                  Padding(
                                                                    padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                    child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                  ),

                                                                  Row(
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: (){
                                                                          patientContactProvider.callClinic(patientContactProvider.allContacts[index].phone);
                                                                        },
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: Text(
                                                                            "Call Clinic",
                                                                            style: TextStyle( color: Colors.green,
                                                                                fontFamily: UiConstants.segeoLightFont,
                                                                                fontSize: 18, fontWeight: FontWeight.bold
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(height: 5,),
                                                                  Padding(
                                                                    padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                    child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                  ),
                                                                  Row(
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: () async{
                                                                          patientContactProvider.sendSms(patientContactProvider.allContacts[index].phone);
                                                                        },
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: Text(
                                                                            "SMS",
                                                                            style: TextStyle( color: Colors.red,
                                                                                fontFamily: UiConstants.segeoLightFont,
                                                                                fontSize: 18, fontWeight: FontWeight.bold
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(height: 5,),
                                                                  Padding(
                                                                    padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                    child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                  ),
                                                                  Row(
                                                                    children: <Widget>[
                                                                      GestureDetector(
                                                                        onTap: ()async{
                                                                          patientContactProvider.sendEmail(patientContactProvider.allContacts[index].email);
                                                                        },
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: Text(
                                                                            "Email",
                                                                            style: TextStyle( color:ColorConstants.darkBluecolor,
                                                                                fontFamily: UiConstants.segeoLightFont,
                                                                                fontSize: 18, fontWeight: FontWeight.bold
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),

                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      );

                                                    }
                                                );

                                              },
                                              child: Container(

                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Column(
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: <Widget>[
                                                        Text("Actions ",style: TextStyle(fontSize: 15,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                                                        Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),

                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),

                        Divider(color: Colors.blueAccent, thickness: 2,),

                        GestureDetector(
                          onTap: (){
                            //patientBookingProvider.showDialog();
                          },
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                DefaultIconButton(
                                  labelText: 'Bookings',
                                  iconWidget: Image.asset(
                                    'assets/images/calender.png',width: 20, height: 23,),
                                  onPressed: () {
                                    Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                  },
                                ),
                                DefaultIconButton(
                                  labelText: 'Invoices',
                                  iconWidget: Image.asset(
                                    'assets/images/invoices2.png',width: 20, height: 23,),
                                  onPressed: () {
                                    // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                  },
                                ),
                                DefaultIconButton(
                                  labelText: 'Contact',
                                  iconWidget: Image.asset(
                                    'assets/images/comment.png',width: 20, height: 23,),
                                  onPressed: () async {
                                    //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                    //onNavigate(true);

                                  },
                                ),
                                DefaultIconButton(
                                  labelText: 'Profile',
                                  iconWidget: Image.asset(
                                    'assets/images/user2.png',width: 20, height: 23,),
                                  onPressed: () {
                                    Navigator.pushNamed(context, RoutesConstants.patientProfileMainPage);
                                  },
                                ),
                              ],
                            ),
                          ),
                        )

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
