import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/main.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/registerNewPatientProvider.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class RegisterNewPatientPage extends StatefulWidget {
  @override
  RegisterNewPatientPageState createState() {
    return RegisterNewPatientPageState();
  }
}
class RegisterNewPatientPageState extends  State<RegisterNewPatientPage> {
  RegisterNewPatientProvider registerNewPatientProvider;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String selectedTitle;
  String selectedGender;
  String numberType;
  String countryCode;
  String date = "Date of Birth";
  double entryHeight=50;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)

      setState(() {
        selectedDate = picked;
        date =selectedDate.year.toString()+"-"+ selectedDate.month.toString() +"-" +selectedDate.day.toString() ;

        registerNewPatientProvider.dateOfBirth=date;
      });
  }

  @override
  Widget build(BuildContext context) {
    registerNewPatientProvider = Provider.of<RegisterNewPatientProvider>(context, listen: true);

    return Form(
      key: _formKey,
      child: new Scaffold(
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:Column(
                  children: <Widget>[
                    Container(
                      height:155,
                      padding:EdgeInsets.fromLTRB(10,40,10,10),
                      decoration:BoxDecoration(
                        color: Colors.transparent,
                        border: Border.all(
                          color: Colors.blue,
                          width: 2,
                        ),
                        borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(45, 50)),
                      ),
                      child: Column(
                        children: <Widget>[

                          Row(
                            children: <Widget>[
                              GestureDetector(
                                onTap: (){
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: 35, height: 35,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                    child:IconButton(

                                      icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                      color: ColorConstants.primaryColor,
                                    ),
                                  ),
                                  decoration: BoxDecoration(

                                    color: Colors.blueAccent,

                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20,0,10,10),
                                child: Text("Sign up",style: TextStyle(fontSize: 30,color: Colors.blueGrey, fontWeight: FontWeight.bold),),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(60,0,90,10),
                            child: Divider(height: 5,thickness: 2,color: Colors.blue,),
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width:330,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(60,0,10,10),
                                  child: Text("We need some information from you to make an account.",

                                    overflow: TextOverflow.clip,
                                    style: TextStyle(fontSize: 12,color: Colors.black45),),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    Container(
                      height: MediaQuery.of(context).size.height-155,
                      child: ListView(
                        children: <Widget>[
                         Column(children: <Widget>[
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,0,10,0),
                               child: Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   Expanded(
                                     child:   Container(
height:entryHeight,
                                       padding:  const EdgeInsets.fromLTRB(10,0,5,0),
                                       decoration: BoxDecoration(
                                         color: Colors.transparent,
                                         border: Border.all(
                                           color: Colors.grey,
                                           width: 1.5,
                                         ),
                                         borderRadius: BorderRadius.circular(30),
                                       ),
                                       child: DropdownButtonHideUnderline(
                                         child: new DropdownButton<String>(
                                           value: selectedTitle,
                                           icon:  Icon(Icons.keyboard_arrow_down,size: 35, color: Colors.blueAccent,),
                                           hint: Row(
                                             children: <Widget>[
                                               Text('Title'),

                                             ],
                                           ),
                                           items: <String>['Mr', 'Mrs',].map((String value) {
                                             return new DropdownMenuItem<String>(
                                               value: value,
                                               child: new Text(value),
                                             );
                                           }).toList(),
                                           onChanged: (value) {
                                             setState(() {
                                               selectedTitle=value;
                                             });
                                           },
                                         ),
                                       ),
                                     ),
                                   ),
                                   SizedBox(width: 10,),
                                   Expanded(
                                     child: Container(

                                       height: entryHeight,
                                       child: new TextFormField(
                                         onChanged: (val) => registerNewPatientProvider.username = val,
                                         onSaved: (val) => registerNewPatientProvider.username = val,
                                         validator: (value) {
                                           if (value.isEmpty) {
                                             return 'username can not be blank';
                                           }

                                           return null;
                                         },
                                         decoration: new InputDecoration(
                                             labelText: "Username",
                                             enabled: true,

                                             border: new OutlineInputBorder( gapPadding: 0.5,
                                               borderRadius: const BorderRadius.all(
                                                 const Radius.circular(30.0),
                                               ),
                                             ),

                                             filled: false, alignLabelWithHint: false, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                             hintStyle: UiConstants.hintTextStyle,
                                             hintText: "Username",
                                             fillColor: ColorConstants.lightgray),
                                       ),
                                     ),
                                   ),

                                 ],
                               ),
                             ),
                           ),
                           SizedBox(height: 30,),
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,0,10,0),
                               child: Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   Expanded(
                                     child:   Container(
                                       height: entryHeight,
                                       child: new TextFormField(
                                         onChanged: (val) => registerNewPatientProvider.surname = val,
                                         onSaved: (val) => registerNewPatientProvider.surname = val,
                                         validator: (value) {
                                           if (value.isEmpty) {
                                             return 'Middle name can not be blank';
                                           }

                                           return null;
                                         },
                                         decoration: new InputDecoration(
                                             labelText: "Middle name",
                                             focusColor:    Colors.white,
                                             hoverColor: Colors.white,
                                             border: new OutlineInputBorder(
                                               borderRadius: const BorderRadius.all(
                                                 const Radius.circular(30.0),
                                               ),
                                             ),
                                             filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                             hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                             hintText: "Middle name",
                                             fillColor: ColorConstants.lightgray),
                                       ),
                                     ),
                                   ),
                                   SizedBox(width: 10,),
                                   Expanded(
                                     child: Container(
                                       height: entryHeight,
                                       child: new TextFormField(
                                         onChanged: (val) => registerNewPatientProvider.surname = val,
                                         onSaved: (val) => registerNewPatientProvider.surname = val,
                                         validator: (value) {
                                           if (value.isEmpty) {
                                             return 'surname can not be blank';
                                           }

                                           return null;
                                         },
                                         decoration: new InputDecoration(
                                             labelText: "Surname",
                                             focusColor:    Colors.white,
                                             hoverColor: Colors.white,
                                             border: new OutlineInputBorder(
                                               borderRadius: const BorderRadius.all(
                                                 const Radius.circular(30.0),
                                               ),
                                             ),
                                             filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                             hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                             hintText: "Surname",
                                             fillColor: ColorConstants.lightgray),
                                       ),
                                     ),
                                   ),

                                 ],
                               ),
                             ),
                           ),
                           SizedBox(height: 30,),
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,0,10,0),
                               child: Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   Expanded(
                                     child:   Container(
                                       height:entryHeight,
                                       padding:  const EdgeInsets.fromLTRB(10,0,5,0),
                                       decoration: BoxDecoration(
                                         color: Colors.transparent,
                                         border: Border.all(
                                           color: Colors.grey,
                                           width: 1.5,
                                         ),
                                         borderRadius: BorderRadius.circular(30),
                                       ),
                                       child: DropdownButtonHideUnderline(
                                         child: new DropdownButton<String>(
                                           value: selectedGender,
                                           icon:  Icon(Icons.keyboard_arrow_down,size: 35, color: Colors.blueAccent,),
                                           hint: Row(
                                             children: <Widget>[
                                               Text('Gender'),

                                             ],
                                           ),
                                           items: <String>['Male', 'Female',].map((String value) {
                                             return new DropdownMenuItem<String>(
                                               value: value,
                                               child: new Text(value),
                                             );
                                           }).toList(),
                                           onChanged: (value) {
                                             setState(() {
                                               selectedGender=value;
                                             });
                                           },
                                         ),
                                       ),
                                     ),
                                   ),
SizedBox(width: 10,),
                                   Expanded(
                                     child: GestureDetector(
                                       onTap: () => _selectDate(context),
                                       child:Container(
                                         height: entryHeight,
                                         decoration: BoxDecoration(
                                           color: Colors.white,
                                           border: Border.all(
                                             color: Colors.black45,
                                             width: 1,
                                           ),
                                           borderRadius: BorderRadius.circular(30),
                                         ),
                                         child: Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           children: <Widget>[
                                             Padding(
                                               padding: const EdgeInsets.fromLTRB(15, 5, 0, 15),
                                               child: Text(date,
                                                   style: TextStyle(
                                                     color: Colors.black,
                                                     fontSize: 16,
                                                     fontFamily: UiConstants.segeoLightFont,
                                                     fontWeight: FontWeight.normal,
                                                     decorationStyle: TextDecorationStyle.wavy,
                                                   )),
                                             ),
                                             Padding(
                                               padding: const EdgeInsets.fromLTRB(0, 2, 5, 0),
                                               child: Icon(Icons.keyboard_arrow_down,color: Colors.blueAccent,size: 35,),
                                             )
                                           ],
                                         ),
                                       ),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                           SizedBox(height: 30,),
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,0,10,0),
                               child: Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   Expanded(
                                     child:   Container(
                                       height:entryHeight,
                                       padding:  const EdgeInsets.fromLTRB(10,0,5,0),
                                       decoration: BoxDecoration(
                                         color: Colors.transparent,
                                         border: Border.all(
                                           color: Colors.grey,
                                           width: 1.5,
                                         ),
                                         borderRadius: BorderRadius.circular(30),
                                       ),
                                       child: DropdownButtonHideUnderline(
                                         child: new DropdownButton<String>(
                                           value: numberType,
                                           icon:  Icon(Icons.keyboard_arrow_down,size: 35, color: Colors.blueAccent,),
                                           hint: Row(
                                             children: <Widget>[
                                               Text('Number Type'),

                                             ],
                                           ),
                                           items: <String>['Home', 'Work',].map((String value) {
                                             return new DropdownMenuItem<String>(
                                               value: value,
                                               child: new Text(value),
                                             );
                                           }).toList(),
                                           onChanged: (value) {
                                             setState(() {
                                               numberType=value;
                                             });
                                           },
                                         ),
                                       ),
                                     ),
                                   ),
                                   SizedBox(width: 10,),
                                   Expanded(
                                     child:   Container(
                                       height:entryHeight,
                                       padding:  const EdgeInsets.fromLTRB(10,0,5,0),
                                       decoration: BoxDecoration(
                                         color: Colors.transparent,
                                         border: Border.all(
                                           color: Colors.grey,
                                           width: 1.5,
                                         ),
                                         borderRadius: BorderRadius.circular(30),
                                       ),
                                       child: DropdownButtonHideUnderline(
                                         child: new DropdownButton<String>(
                                           value: countryCode,
                                           icon:  Icon(Icons.keyboard_arrow_down,size: 35, color: Colors.blueAccent,),
                                           hint: Row(
                                             children: <Widget>[
                                               Text('Country code'),

                                             ],
                                           ),
                                           items: <String>['Egypt +02', 'Denmark +45',"Spain +34"].map((String value) {
                                             return new DropdownMenuItem<String>(
                                               value: value,
                                               child: new Text(value),
                                             );
                                           }).toList(),
                                           onChanged: (value) {
                                             setState(() {
                                               countryCode=value;
                                             });
                                           },
                                         ),
                                       ),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,30,10,0),
                               child: Container(
                                 height: entryHeight,
                                 child:new TextFormField(
                                   onChanged: (val) => registerNewPatientProvider.mobileNumber = val,
                                   onSaved: (val) => registerNewPatientProvider.mobileNumber = val,
                                   validator: (value) {
                                     if (value.isEmpty) {
                                       return 'Phone number can not be blank';
                                     }
                                     if ( !isNumeric(value)) {
                                       return 'phone number must be supplied';
                                     }
                                     return null;
                                   },
                                   keyboardType: TextInputType.phone,
                                   decoration: new InputDecoration(
                                       labelText: "Phone number",

                                       border: new OutlineInputBorder(
                                         borderRadius: const BorderRadius.all(
                                           const Radius.circular(30.0),
                                         ),
                                       ),
                                       filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                       hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                       hintText: "Phone number",
                                       fillColor: ColorConstants.lightgray),
                                 ),
                               ),
                             ),
                           ),
                           Center(
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(10,30,10,20),
                               child: Container(
                                 height: 50,
                                 child: new TextFormField(
                                   onChanged: (val) => registerNewPatientProvider.email = val,
                                   onSaved: (val) => registerNewPatientProvider.email = val,
                                   validator: (value) {
                                     if (value.isEmpty) {
                                       return 'Email can not be blank';
                                     }
                                     if (!isEmail(value) ) {
                                       return 'Email must be supplied';
                                     }
                                     return null;
                                   },
                                   decoration: new InputDecoration(
                                       labelText: "Email Address",

                                       border: new OutlineInputBorder(
                                         borderRadius: const BorderRadius.all(
                                           const Radius.circular(30.0),
                                         ),
                                       ),
                                       filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                       hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                       hintText: "Email Address",
                                       fillColor: ColorConstants.lightgray),
                                 ),
                               ),
                             ),
                           ),
                           Center(
                             child: Container(
                               width: 165,
                               child: Padding(
                                 padding: const EdgeInsets.fromLTRB(0,10,5,20),
                                 child:   RaisedButton(
                                   color:Colors.blue,
                                   textColor: ColorConstants.primaryColor,
                                   elevation: 0.0,
                                   shape: RoundedRectangleBorder(
                                       borderRadius: new BorderRadius.circular(30.0),
                                       side: BorderSide(
                                           color: Colors.blue)),

                                   child: Row(
                                     children: <Widget>[
                                       Text(
                                         "Create an account ",
                                         style: TextStyle( color: Colors.white,
                                             fontFamily: UiConstants.segeoLightFont,
                                             fontSize: 16, fontWeight: FontWeight.bold
                                         ),
                                       ),
                                    /*   Icon(Icons.search,size: 15, color: Colors.white,),*/
                                     ],
                                   ),
                                   onPressed: () {
                                  /*   if(_formKey.currentState.validate()) {
                                    //   entryHeight = 40;
                                      *//* registerNewPatientProvider
                                           .patientLogin();*//*
                                     }
                                     else{
                                       setState(() {
                                       //  entryHeight=60;
                                       });
                                     }*/
                                     Navigator.pushNamed(context, RoutesConstants.registerPatientWithoutDataPage);
                                   },
                                 ),
                               ),
                             ),
                           ),
                         ],)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
