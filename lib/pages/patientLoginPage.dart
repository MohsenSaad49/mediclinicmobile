import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/main.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class PatientLoginPage extends StatefulWidget {
  @override
  PatientLoginPageState createState() {
    return PatientLoginPageState();
  }
}
class PatientLoginPageState extends  State<PatientLoginPage> {
  LoginProvider loginProvider;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String date = "Date of Birth";

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)

      setState(() {
        selectedDate = picked;
        date =selectedDate.year.toString()+"-"+ selectedDate.month.toString() +"-" +selectedDate.day.toString() ;

        loginProvider.dateOfBirth=date;
      });
  }

  @override
  Widget build(BuildContext context) {
   loginProvider = Provider.of<LoginProvider>(context, listen: false);
    return new Scaffold(
   //   backgroundColor: ColorConstants.mainContainersColor.withOpacity(0.5),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:Column(
                children: <Widget>[
                  Container(
                    padding:EdgeInsets.fromLTRB(10,20,10,10),
                    decoration:BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(
                        color: Colors.blue,

                        width: 2,
                      ),
                      borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                    ),
                    child: Column(
                      children: <Widget>[
                        /*       Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 30,),
                                ),
                              ],
                            ),*/
                        SizedBox(height: 30,),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(40,0,10,10),
                              child: Text("App Registration",style: TextStyle(fontSize: 30,color: Colors.blueGrey, fontWeight: FontWeight.bold),),
                            ),

                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(40,0,90,10),
                          child: Divider(height: 5,thickness: 2,color: Colors.blue,),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width:300,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(40,0,10,10),
                                child: Text("We will need to search our database for your clinic. Please fill in information below so we may assist you ",

                                  overflow: TextOverflow.clip,
                                  style: TextStyle(fontSize: 12,color: Colors.black45),),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 25,),
                  Row(
                    children: <Widget>[
                      Container(
                        width:300,
                        decoration:BoxDecoration(
                          color: Colors.blue,
                          border: Border.all(
                            color: Colors.blue,
                            width: 2,
                          ),
                          borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                        ),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(Icons.radio_button_checked,color: Colors.white,),
                            ),
                            Text("Account Details",style: TextStyle(color: Colors.white,fontSize: 20),)
                          ],
                        ),

                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text("Please click 'Search' after filled details.",style: TextStyle(color: Colors.black,fontSize: 17,fontFamily: UiConstants.segeoLightFont),)
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height-300,
                    child: ListView(
                      children: <Widget>[
                       Column(children: <Widget>[
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,0,40,0),
                             child: Container(

                               height: 50,
                               child: new TextFormField(
                                 onChanged: (val) => loginProvider.username = val,
                                 onSaved: (val) => loginProvider.username = val,
                                 validator: (value) {
                                   if (value.isEmpty) {
                                     return 'username can not be blank';
                                   }

                                   return null;
                                 },
                                 decoration: new InputDecoration(
                                     labelText: "Username",
                                     enabled: true,

                                     border: new OutlineInputBorder( gapPadding: 0.5,
                                       borderRadius: const BorderRadius.all(
                                         const Radius.circular(30.0),
                                       ),
                                     ),

                                     filled: false, alignLabelWithHint: false, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                     hintStyle: UiConstants.hintTextStyle,
                                     hintText: "Username",
                                     fillColor: ColorConstants.lightgray),
                               ),
                             ),
                           ),
                         ),
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,20,40,0),
                             child: Container(
                               height: 50,
                               child: new TextFormField(
                                 onChanged: (val) => loginProvider.surname = val,
                                 onSaved: (val) => loginProvider.surname = val,
                                 validator: (value) {
                                   if (value.isEmpty) {
                                     return 'surname can not be blank';
                                   }

                                   return null;
                                 },
                                 decoration: new InputDecoration(
                                     labelText: "Surname",
                                     focusColor:    Colors.white,
                                     hoverColor: Colors.white,
                                     border: new OutlineInputBorder(
                                       borderRadius: const BorderRadius.all(
                                         const Radius.circular(30.0),
                                       ),
                                     ),
                                     filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                     hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                     hintText: "Surname",
                                     fillColor: ColorConstants.lightgray),
                               ),
                             ),
                           ),
                         ),
                         Row(
                           children: <Widget>[
                             Expanded(
                               child: GestureDetector(
                                 onTap: () => _selectDate(context),
                                 child: Padding(
                                   padding: const EdgeInsets.fromLTRB(30,20,40,0),
                                   child: Container(

                                     decoration: BoxDecoration(
                                       color: Colors.white,
                                       border: Border.all(
                                         color: Colors.black45,
                                         width: 1,
                                       ),
                                       borderRadius: BorderRadius.circular(30),
                                     ),
                                     child: Row(
                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                       children: <Widget>[
                                         Padding(
                                           padding: const EdgeInsets.fromLTRB(15, 15, 50, 15),
                                           child: Text(date,
                                               style: TextStyle(
                                                 color: Colors.black,
                                                 fontSize: 16,
                                                 fontFamily: UiConstants.segeoLightFont,
                                                 fontWeight: FontWeight.normal,
                                                 decorationStyle: TextDecorationStyle.wavy,
                                               )),
                                         ),
                                         Padding(
                                           padding: const EdgeInsets.all(8.0),
                                           child: Icon(Icons.calendar_today,color: Colors.blueAccent,size: 30,),
                                         )
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ],
                         ),
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,20,40,0),
                             child: Container(
                               height: 50,
                               child:new TextFormField(
                                 onChanged: (val) => loginProvider.mobileNumber = val,
                                 onSaved: (val) => loginProvider.mobileNumber = val,
           /*                      validator: (value) {
                                   if (value.isEmpty) {
                                     return 'Mobile number can not be blank';
                                   }
                                   if ( !isNumeric(value)) {
                                     return 'phone number must be supplied';
                                   }
                                   return null;
                                 },*/
                                 keyboardType: TextInputType.phone,
                                 decoration: new InputDecoration(
                                     labelText: "Mobile number",

                                     border: new OutlineInputBorder(
                                       borderRadius: const BorderRadius.all(
                                         const Radius.circular(30.0),
                                       ),
                                     ),
                                     filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                     hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                     hintText: "Mobile number",
                                     fillColor: ColorConstants.lightgray),
                               ),
                             ),
                           ),
                         ),
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,20,40,0),
                             child: Container(
                               height: 50,
                               child:new TextFormField(
                                 onChanged: (val) => loginProvider.homeNumber = val,
                                 onSaved: (val) => loginProvider.homeNumber = val,
                        /*         validator: (value) {
                                   if (value.isEmpty) {
                                     return 'Home number can not be blank';
                                   }
                                   if ( !isNumeric(value)) {
                                     return 'Home number must be supplied';
                                   }
                                   return null;
                                 },*/
                                 keyboardType: TextInputType.phone,
                                 decoration: new InputDecoration(
                                     labelText: "Home number",

                                     border: new OutlineInputBorder(
                                       borderRadius: const BorderRadius.all(
                                         const Radius.circular(30.0),
                                       ),
                                     ),
                                     filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                     hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                     hintText: "Home number",
                                     fillColor: ColorConstants.lightgray),
                               ),
                             ),
                           ),
                         ),
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,20,40,20),
                             child: Container(
                               height: 50,
                               child: new TextFormField(
                                 onChanged: (val) => loginProvider.email = val,
                                 onSaved: (val) => loginProvider.email = val,
                                 validator: (value) {
                                   if (value.isEmpty) {
                                     return 'Email can not be blank';
                                   }
                                   if (!isEmail(value) ) {
                                     return 'Email must be supplied';
                                   }
                                   return null;
                                 },
                                 decoration: new InputDecoration(
                                     labelText: "Email",

                                     border: new OutlineInputBorder(
                                       borderRadius: const BorderRadius.all(
                                         const Radius.circular(30.0),
                                       ),
                                     ),
                                     filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                     hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                     hintText: "Email",
                                     fillColor: ColorConstants.lightgray),
                               ),
                             ),
                           ),
                         ),
                         Center(
                           child: Container(
                             width: 120,
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(0,5,5,0),
                               child:   RaisedButton(
                                 color:Colors.transparent,
                                 textColor: ColorConstants.primaryColor,
                                 elevation: 0.0,
                                 shape: RoundedRectangleBorder(
                                     borderRadius: new BorderRadius.circular(30.0),
                                     side: BorderSide(
                                       width: 1.5,
                                         color: Colors.black)),

                                 child: Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                   children: <Widget>[
                                     Text(
                                       "Search ",
                                       style: TextStyle( color: Colors.blue,
                                           fontFamily: UiConstants.segeoLightFont,
                                           fontSize: 16, fontWeight: FontWeight.bold
                                       ),
                                     ),
                                     Icon(Icons.search,size: 22, color: Colors.blueAccent,),
                                   ],
                                 ),
                                 onPressed: () {
                                   loginProvider.patientLogin();
                                 },
                               ),
                             ),
                           ),
                         ),
                         Padding(
                           padding: const EdgeInsets.fromLTRB(40,10,40,0),
                           child: Divider(height: 1,thickness: 0.5,color: Colors.black,),
                         ),
                         Center(
                           child: Container(
                             width: 165,
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(0,10,5,20),
                               child:   RaisedButton(
                                 color:Colors.blue,
                                 textColor: ColorConstants.primaryColor,
                                 elevation: 0.0,
                                 shape: RoundedRectangleBorder(
                                     borderRadius: new BorderRadius.circular(30.0),
                                     side: BorderSide(
                                         color: Colors.blue)),

                                 child: Row(
                                   children: <Widget>[
                                     Text(
                                       "Create an account ",
                                       style: TextStyle( color: Colors.white,
                                           fontFamily: UiConstants.segeoLightFont,
                                           fontSize: 16, fontWeight: FontWeight.bold
                                       ),
                                     ),
                                     /*   Icon(Icons.search,size: 15, color: Colors.white,),*/
                                   ],
                                 ),
                                 onPressed: () {
                                        Navigator.pushNamed(context, RoutesConstants.registerNewPatientPage);
                                 },
                               ),
                             ),
                           ),
                         ),
                       ],)

                      ],

                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
