import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/forms/appointmentClinicForm.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/providers/siteSelectionProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:provider/provider.dart';
import 'package:simple_tooltip/simple_tooltip.dart';

class SiteSelectionPage extends StatefulWidget {
  @override
  SiteSelectionPageState createState() {
    return SiteSelectionPageState();
  }
}

class SiteSelectionPageState extends  State<SiteSelectionPage> {
  SiteSelectionProvider siteSelectionProvider   ;
  bool show=false;
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();


  @override
  Widget build(BuildContext context) {
    siteSelectionProvider = Provider.of<SiteSelectionProvider>(context, listen: true);

    return Scaffold(
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:ListView(
                children: <Widget>[
                  Container(
                    decoration:BoxDecoration(
                      color: Colors.white30,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding:EdgeInsets.fromLTRB(10,20,10,10),
                          decoration:BoxDecoration(
                            color: ColorConstants.mainContainersColor,
                            border: Border.all(
                              color: Colors.blue,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    GestureDetector(
                                      child:Padding(
                                        padding: const EdgeInsets.only(right:0.0),
                                        child:  Image.asset(
                                          "assets/images/arrowleft.png", width: 30, height: 25,
                                        ),
                                      ),
                                      onTap: (){
                                        Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                      },
                                    ),
                                    GestureDetector(
                                      child:Container(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(top:10),
                                              child: Container(  child: Text('Why select preference',style: TextStyle(fontSize: 13,fontFamily: UiConstants.segeoRegularFont),maxLines: 2,)),
                                            ),

                                            SimpleTooltip(
                                              tooltipTap: () {
                                                print("Tooltip tap");
                                              },
                                              animationDuration: Duration(milliseconds: 500),
                                              show: show, backgroundColor: Colors.blueAccent, borderColor: Colors.transparent,
                                              tooltipDirection: TooltipDirection.left, arrowTipDistance: 2,
                                              child: IconButton(

                                                icon:Icon(Icons.help, color: Colors.blueAccent,size: 40, ),
                                                color: ColorConstants.primaryColor,
                                              ),
                                              content: Text(
                                                "our system found you registered in multiple Clinic groups , please select the clinic group to manage your bookin",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 13, fontFamily: UiConstants.segeoLightFont,
                                                  decoration: TextDecoration.none,
                                                ),
                                              ),
                                            ),


                                          ],),
                                      ),
                                      onTap: (){
                                        setState(() {
                                          show=!show;
                                        });
                                      },
                                    )

                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Select Preference",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),


                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,130,0),
                                child: Divider(color: Colors.grey, thickness: 0.5,),
                              ),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        width:300,
                                        child: Text("Select Clinic group to manage your booking, invoices or contact clinic staff.",maxLines: 2,style: TextStyle(fontSize: 13, color:Colors.black,fontWeight: FontWeight.normal, fontFamily: UiConstants.segeoRegularFont),)),


                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                        SizedBox(height: 25,),
                        Container(
                          height: 400,
                          child: AppStateHandlerWidget(
                            state: siteSelectionProvider.loadingState,
                            child: ListView.builder(
                                itemCount: siteSelectionProvider.patientSites==null?0:siteSelectionProvider.patientSites.length,
                                itemBuilder: (context, index) {
                                  return  GestureDetector(
                                    onTap: (){
                                      if(siteSelectionProvider.fromProvider)
                                        Navigator.pushNamed(context, RoutesConstants.providerMainMenuPage);
                                      else
                                        Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8,8,8,0),
                                      child: Container(
                                        padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                        decoration:BoxDecoration(
                                          color:Colors.white,
                                          border: Border.all(
                                            color: ColorConstants.actionsColor,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.circular(15),
                                        ),
                                        child: Row(
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.all(0),
                                                  child: FittedBox(
                                                    child: Image.asset(
                                                      "assets/images/frontshop.png",
                                                    ),
                                                    fit: BoxFit.fitHeight,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Container(
                                              width: 205,
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(8,8,8,0),
                                                child: Column(
                                                  children: <Widget>[
                                                    Row(children: <Widget>[

                                                      Padding(
                                                        padding: const EdgeInsets.only(top:5,left: 5),
                                                        child: Container( width: 130, child: Text(siteSelectionProvider.patientSites[index].siteFriendlyName,style: TextStyle(fontSize: 14,fontFamily: UiConstants.segeoRegularFont,fontWeight:FontWeight.bold),maxLines: 2,)),
                                                      )
                                                    ],),
                                                    SizedBox(height: 5,),
                                                    Row(children: <Widget>[
                                                      Image.asset(
                                                        "assets/images/global.png", height: 18, width: 18,
                                                      ),
                                                      SizedBox(width: 5,),
                                                      Padding(
                                                        padding: const EdgeInsets.only(top:5),
                                                        child: Container( width: 130, child: Text('494 WHITEHOUSE ROAD SURREY HILLS VIC,3127 ',style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                      )
                                                    ],),
                                                    SizedBox(height: 5,),
                                                    Row(children: <Widget>[
                                                      Icon(Icons.mail_outline,color: ColorConstants.actionsColor,size: 18,),
                                                      SizedBox(width: 5,),
                                                      Container( width: 150, child: Text(siteSelectionProvider.patientSites[index].email==null?'info@MediClinic.com.au':siteSelectionProvider.patientSites[index].email,style: TextStyle(fontSize: 10),maxLines: 2,))
                                                    ],),
                                                    SizedBox(height: 5,),
                                                    Row(children: <Widget>[
                                                      Icon(Icons.phone_in_talk,color: ColorConstants.actionsColor,size: 18,),
                                                      SizedBox(width: 5,),
                                                      Container( width: 150, child: Text(siteSelectionProvider.patientSites[index].phone==null?'03 9947 1981':siteSelectionProvider.patientSites[index].phone,style: TextStyle(fontSize: 10),maxLines: 2,))
                                                    ],),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: <Widget>[
                                                        RaisedButton(
                                                          color:Colors.blueAccent,
                                                          textColor: ColorConstants.primaryColor,
                                                          elevation: 1,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: new BorderRadius.circular(20.0),
                                                              side: BorderSide(
                                                                  color: Colors.blueAccent)),
                                                          child: Text(
                                                            "Select",
                                                            style: TextStyle( color: Colors.white,
                                                                fontFamily: UiConstants.segeoLightFont,
                                                                fontSize: 15, fontWeight: FontWeight.normal
                                                            ),
                                                          ),
                                                          onPressed: () {
                                                            if(siteSelectionProvider.fromProvider)
                                                              Navigator.pushNamed(context, RoutesConstants.providerMainMenuPage,arguments: {"DBNAME":siteSelectionProvider.patientSites[index].dbName});
                                                            else
                                                              Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage,arguments: {"DBNAME":siteSelectionProvider.patientSites[index].dbName});
                                                          },
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )

                                          ],
                                        ),

                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                        SizedBox(height: 5,),
                        Divider(color: Colors.blueAccent, thickness: 2,),
                        SizedBox(height: 25,),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Image.asset('assets/images/calender.png',width: 30, height: 30,),
                              SizedBox(width: 10,),
                              Padding(
                                padding: const EdgeInsets.only(top:0),
                                child: Container(  child: Text('I can not find my clinic',style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont),maxLines: 2,)),
                              )
                            ],),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
