import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:provider/provider.dart';

class ProviderMainMenuPage extends StatefulWidget {
  @override
  ProviderMainMenuPageState createState() {
    return ProviderMainMenuPageState();
  }
}

class ProviderMainMenuPageState extends  State<ProviderMainMenuPage> {
  ProviderMainMenuProvider  providerMainMenuProvider;
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  void initState() {
    tiles = new List<Widget>();
    tiles.add(new Row(
      children: <Widget>[
        Text("Clinic",style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
        SizedBox(width: 12,),
        Text("Healthy West Clinic",style: TextStyle(fontSize: 15,color: Colors.black54, fontFamily: UiConstants.segeoRegularFont),),
      ],
    ));

    tiles.add(new Row(
      children: <Widget>[
        Text("Saturday",style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
        SizedBox(width: 12,),
        Text(",14th April | 10:00 am",style: TextStyle(fontSize: 15,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
      ],
    ));

    tiles.add(new Row(
      children: <Widget>[
        Text("Provider",style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),),
        SizedBox(width: 12,),
        Text(": John Clark",style: TextStyle(fontSize: 15,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
      ],
    ));

    indicators = new List<Padding>();
    indicators.add(Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(

        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
      ),
    ));
    indicators.add(Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(

        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
      ),
    ));
    indicators.add(Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    providerMainMenuProvider = Provider.of<ProviderMainMenuProvider>(context, listen: true);
    initState();
    return Container(
      child:  AppStateHandlerWidget(
        state: providerMainMenuProvider.loadingState,
        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.fromLTRB(10,20,10,10),
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                              ),
                              child: Column(
                                children: <Widget>[

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Today Appointments",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                        GestureDetector(
                                          child: Container(
                                            width: 40, height: 40,
                                            child: IconButton(

                                              icon: Image.asset(
                                                'assets/images/userprofile.png',width: 20, height: 20,),
                                              color: ColorConstants.primaryColor,
                                            ),
                                            decoration: BoxDecoration(
                                              color: Colors.blueAccent,
                                              borderRadius: BorderRadius.circular(25),
                                            ),
                                          ),
                                          onTap: (){
                                            Navigator.pushNamed(
                                                context, RoutesConstants.providerProfileMainPage,arguments: {"fromUnRegisteredUser":false});
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,0,80,0),
                                    child: Divider(color: Colors.blueAccent, thickness: 2,),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,0,80,0),
                                    child:Row(
                                      children: <Widget>[
                                        Text("26th May 2020",style: TextStyle(fontSize: 17,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12.0,10,80,10),
                                    child:Row(
                                      children: <Widget>[
                                    Image.asset(
                                    'assets/images/clinic2.png',width: 29, height: 27,),
                                        Text("   Healthy West Clinic",style: TextStyle(fontSize: 17,color: Colors.black,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.normal),),

                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[

                                      Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(12.0,10,0,10),
                                            child:Row(
                                              children: <Widget>[
                                                Image.asset(
                                                  'assets/images/appointment2.png',width: 29, height: 27,),
                                                Text("   5 Appointments",style: TextStyle(fontSize: 15,color: Colors.black,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.normal),),

                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(12.0,10,0,10),
                                            child:Row(
                                              children: <Widget>[
                                                Image.asset(
                                                  'assets/images/clock.png',width: 25, height: 25,),
                                                Text("   8 am - 15 pm",style: TextStyle(fontSize: 15,color: Colors.black,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.normal),),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),

                            SizedBox(height: 25,),
                            GestureDetector(
                              onTap: (){
                                providerMainMenuProvider.showDialog();
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(

                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset('assets/images/calender.png',width: 40, height: 40,),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,15,0),
                                                      child: Text("ALL BOOKINGS",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),
                                                    ),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View booking histories",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View all appointments ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                            width: 4,thickness: 0.75,
                                                            color: ColorConstants.arrowColor
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),
                            SizedBox(height: 10,),
                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.providerInvoicePage);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(
                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(

                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset(
                                                    'assets/images/invoice.png',width: 40, height: 40,),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(

                                                  children: <Widget>[
                                                    Text("INVOICES                        ",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" View invoice status and histories",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Request invoice                        ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                          width: 4, thickness: 0.75,
                                                          color: ColorConstants.arrowColor,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),

                            SizedBox(height: 10,),
                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.providerContactPage);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Container(

                                  decoration:BoxDecoration(
                                    color:ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: ColorConstants.graylightcolor,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(

                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  padding:EdgeInsets.fromLTRB(10,10,10,10),
                                                  decoration: UiConstants.defaultBoxDecoration,
                                                  child: Image.asset(
                                                    'assets/images/comment.png',width: 40, height: 40,),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Row(

                                                  children: <Widget>[
                                                    Text("CONTACT                        ",style: TextStyle(fontSize: 16, color:Colors.black,fontFamily: UiConstants.segeoRegularFont, fontWeight: FontWeight.bold),),

                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Contact clinic staff                       ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[

                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Inbox messages                        ",style: TextStyle(fontSize: 12, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[

                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 40,
                                                        child: VerticalDivider(
                                                          width: 4, thickness: 0.75,
                                                          color: ColorConstants.arrowColor,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10,),
                                                      Image.asset(
                                                        'assets/images/arrow.png',width: 20, height: 20,)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),

                                ),
                              ),
                            ),

                            GestureDetector(
                              onTap: (){
                                Navigator.pushNamed(context, RoutesConstants.createPatientProfilePage);
                               // providerMainMenuProvider.showDialog();
                              },
                              child:Padding(
                                padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                child: Container(
                                  padding:EdgeInsets.fromLTRB(15, 7, 15, 7),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text("Add New Patient",style: TextStyle(fontSize: 17,color: Colors.white,fontFamily: UiConstants.segeoRegularFont),),
                                      SizedBox(width: 10,),
                                      Container(
                                        padding:EdgeInsets.fromLTRB(5,5,5,5),
                                        decoration: BoxDecoration(
                                          color: ColorConstants.defaultBoxColor,
                                          border: Border.all(
                                            color: Colors.white,
                                            width: 0,
                                          ),
                                          borderRadius: BorderRadius.circular(25),
                                        ),
                                        child: Icon(Icons.add,color: Colors.blueAccent, size: 15,),
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(

                                    color: Colors.blueAccent,

                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                ),
                              )
                            ),
                            SizedBox(height: 10,),
                            Container(
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.transparent,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.vertical(top: new Radius.elliptical(15, 15)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(20,10,20,10),
                                child:   Container(
                                  padding:  const EdgeInsets.fromLTRB(20,0,5,0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                      color: ColorConstants.primaryBlueColors,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: TextFormField(
                                    /*onChanged: (val) => textChanged(val),*/
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Search text can not be blank';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: InputBorder.none, hintStyle: TextStyle(
      fontFamily: UiConstants.segeoRegularFont,
      color: ColorConstants.graylightcolor,
      fontSize: 16),
                                      hintText: 'Search Patient',
                                      suffixIcon: IconButton(
                                        icon: Container(
                                            width:200, height: 200,
                                            decoration: BoxDecoration(
                                              color: Colors.blueAccent,
                                              border: Border.all(
                                                color: Colors.blueAccent,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Icon(Icons.search,size: 15, color: Colors.white,)),
                                        onPressed: () {
                                          providerMainMenuProvider.showDialog();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
