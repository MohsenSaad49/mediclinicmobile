import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:mediclinic/LogicUtilities.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/patientClinicsProvider.dart';
import 'package:mediclinic/providers/patientMainMenuProvider.dart';
import 'package:mediclinic/providers/registerPatientWithoutDateProvider.dart';
import 'package:mediclinic/shimmers/shimmerDashboardLayout.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/AppBarWidget.dart';
import 'package:mediclinic/widgets/timeline.dart';
import 'package:provider/provider.dart';

class RegisterPatientWithoutDatePage extends StatefulWidget {
  @override
  RegisterPatientWithoutDatePageState createState() {
    return RegisterPatientWithoutDatePageState();
  }
}
class RegisterPatientWithoutDatePageState extends  State<RegisterPatientWithoutDatePage> {
  RegisterPatientWithoutDateProvider  registerPatientWithoutDateProvider;

  @override
  Widget build(BuildContext context) {
    registerPatientWithoutDateProvider = Provider.of<RegisterPatientWithoutDateProvider>(context, listen: true);

    return  Container(
      color: ColorConstants.graylightcolor,
      child:  Scaffold(
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:AppStateHandlerWidget(
                  state: registerPatientWithoutDateProvider.loadingState,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12.0,20,8,8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Mediclinic Application",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                  GestureDetector(
                                    child: Container(
                                      width: 40, height: 40,
                                      child: IconButton(

                                        icon: Image.asset(
                                          'assets/images/userprofile.png',width: 20, height: 20,),
                                        color: ColorConstants.primaryColor,
                                      ),
                                      decoration: BoxDecoration(

                                        color: Colors.blueAccent,

                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.pushNamed(context, RoutesConstants.patientProfileMainPage,arguments: {"fromUnRegisteredUser":true});
                                    },
                                  )

                                ],
                              ),
                            ),
                            SizedBox(height: 10,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(

                                decoration:BoxDecoration(
                                  color:ColorConstants.mainContainersColor,
                                  border: Border.all(
                                    color: ColorConstants.graylightcolor,
                                    width: 0.5,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(

                                            children: <Widget>[
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Row(
                                                  children: <Widget>[
                                                    Text("SHARED DOCUMENT ",style: TextStyle(fontSize: 20,color: Colors.black,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                                                  ],
                                                ),
                                                  SizedBox(height: 10,),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Upload document",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                      SizedBox(width: 20,),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                                          Text(" Shared result",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(width: 40,),
                                              Column(
                                                children: <Widget>[
                                                  Container(
                                                    width: 50, height: 50,
                                                    child: IconButton(

                                                      icon: Icon(Icons.folder_open,color: Colors.blue,),
                                                      color: ColorConstants.primaryColor,
                                                    ),
                                                    decoration: BoxDecoration(

                                                      color: Colors.white,

                                                      borderRadius: BorderRadius.circular(10),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),


                                        ],
                                      ),
                                      SizedBox(height: 20,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[

                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[

                                                          Container(
                                                              width: MediaQuery.of(context).size.width-40,
                                                              child: Text("You can upload and keep your Medical Records with you and anywhere you go!Your Data kept Safe and Secure in Australia Government data centre",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),)),
                                                        ],
                                                      ),


                                                    ],
                                                  ),
                                                  Center(
                                                    child: Container(
                                                      width:  MediaQuery.of(context).size.width-40,
                                                      child: Padding(
                                                        padding: const EdgeInsets.fromLTRB(0,10,5,5),
                                                        child:   RaisedButton(
                                                          color:Colors.white,
                                                          textColor: ColorConstants.primaryColor,
                                                          elevation: 0.0,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: new BorderRadius.circular(10.0),
                                                              side: BorderSide(
                                                                  color: Colors.blue)),

                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Text(
                                                                "My Documents ",
                                                                style: TextStyle( color: Colors.black,
                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                    fontSize: 16, fontWeight: FontWeight.bold
                                                                ),
                                                              ),
                                                              /*   Icon(Icons.search,size: 15, color: Colors.white,),*/
                                                            ],
                                                          ),
                                                          onPressed: () {
                                                            Navigator.pushNamed(context, RoutesConstants.patientSharedDocumentPage);
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),


                                            ],
                                          ),
                                          /*      Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[

                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: <Widget>[
                                                        SizedBox(
                                                          height: 40,
                                                          child: VerticalDivider(
                                                              width: 4,thickness: 0.75,
                                                              color: ColorConstants.arrowColor
                                                          ),
                                                        ),
                                                        SizedBox(width: 10,),
                                                        Image.asset(
                                                          'assets/images/arrow.png',width: 20, height: 20,)
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          )*/

                                        ],
                                      ),
                                    ],
                                  ),
                                ),

                              ),
                            ),
                            SizedBox(height: 10,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(

                                decoration:BoxDecoration(
                                  color:ColorConstants.mainContainersColor,
                                  border: Border.all(
                                    color: ColorConstants.graylightcolor,
                                    width: 0.5,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(10.0,0,0,0),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Text("Mediclinic Mobile Application ",style: TextStyle(fontSize: 20,color: Colors.black,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                                                    ],
                                                  ),


                                                ],
                                              ),
                                              SizedBox(width:20,),
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(0,20,0,0),
                                                    child: Container(
                                                      width: 50, height: 50,
                                                      child: IconButton(

                                                        icon: Icon(Icons.info_outline,color: Colors.white,size: 35,),
                                                        color: ColorConstants.primaryColor,
                                                      ),
                                                      decoration: BoxDecoration(

                                                        color: ColorConstants.darkBluecolor,

                                                        borderRadius: BorderRadius.circular(10),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),


                                        ],
                                      ),

                                      Row(
                                        children: <Widget>[

                                          Container(
                                              width: MediaQuery.of(context).size.width-80,
                                              child: Text("Managing your Medical appointment couldn't get any easier. With our app you can",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),)),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                              Text(" Manage your Bookings",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                            ],
                                          ),
                                          SizedBox(width: 20,),
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                              Text(" View Invoice",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                              Text(" Connect with Clinics     ",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                            ],
                                          ),
                                          SizedBox(width: 20,),
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.brightness_1,size: 5, color: Colors.blue,),
                                              Text(" Shared Document",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[

                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[

                                                          Container(
                                                              width: MediaQuery.of(context).size.width-40,
                                                              child: Text("You can upload and keep your Medical Records with you and anywhere you go!Your Data kept Safe and Secure in Australia Government data centre",style: TextStyle(fontSize: 14, color:Colors.black,fontFamily: UiConstants.segeoRegularFont),)),
                                                        ],
                                                      ),


                                                    ],
                                                  ),
                                                  Center(
                                                    child: Container(
                                                      width:  MediaQuery.of(context).size.width-40,
                                                      child: Padding(
                                                        padding: const EdgeInsets.fromLTRB(0,10,5,5),
                                                        child:   RaisedButton(
                                                          color:Colors.white,
                                                          textColor: ColorConstants.primaryColor,
                                                          elevation: 0.0,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: new BorderRadius.circular(10.0),
                                                              side: BorderSide(
                                                                  color: Colors.blue)),

                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Text(
                                                                "Read more ",
                                                                style: TextStyle( color: Colors.black,
                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                    fontSize: 16, fontWeight: FontWeight.bold
                                                                ),
                                                              ),
                                                              /*   Icon(Icons.search,size: 15, color: Colors.white,),*/
                                                            ],
                                                          ),
                                                          onPressed: () {
                                                          //  Navigator.pushNamed(context, RoutesConstants.registerNewPatientPage);
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),


                                            ],
                                          ),
                                          /*      Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[

                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: <Widget>[
                                                        SizedBox(
                                                          height: 40,
                                                          child: VerticalDivider(
                                                              width: 4,thickness: 0.75,
                                                              color: ColorConstants.arrowColor
                                                          ),
                                                        ),
                                                        SizedBox(width: 10,),
                                                        Image.asset(
                                                          'assets/images/arrow.png',width: 20, height: 20,)
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          )*/

                                        ],
                                      ),
                                    ],
                                  ),
                                ),

                              ),
                            ),

                          ],
                        ),
                      ),
                    ],

                  ),
                ),
              ),
            ),
          )),
    );
  }




}
