import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/patientBookingProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:mediclinic/widgets/timeline.dart';
import 'package:provider/provider.dart';

import '../appEnums.dart';

class PatientBookingPage extends StatefulWidget {
  @override
  PatientBookingPageState createState() {
    return PatientBookingPageState();
  }
}
class PatientBookingPageState extends  State<PatientBookingPage> {
  PatientBookingProvider patientBookingProvider   ;

  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  bool show=false;
  int _value = 1;


  void initState() {


      tiles = new List<Widget>();

      tiles.add(new Row(
        children: <Widget>[
           Text("Clinic",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
           SizedBox(width: 12,),
           Text(patientBookingProvider?.patientLastBooking?.clinicName==null?"loading...":patientBookingProvider?.patientLastBooking?.clinicName,style: TextStyle(fontSize: 14,color: Colors.black54, fontFamily: UiConstants.segeoRegularFont),),
        ],

      ));
      tiles.add(new Row(
        children: <Widget>[
          Text(patientBookingProvider?.patientLastBooking?.bookingDateStart==null?"loading...":patientBookingProvider?.patientLastBooking?.bookingDateEnd.split(',')[0],style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
          SizedBox(width: 12,),
          Text(patientBookingProvider?.patientLastBooking?.bookingDateStart==null?"loading...":patientBookingProvider?.patientLastBooking?.bookingDateEnd.split(',')[1],style: TextStyle(fontSize: 14,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Provider",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
          SizedBox(width: 12,),
          Text(patientBookingProvider?.patientLastBooking?.fullName()==null?"loading...":patientBookingProvider?.patientLastBooking?.fullName().toString(),style: TextStyle(fontSize: 14,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));



      indicators = new List<Padding>();

        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(

            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.brightness_1,size: 10,color: Colors.blueAccent,),
          ),
        ));



  }


  @override
  Widget build(BuildContext context) {
    patientBookingProvider = Provider.of<PatientBookingProvider>(context, listen: true);
    if(patientBookingProvider.loadingState==LoadingState.done)
      initState();
    return Container(
      color: ColorConstants.graylightcolor,
      child:  AppStateHandlerWidget(

        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: ColorConstants.graylightcolor.withOpacity(0.4),
                        ),
                        child: Column(
                          children: <Widget>[
                            Stack(
                              overflow: Overflow.visible,
                              children: <Widget>[
                                Container(
                                  padding:EdgeInsets.fromLTRB(12,20,10,10),
                                  decoration:BoxDecoration(
                                    color: ColorConstants.mainContainersColor,
                                    border: Border.all(
                                      color: Colors.blue,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0.0,0,8,8),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[


                                            GestureDetector(
                                              child: Container(
                                                width: 40, height: 40,
                                                child: IconButton(

                                                  icon:Icon(Icons.arrow_back_ios,color: Colors.white,size: 25,),
                                                  color: ColorConstants.primaryColor,
                                                ),
                                                decoration: BoxDecoration(

                                                  color: Colors.blueAccent,

                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                              onTap: (){
                                                Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                              },
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(35,0,0,0),
                                              child: Text("Last appointment",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                            ),


                                          ],
                                        ),
                                      ),
                                      Timeline(
                                        padding:EdgeInsets.fromLTRB(60, 0, 0, 0),
                                        children:tiles,
                                        indicators: indicators,
                                      ),
                                      SizedBox(height: 20,),

                                    ],
                                  ),
                                ),
                                Positioned(
                                  bottom: -35,
                                  left: 100,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(20),
                                      child: GestureDetector(
                                        onTap: (){
                                          Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                                        },
                                        child: Container(
                                          padding:EdgeInsets.fromLTRB(10, 5, 5, 5),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text("Book again",style: TextStyle(fontSize: 15,color: Colors.white,fontFamily: UiConstants.segeoRegularFont),),
                                              SizedBox(width: 10,),
                                              Container(
                                                padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                decoration: BoxDecoration(
                                                  color: ColorConstants.defaultBoxColor,
                                                  border: Border.all(
                                                    color: Colors.white,
                                                    width: 0,
                                                  ),
                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                                child: Image.asset(
                                                  'assets/images/calender.png',width: 16, height: 18,),


                                              )
                                            ],
                                          ),
                                          decoration: BoxDecoration(

                                            color: Colors.blueAccent,

                                            borderRadius: BorderRadius.circular(25),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                              ],
                            ),
                            SizedBox(height: 25,),
                            GestureDetector(

                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: ()async{
                                     await patientBookingProvider.selectFuture();
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(20,0,0,0),
                                      child: Container(
                                        padding:EdgeInsets.fromLTRB(10, 5, 5, 5),

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Text("Future Bookings",style: TextStyle(fontSize: 13,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                            SizedBox(width: 10,),

                                          ],
                                        ),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color:patientBookingProvider.futureSelected?ColorConstants.primaryBlueColors :ColorConstants.lightgray,
                                              width:patientBookingProvider.futureSelected?2: 1,
                                            ),
                                            borderRadius: BorderRadius.circular(30),
                                          )
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: ()async{
                                    await  patientBookingProvider.selectPrevious();
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(20,0,10,0),
                                      child: Container(
                                          padding:EdgeInsets.fromLTRB(5, 5, 0, 5),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text("Previous Bookings",style: TextStyle(fontSize: 13,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                              SizedBox(width: 10,),

                                            ],
                                          ),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color:patientBookingProvider.previousSelected?ColorConstants.primaryBlueColors :ColorConstants.lightgray,
                                              width:patientBookingProvider.previousSelected?2: 1,
                                            ),
                                            borderRadius: BorderRadius.circular(30),
                                          )
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            GestureDetector(
                                onTap: (){
                                  patientBookingProvider.removeFilter();


                                  showModalBottomSheet(
                                      context: context,
                                      isScrollControlled:
                                      true,
                                      elevation: 1,
                                      isDismissible: true,
                                      backgroundColor:
                                      Colors
                                          .white,
                                      builder: (builder) {

                                        return Container(
                                          height: 570,
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                child:           Column(
                                                  children: <Widget>[

                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                          child: Text(
                                                            "Sort by",
                                                            style: TextStyle( color: ColorConstants.darkBluecolor,
                                                                fontFamily: UiConstants.segeoRegularFont,
                                                                fontSize: 24, fontWeight: FontWeight.bold
                                                            ),
                                                          ),
                                                        ),
                                                        GestureDetector(
                                                            onTap: (){Navigator.pop(context);},
                                                            child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                      ],
                                                    ),

                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                          child: Text(
                                                            "Filter by Provider",
                                                            style: TextStyle( color: Colors.black,
                                                                fontFamily: UiConstants.segeoRegularFont,
                                                                fontSize: 15, fontWeight: FontWeight.bold
                                                            ),
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(8.0,0,8,0),
                                                      child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                    ),
                                                    Container(
                                                      height: 150,
                                                      child:   ListView(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(8.0,0,8,8),

                                                            child: RadioButtonGroup(
                                                                labels:patientBookingProvider.providerFilter,
                                                                labelStyle: TextStyle( color: Colors.grey,
                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                    fontSize: 12, fontWeight: FontWeight.bold
                                                                ),
                                                                onSelected: (String provider) => patientBookingProvider.selectedProvider=provider,
                                                            ),


                                                          ),
                                                        ],

                                                      ),
                                                    ),

                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                          child: Text(
                                                            "Filter by Clinic",
                                                            style: TextStyle( color: Colors.black,
                                                                fontFamily: UiConstants.segeoRegularFont,
                                                                fontSize: 15, fontWeight: FontWeight.bold
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(8.0,0,8,0),
                                                      child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                    ),

                                                    Container(
                                                      height: 150,
                                                      child:ListView(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(8.0,0,8,8),
                                                            child: RadioButtonGroup(
                                                                labels:patientBookingProvider.clinicFilter,
                                                                labelStyle: TextStyle( color: Colors.grey,
                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                    fontSize: 12, fontWeight: FontWeight.bold
                                                                ),
                                                                onSelected: (String clinic) =>patientBookingProvider.selectedClinic=clinic,
                                                            ),
                                                          ),
                                                        ],

                                                      ),
                                                    ),



                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(8.0,0,8,0),
                                                      child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                    ),


                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: <Widget>[
                                                        Container(
                                                          width: 160,
                                                          child: RaisedButton(
                                                            color:Colors.blueAccent,
                                                            textColor: ColorConstants.primaryColor,
                                                            elevation: 0.0,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: new BorderRadius.circular(30.0),
                                                                side: BorderSide(
                                                                    color: Colors.blueAccent)),

                                                            child: Text(
                                                              "Apply Filter",
                                                              style: TextStyle( color: Colors.white,
                                                                  fontFamily: UiConstants.segeoRegularFont,
                                                                  fontSize: 17, fontWeight: FontWeight.normal
                                                              ),
                                                            ),
                                                            onPressed: () {
                                                                patientBookingProvider.applyFilter();
                                                                Navigator.pop(context);
                                                            },
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );

                                      }
                                  );
                                },
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(15,10,15,0),
                                child: Container(

                                  child: Container(
                                      padding:EdgeInsets.fromLTRB(10, 5, 5, 5),

                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Text("Sort by ",style: TextStyle(fontSize: 14,color: ColorConstants.primaryBlueColors,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                          Image.asset(
                                            'assets/images/filter2.png',width: 10, height: 10,),

                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: ColorConstants.actionsColor,
                                          width: 1,
                                        ),
                                        borderRadius: BorderRadius.circular(30),
                                      )
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              height: 300,
                              child: AppStateHandlerWidget(
                                state: patientBookingProvider.loadingState,
                                child: ListView.builder(
                                    itemCount: (patientBookingProvider==null||patientBookingProvider.patientAllBooking==null)?0:patientBookingProvider.patientAllBooking.length,
                                    itemBuilder: (context, index) {
                                       return   GestureDetector(
                                         child: Padding(
                                           padding: const EdgeInsets.fromLTRB(12,10,12,0),
                                           child: Container(
                                             decoration:BoxDecoration(
                                               color:Colors.white,
                                               border: Border.all(
                                                 color: ColorConstants.actionsColor,
                                                 width: 1,
                                               ),
                                               borderRadius: BorderRadius.circular(30),
                                             ),
                                             child: Column(
                                               children: <Widget>[
                                                 Padding(
                                                   padding: const EdgeInsets.all(10.0),
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                     children: <Widget>[
                                                       Column(
                                                         children: <Widget>[
                                                           Row(
                                                             children: <Widget>[

                                                               Padding(
                                                                 padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                                 child: Row(
                                                                   mainAxisAlignment: MainAxisAlignment.end,
                                                                   children: <Widget>[

                                                                     Image.asset(
                                                                       'assets/images/calendergreen.png',width: 36, height: 42,)
                                                                   ],
                                                                 ),
                                                               ),
                                                             ],
                                                           ),
                                                         ],
                                                       ),
                                                       Column(
                                                         children: <Widget>[
                                                           Row(
                                                             children: <Widget>[

                                                               Padding(
                                                                 padding: const EdgeInsets.fromLTRB(8.0,0,0,0),
                                                                 child: Text(patientBookingProvider.patientAllBooking[index].bookingDateStart,style: TextStyle(fontSize: 15,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                               ),
                                                             ],
                                                           ),
                                                           SizedBox(height: 7,),
                                                           Row( mainAxisAlignment: MainAxisAlignment.start,
                                                             children: <Widget>[

                                                               Padding(
                                                                 padding: const EdgeInsets.fromLTRB(0,0,15,0),
                                                                 child: Container(
                                                                   width: 120,
                                                                   child: Text(
                                                                     patientBookingProvider.patientAllBooking[index].clinicName,
                                                                     maxLines: 1,
                                                                   overflow: TextOverflow.fade,
                                                                      softWrap: false,style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                                 ),
                                                               ),
                                                             ],
                                                           ),
                                                         ],
                                                       ),
                                                       Column(
                                                         children: <Widget>[
                                                           Padding(
                                                             padding: const EdgeInsets.fromLTRB(5,0,0,20),
                                                             child: Container(
                                                               padding:EdgeInsets.fromLTRB(10, 3, 5, 3),
                                                               child: Row(
                                                                 mainAxisAlignment: MainAxisAlignment.center,
                                                                 mainAxisSize: MainAxisSize.min,
                                                                 children: <Widget>[
                                                                   Text("Completed",style: TextStyle(fontSize: 12,color:patientBookingProvider.previousSelected? Colors.white:Colors.transparent,fontFamily: UiConstants.segeoRegularFont),),
                                                                   SizedBox(width: 5,),
                                                                   Container(
                                                                     padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                                     decoration: BoxDecoration(
                                                                       color: patientBookingProvider.previousSelected?ColorConstants.defaultBoxColor:Colors.transparent,
                                                                       border: Border.all(
                                                                         color:patientBookingProvider.previousSelected? Colors.white:Colors.transparent,
                                                                         width: 0,
                                                                       ),
                                                                       borderRadius: BorderRadius.circular(25),
                                                                     ),
                                                                     child: Icon(Icons.done,size: 10,color:patientBookingProvider.previousSelected? ColorConstants.primaryBlueColors:Colors.transparent,),


                                                                   )
                                                                 ],
                                                               ),
                                                               decoration: BoxDecoration(

                                                                 color:patientBookingProvider.previousSelected? Colors.blueAccent:Colors.transparent,

                                                                 borderRadius: BorderRadius.circular(25),
                                                               ),
                                                             ),
                                                           )
                                                         ],
                                                       )
                                                     ],
                                                   ),
                                                 ),
                                                 Padding(
                                                   padding: const EdgeInsets.fromLTRB(2,0,2,5),
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.start,
                                                     children: <Widget>[

                                                       Column(
                                                         children: <Widget>[
                                                           Padding(
                                                             padding: const EdgeInsets.only(left:20),
                                                             child:  Text("Patient ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                           ),
                                                         ],
                                                       ),
                                                       Column(
                                                         children: <Widget>[
                                                           Padding(
                                                             padding: const EdgeInsets.only(left:10),
                                                             child:  Text(patientBookingProvider.patientAllBooking[index].patientFullName(),style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                           ),
                                                         ],
                                                       ),


                                                     ],
                                                   ),
                                                 ),
                                                 Padding(
                                                   padding: const EdgeInsets.all(2.0),
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.start,
                                                     children: <Widget>[

                                                       Column(
                                                         children: <Widget>[
                                                           Padding(
                                                             padding: const EdgeInsets.only(left:20),
                                                             child:  Text("Service ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                           ),
                                                         ],
                                                       ),
                                                       Column(
                                                         children: <Widget>[
                                                           Padding(
                                                             padding: const EdgeInsets.only(left:10),

                                                             child:  Container(
                                                               width: 120,
                                                               child: Text(
                                                                 patientBookingProvider.patientAllBooking[index].serviceShortName,
                                                                 maxLines: 1,
                                                                 overflow: TextOverflow.fade,
                                                                 softWrap: false,style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                             )
                                                           ),
                                                         ],
                                                       ),


                                                     ],
                                                   ),
                                                 ),
                                                 Padding(
                                                   padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                                   child: Divider(color: Colors.blueAccent, thickness: 1,),
                                                 ),
                                                 GestureDetector(
                                                   onTap: (){

                                                if(patientBookingProvider.futureSelected)
                                                  {
                                                    showModalBottomSheet(
                                                        context: context,
                                                        isScrollControlled:
                                                        true,
                                                        elevation: 1,
                                                        isDismissible: true,
                                                        backgroundColor:
                                                        Colors
                                                            .white,
                                                        builder: (builder) {

                                                          return Container(
                                                            height: 250,
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                                  child:           Column(
                                                                    children: <Widget>[

                                                                      Row(
                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: GestureDetector(
                                                                              onTap: ()async{
                                                                                patientBookingProvider.launchURL();
                                                                              },
                                                                              child: Text(
                                                                                "Call Clinic",
                                                                                style: TextStyle( color: Colors.blueGrey[700],
                                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                                    fontSize: 18, fontWeight: FontWeight.bold
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          GestureDetector(
                                                                              onTap: (){Navigator.pop(context);},
                                                                              child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                        ],
                                                                      ),

                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),

                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Move appointment",
                                                                              style: TextStyle( color: Colors.green,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),
                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Cancel appointment",
                                                                              style: TextStyle( color: Colors.red,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),

                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          );

                                                        }
                                                    );
                                                  }else{
                                                  showModalBottomSheet(
                                                      context: context,
                                                      isScrollControlled:
                                                      true,
                                                      elevation: 1,
                                                      isDismissible: true,
                                                      backgroundColor:
                                                      Colors
                                                          .white,
                                                      builder: (builder) {

                                                        return Container(
                                                          height: 180,
                                                          child: Column(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                                child:           Column(
                                                                  children: <Widget>[

                                                                    Row(
                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: GestureDetector(
                                                                            onTap: ()async{
                                                                              patientBookingProvider.launchURL();
                                                                            },
                                                                            child: Text(
                                                                              "Call Clinic",
                                                                              style: TextStyle( color: Colors.blueGrey[700],
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        GestureDetector(
                                                                            onTap: (){Navigator.pop(context);},
                                                                            child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                      ],
                                                                    ),

                                                                    SizedBox(height: 5,),
                                                                    Padding(
                                                                      padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                      child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                    ),

                                                                    Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                          child: Text(
                                                                            "Request invoice to my email",
                                                                            style: TextStyle( color: Colors.blueGrey[700],
                                                                                fontFamily: UiConstants.segeoLightFont,
                                                                                fontSize: 18, fontWeight: FontWeight.bold
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    SizedBox(height: 5,),
                                                                    Padding(
                                                                      padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                      child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                    ),

                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );

                                                      }
                                                  );
                                                }

                                                   },
                                                   child: Container(
                                                     child: Row(
                                                       mainAxisAlignment: MainAxisAlignment.center,
                                                       children: <Widget>[
                                                         Column(
                                                           mainAxisSize: MainAxisSize.max,
                                                           children: <Widget>[
                                                             Text("Actions ",style: TextStyle(fontSize: 17,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                             Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                           ],
                                                         )
                                                       ],
                                                     ),
                                                   ),
                                                 )
                                               ],
                                             ),

                                           ),
                                         ),
                                       );
                                    }
                                ),
                              ),
                            ),



                            GestureDetector(
                              onTap: (){

                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    DefaultIconButton(
                                      labelText: 'Bookings',
                                      iconWidget: Image.asset(
                                        'assets/images/calender.png',width: 20, height: 23,),
                                      onPressed: () {
                                        //Navigator.pushNamed(context, RoutesConstants.callLogPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Invoices',
                                      iconWidget: Image.asset(
                                        'assets/images/invoices2.png',width: 20, height: 23,),
                                      onPressed: () {
                                       // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Contact',
                                      iconWidget: Image.asset(
                                        'assets/images/comment.png',width: 20, height: 23,),
                                      onPressed: () async {
                                        //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                        //onNavigate(true);

                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Profile',
                                      iconWidget: Image.asset(
                                        'assets/images/user2.png',width: 20, height: 23,),
                                      onPressed: () {
                                       // Navigator.pushNamed(context, RoutesConstants.transactionPage);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ],

                  ),
                ),
              ),
            )),
      ),
    );
  }




}
