import 'package:flutter/material.dart';
import 'package:flutter_fab_dialer/flutter_fab_dialer.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class WelcomePage extends StatefulWidget {
  @override
  WelcomePageState createState() {
    return WelcomePageState();
  }
}
class WelcomePageState extends  State<WelcomePage> {
 // LoginProvider loginProvider;
  final controller = PageController(viewportFraction: 0.9);
  int index=0;

  void navigate(int index) {
    setState(() {
      controller.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  Widget build(BuildContext context) {
   //loginProvider = Provider.of<LoginProvider>(context, listen: false);



    var _fabMiniMenuItemList = [
      new FabMiniMenuItem.withText(new Icon(Icons.add), Colors.blue, 4.0, "Button menu", _incrementCounter, "Click me", Colors.blue, Colors.white, true),
    ];

    return new Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 16),
            SizedBox(
              height: MediaQuery.of(context).size.height-160, width: 700,
              child: PageView( allowImplicitScrolling: true,

                controller: controller,
                onPageChanged: (indexx) {
                    index=indexx;
                },
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,0,10,0),
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                      decoration: UiConstants.whiteContainerWithShadowDecoration,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,10),
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      child: Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Image.asset(
                                          'assets/images/logomain2.png',
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      left: 10,
                                      child: Image.asset(
                                        'assets/images/logo.png' ,
                                        width: 300, height: 300,),
                                    ),
                                  ],

                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20,0,20,10.0),
                                    child: Text("Welcome",style: TextStyle(color: Colors.black,fontSize: 30,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.bold ),),
                                  ),
                                ],
                              ),


                              Padding(
                                padding: const EdgeInsets.fromLTRB(20,0,0,8.0),
                                child: Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[

                                    Text("To MedClinic Application ",style: TextStyle(color: Colors.black,fontSize: 15,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),),

                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20,0,0,8.0),
                                child: Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[

                                    Container( width:220 ,child: Text("Fully cloud integration app that allow you to manage your booking, invoice and others at the tip of your finger ",style: TextStyle(color: Colors.black,fontSize: 15,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),)),

                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,20,0,8.0),
                                child: Row(

                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[

                                    Container(
                                      padding:  const EdgeInsets.fromLTRB(20,0,5,0),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        border: Border.all(
                                          color: Colors.blueAccent,
                                          width: 1,
                                        ),
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: new DropdownButton<String>(
                                          icon: RaisedButton(
                                            disabledColor: Colors.blueAccent,
                                            color:Colors.blueAccent,
                                            textColor: Colors.blueAccent,
                                            elevation: 0.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(30.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),

                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "English",
                                                  style: TextStyle( color: Colors.white,
                                                      fontFamily: UiConstants.segeoLightFont,
                                                      fontSize: 12, fontWeight: FontWeight.bold
                                                  ),
                                                ),
                                                Icon(Icons.keyboard_arrow_down,size: 25, color: Colors.white,),
                                              ],
                                            ),

                                          ),
                                          hint: Text('Select your language  '),
                                          items: <String>['Arabic', 'English', 'Danish',].map((String value) {
                                            return new DropdownMenuItem<String>(
                                              value: value,

                                              child: new Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (_) {

                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],

                          ),
                        ],

                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,0,10,0),
                    child: Container(

                      decoration: UiConstants.whiteContainerWithShadowDecoration,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,10),
                                child: Stack(

                                  children: <Widget>[
                                    Container(

                                      child: Padding(
                                        padding: const EdgeInsets.all(0),

                                        child: Image.asset(
                                          'assets/images/logomain2.png',
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      left: 10,
                                      child: Image.asset(
                                        'assets/images/logo.png' ,
                                        width: 300, height: 300,),
                                    ),
                                  ],

                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left:20.0),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0,0,20,20.0),
                                      child: Text("Bookings & Invoices",style: TextStyle(color: Colors.black,fontSize: 30,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.bold ),),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,0,0,20.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text("Either you are ",style: TextStyle(color: Colors.black,fontSize: 15,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.normal ),),
                                          Text("Provider ",style: TextStyle(color: Colors.blueAccent,fontSize: 15,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.bold ),),
                                          Text("or ",style: TextStyle(color: Colors.black,fontSize: 15,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.normal ),),
                                          Text("Client",style: TextStyle(color: Colors.blueAccent,fontSize: 15,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.bold ),),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,0,0,8.0),
                                      child: Row(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(0,0,0,15),
                                            child: Icon(Icons.stop,color: Colors.blueAccent,size: 15,),
                                          ),
                                          Container(width: 230, child: Text(" You can add new appointment at the registered clinic ",style: TextStyle(color: Colors.black,fontSize: 12,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),)),

                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,15,0,8.0),
                                      child: Row(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.stop,color:Colors.blueAccent,size: 15,),
                                          Text(" Move existing appointment or cancel",style: TextStyle(color: Colors.black,fontSize: 12,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),),

                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,15,0,8.0),
                                      child: Row(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.stop,color:Colors.blueAccent,size: 15,),
                                          Text(" Complete the appointment provider only ",style: TextStyle(color: Colors.black,fontSize: 12,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),),

                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,15,0,8.0),
                                      child: Row(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.stop,color: Colors.blueAccent,size: 15,),
                                          Text(" View invoices ",style: TextStyle(color: Colors.black,fontSize: 12,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),),

                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],

                          ),
                        ],

                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,0,10,0),
                    child: Container(

                      decoration: UiConstants.whiteContainerWithShadowDecoration,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,10),
                                child: Stack(

                                  children: <Widget>[
                                    Container(

                                      child: Padding(
                                        padding: const EdgeInsets.all(0),

                                        child: Image.asset(
                                          'assets/images/logomain2.png',
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      left: 10,
                                      child: Image.asset(
                                        'assets/images/logo.png' ,
                                        width: 300, height: 300,),
                                    ),
                                  ],

                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left:15.0),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0,0,20,20.0),
                                      child: Text("Communication",style: TextStyle(color: Colors.black,fontSize: 35,fontFamily:UiConstants.segeoRegularFont,fontWeight: FontWeight.bold ),),
                                    ),

                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(20,0,0,8.0),
                                      child: Row(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(width: 250, child: Text("We understand important roll of communication in managing clinic between provider and patient.",style: TextStyle(color: Colors.black,fontSize: 18,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.normal ),)),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],

                          ),
                        ],

                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,0,10,0),
                    child: Container(

                      decoration: UiConstants.whiteContainerWithShadowDecoration,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,10),
                                child: Stack(

                                  children: <Widget>[
                                    Container(

                                      child: Padding(
                                        padding: const EdgeInsets.all(0),

                                        child: Image.asset(
                                          'assets/images/logomain2.png',
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      left: 10,
                                      child: Image.asset(
                                        'assets/images/logo.png' ,
                                        width: 300, height: 300,),
                                    ),
                                  ],

                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,20,20.0),
                                child: Text("Select your roll",style: TextStyle(color: Colors.black,fontSize: 35,fontFamily:UiConstants.segeoLightFont,fontWeight: FontWeight.bold ),),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,20.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[

                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(0,20,0,8.0),
                                            child: Image.asset(
                                              'assets/images/doctor.png',
                                            ),
                                          ),

                                          RaisedButton(
                                            color:Colors.blueAccent,
                                            textColor: ColorConstants.primaryColor,
                                            elevation: 0.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(30.0),
                                                side: BorderSide(
                                                    color: Colors.blueAccent)),

                                            child: Text(
                                              "I am a Provider",
                                              style: TextStyle( color: Colors.white,
                                                  fontFamily: UiConstants.segeoLightFont,
                                                  fontSize: 14, fontWeight: FontWeight.normal
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.pushNamed(
                                                  context, RoutesConstants.providerLoginPage);
                                            },
                                          )
                                        ],
                                      ),
                                      SizedBox(width: 10,),
                                      Column(
                                        children: <Widget>[

                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(0,20,0,8.0),
                                            child: Image.asset(
                                              'assets/images/user.png',
                                            ),
                                          ),

                                          RaisedButton(
                                            color:Colors.blueAccent,
                                            textColor: ColorConstants.primaryColor,
                                            elevation: 0.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(30.0),
                                                side: BorderSide(
                                                    color: Colors.blueAccent)),

                                            child: Text(
                                              "I am a Client",
                                              style: TextStyle( color: Colors.white,
                                                  fontFamily: UiConstants.segeoLightFont,
                                                  fontSize: 14, fontWeight: FontWeight.normal
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.pushNamed(
                                                  context, RoutesConstants.termsConditionsPage);
                                            },
                                          )
                                        ],
                                      )

                                    ]
                                ),
                              ),
                            ],

                          ),
                        ],

                      ),
                    ),
                  ),

                ],
                /*children: List.generate(
                        6,
                            (_) => Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                          child: Container(height: 500),
                        )),*/
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(1,1,1,1),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 160,
                    child: RaisedButton(
                      color:Colors.blueAccent,
                      textColor: ColorConstants.primaryColor,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                          side: BorderSide(
                              color: Colors.blueAccent)),

                      child: Text(
                        "Next",
                        style: TextStyle( color: Colors.white,
                            fontFamily: UiConstants.segeoRegularFont,
                            fontSize: 17, fontWeight: FontWeight.normal
                        ),
                      ),
                      onPressed: () {
                        if(index==3)
                        {

                        }else {
                          index++;
                          navigate(index);
                        }

                      },
                    ),
                  ),
                ],

              ),
            ),

            SmoothPageIndicator(
              controller: controller,
              count: 4,
              effect: JumpingDotEffect(),
            ),
            SizedBox(height: 5,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(60,1,1,1),
                  child: Container(
                    width: 100,
                    child: RaisedButton(
                      color:Colors.blue,
                      textColor: ColorConstants.primaryColor,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.only(topLeft: new Radius.elliptical(50,60),bottomLeft: new Radius.elliptical(50,60)),
                          side: BorderSide(
                              color: Colors.blue,)),

                      child: Text(
                        "Skip",
                        style: TextStyle( color: Colors.white,
                            fontFamily: UiConstants.segeoRegularFont,
                            fontSize: 14, fontWeight: FontWeight.normal
                        ),
                      ),
                      onPressed: () {

                        Navigator.pushNamed(
                            context, RoutesConstants.providerLoginPage);
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),

    );

  }


  void _incrementCounter() {
    setState(() {

    });
  }

}
