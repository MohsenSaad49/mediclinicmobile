import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/main.dart';
import 'package:mediclinic/providers/appRegConfirmProvider.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class AppRegConfirmPage extends StatefulWidget {
  @override
  AppRegConfirmPageState createState() {
    return AppRegConfirmPageState();
  }
}
class AppRegConfirmPageState extends  State<AppRegConfirmPage> {
  AppRegConfirmProvider appRegConfirmProvider;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    appRegConfirmProvider = Provider.of<AppRegConfirmProvider>(context, listen: false);
    return new Scaffold(
   //   backgroundColor: ColorConstants.mainContainersColor.withOpacity(0.5),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:Column(
                children: <Widget>[
                  Container(
                    padding:EdgeInsets.fromLTRB(10,20,10,10),
                    decoration:BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(
                        color: Colors.blue,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30,),
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: 35, height: 35,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                  child:IconButton(

                                    icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                    color: ColorConstants.primaryColor,
                                  ),
                                ),
                                decoration: BoxDecoration(

                                  color: Colors.blueAccent,

                                  borderRadius: BorderRadius.circular(25),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30,10,10,10),
                              child: Text("App Registration",style: TextStyle(fontSize: 30,color: Colors.blueGrey, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(70,0,90,10),
                          child: Divider(height: 5,thickness: 2,color: Colors.blue,),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width:300,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(70,0,10,10),
                                child: Text("We will need to search our database for your clinic. Please fill in information below so we may assist you ",

                                  overflow: TextOverflow.clip,
                                  style: TextStyle(fontSize: 12,color: Colors.black45),),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),

                  Container(
                    height:550,
                    child: ListView(
                      children: <Widget>[
                       Column(children: <Widget>[
                         Center(
                           child: Padding(
                             padding: const EdgeInsets.fromLTRB(30,0,40,0),
                             child: Container(

                               child:   Container(
                                 width:400,
                                 child: Padding(
                                   padding: const EdgeInsets.fromLTRB(10,0,0,10),
                                   child: Column(
                                     children: <Widget>[
                                       Text("We are unable to find your details",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: Colors.red),),
                                       Text("in our database ",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: Colors.red),),
                                       SizedBox(height: 50,),
                                       Text("To contact the clinic for new booking",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: Colors.black),),
                                       Text("and we can not find you ",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: Colors.black),),
                                       Text("send us an email at ",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: Colors.black),),
                                       Text("info@mediclinic.com.au",
                                         overflow: TextOverflow.clip,
                                         style: TextStyle(fontSize: 16,color: ColorConstants.darkBluecolor,fontWeight:FontWeight.bold ),),
                                       SizedBox(height: 50,),

                                     ],
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ),

                         SizedBox(height: 60,),
                         Center(
                           child: Column(
                             children: <Widget>[
                               Text("Click here to go back and check your information",
                                 overflow: TextOverflow.visible,
                                 style: TextStyle(fontSize: 15,color: Colors.black,),),
                               Container(
                                 width: 120,
                                 child: Padding(
                                   padding: const EdgeInsets.fromLTRB(0,5,5,0),
                                   child:   RaisedButton(
                                     color:Colors.transparent,
                                     textColor: ColorConstants.primaryColor,
                                     elevation: 0.0,
                                     shape: RoundedRectangleBorder(
                                         borderRadius: new BorderRadius.circular(30.0),
                                         side: BorderSide(
                                           width: 1.5,
                                             color: Colors.black)),

                                     child: Row(
                                       mainAxisAlignment: MainAxisAlignment.center,
                                       children: <Widget>[
                                         Text(
                                           "Search ",
                                           style: TextStyle( color: Colors.blue,
                                               fontFamily: UiConstants.segeoLightFont,
                                               fontSize: 16, fontWeight: FontWeight.bold
                                           ),
                                         ),
                                         Icon(Icons.search,size: 22, color: Colors.blueAccent,),
                                       ],
                                     ),
                                     onPressed: () {
                                       Navigator.pop(context);
                                     },
                                   ),
                                 ),
                               ),
                             ],
                           ),
                         ),
                         Padding(
                           padding: const EdgeInsets.fromLTRB(40,10,40,0),
                           child: Divider(height: 1,thickness: 0.5,color: Colors.black,),
                         ),
                         Center(
                           child: Container(
                             width: 165,
                             child: Padding(
                               padding: const EdgeInsets.fromLTRB(0,10,5,20),
                               child:   RaisedButton(
                                 color:Colors.blue,
                                 textColor: ColorConstants.primaryColor,
                                 elevation: 0.0,
                                 shape: RoundedRectangleBorder(
                                     borderRadius: new BorderRadius.circular(30.0),
                                     side: BorderSide(
                                         color: Colors.blue)),

                                 child: Row(
                                   children: <Widget>[
                                     Text(
                                       "Create an account ",
                                       style: TextStyle( color: Colors.white,
                                           fontFamily: UiConstants.segeoLightFont,
                                           fontSize: 16, fontWeight: FontWeight.bold
                                       ),
                                     ),
                                     /*   Icon(Icons.search,size: 15, color: Colors.white,),*/
                                   ],
                                 ),
                                 onPressed: () {
                                        Navigator.pushNamed(context, RoutesConstants.registerNewPatientPage);
                                 },
                               ),
                             ),
                           ),
                         ),
                       ],)

                      ],

                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
