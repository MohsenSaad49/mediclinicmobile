import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/providerLoginProvider.dart';
import 'package:mediclinic/widgets/statefull/passwordField.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class ProviderLoginPage extends StatefulWidget {
  @override
  ProviderLoginPageState createState() {
    return ProviderLoginPageState();
  }
}

class ProviderLoginPageState extends  State<ProviderLoginPage> {
  ProviderLoginProvider loginProvider;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String date = "Date of Birth";
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)

      setState(() {
        selectedDate = picked;
        date =selectedDate.year.toString()+"-"+ selectedDate.month.toString() +"-" +selectedDate.day.toString() ;

        loginProvider.dateOfBirth=date;
      });
  }
  Widget build(BuildContext context) {
    loginProvider = Provider.of<ProviderLoginProvider>(context, listen: false);
    return new Scaffold(
      backgroundColor: ColorConstants.mainContainersColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:Container(
                decoration:BoxDecoration(
                  color: Colors.white30,

                ),
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(height: 80,),
                        Center(
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 5),
                              child: Image.asset(
                                'assets/images/logo22.png', height: 50, width: 200,
                              ),
                            ),
                            //child: Text("Mediclinic Software", style: TextStyle(color: Colors.blue,fontSize: 35,fontFamily: UiConstants.segeoLightFont , fontWeight: FontWeight.bold) ,),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30,20,0,0),
                              child: Container(
                                child: Text("Login", style: TextStyle(color: Colors.black,fontSize: 30,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                              ),
                            )
                          ],
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(30,20,40,20),
                            child: Container(
                              height: 50,
                              child:new TextFormField(
                                onChanged: (val) => loginProvider.surname = val,
                                onSaved: (val) => loginProvider.surname = val,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Username can not be blank';
                                  }

                                  return null;
                                },
                                decoration: new InputDecoration(
                                    labelText: "Username",

                                    border: new OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(30.0),
                                      ),
                                    ),
                                    filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                    hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                    hintText: "Username",
                                    fillColor: ColorConstants.lightgray),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 0,),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(30,0,40,0),
                            child: Container(
                              height: 50,
                              child:new TextFormField(
                                onChanged: (val) => loginProvider.surname = val,
                                onSaved: (val) => loginProvider.surname = val,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Password can not be blank';
                                  }

                                  return null;
                                },
                                decoration: new InputDecoration(
                                    labelText: "Password",

                                    border: new OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(30.0),
                                      ),
                                    ),
                                    filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                    hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                    hintText: "Password",
                                    fillColor: ColorConstants.lightgray),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: 200,
                                child: Text(
                                  "Please enter second, fourth, and last digit from  password",
                                  style: TextStyle( color: Colors.black,
                                      fontFamily: UiConstants.segeoRegularFont,
                                      fontSize: 12, fontWeight: FontWeight.normal
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(30,0,40,20),
                            child: Container(
                              height: 50,
                              child: PinEntryTextField(
                                //onSubmit: (val) => accessCodeProvider.checkMobileVerifyModel.code = val
                              ),
                            ),
                          ),
                        ),

                        Center(
                          child: Container(
                            width: 200,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0,20,0,20),
                              child:   RaisedButton(
                                color:Colors.blue,
                                textColor: ColorConstants.primaryColor,
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(30.0),
                                    side: BorderSide(
                                        color: Colors.blue)),

                                child: Text(
                                  "Login",
                                  style: TextStyle( color: Colors.white,
                                      fontFamily: UiConstants.segeoRegularFont,
                                      fontSize: 16, fontWeight: FontWeight.normal
                                  ),
                                ),
                                onPressed: () {
                                  //  Navigator.pushNamed(context, RoutesConstants.providerMainMenuPage);
                                  loginProvider.navService.navigateTo(RoutesConstants.providerMainMenuPage);
                                },
                              ),
                            ),
                          ),
                        ),

                        Center(
                          child: Container(

                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                  child:  Text("By Logging in , you agree  to the ",style: TextStyle(color: Colors.black45,fontSize: 12, fontFamily: UiConstants.segeoLightFont),),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                  child:    Container(
                                    child: FlatButton(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                                      onPressed: () {

                                      },
                                      /*     child: Text(
                                      "Terms & Conditions Of Use Security Policies",
                                      style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 12,
                                        fontFamily: UiConstants.segeoRegularFont,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),*/
                                      child:      Container(
                                        child: new InkWell(
                                          child: FlatButton(
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              onPressed: () {
                                                //  Navigator.pop(context);
                                              },
                                              child: Text("Terms & Conditions Of Use Security Policies",
                                                  style: TextStyle(
                                                      color: Colors.blueGrey,
                                                      fontSize: 12,
                                                      decoration: TextDecoration.underline, fontFamily: UiConstants.segeoRegularFont,
                                                      fontStyle: FontStyle.normal))),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10,),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                  child:    Container(
                                    child: FlatButton(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                                      onPressed: () {

                                      },
                                      child: Text(
                                        "Lost you phone? | Forgot your password?",
                                        style: TextStyle(
                                          color: Colors.black45,
                                          fontSize: 12,
                                          fontFamily: UiConstants.segeoLightFont,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                        child:    Container(
                                          child: FlatButton(
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                                            onPressed: () {

                                            },
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "Don't have account?",
                                                  style: TextStyle(
                                                    color: Colors.black45,
                                                    fontSize: 16,
                                                    fontFamily: UiConstants.segeoLightFont,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                                Text(
                                                  "SignUp",
                                                  style: TextStyle(
                                                    color: Colors.blueGrey,
                                                    fontSize: 16,
                                                    fontFamily: UiConstants.segeoRegularFont,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],

                ),
              ),
            ),
          ),
        ));
  }
}


