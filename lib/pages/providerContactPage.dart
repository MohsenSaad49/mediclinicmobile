import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/forms/appointmentClinicForm.dart';
import 'package:mediclinic/providers/patientContactProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/providerContactProvider.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/providers/siteSelectionProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';
import 'package:simple_tooltip/simple_tooltip.dart';

class ProviderContactPage extends StatefulWidget {
  @override
  ProviderContactPageState createState() {
    return ProviderContactPageState();
  }
}

class ProviderContactPageState extends  State<ProviderContactPage> {
  ProviderContactProvider   providerContactProvider   ;
  bool show=false;
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  @override
  Widget build(BuildContext context) {
    providerContactProvider = Provider.of<ProviderContactProvider>(context, listen: true);

    return Container(
      child:  AppStateHandlerWidget(
        state: providerContactProvider.loadingState,
        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.fromLTRB(10,20,10,10),
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(15,0,0,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        GestureDetector(
                                          child:Container(
                                            width: 35, height: 35,
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                              child:IconButton(
                                                icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                                color: ColorConstants.primaryColor,
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                              color: Colors.blueAccent,
                                              borderRadius: BorderRadius.circular(25),
                                            ),
                                          ),
                                          onTap: (){
                                            Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                          },
                                        ),
                                      /*  GestureDetector(
                                          child:Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.only(top:10),
                                                  child: Container(  child: Text('Why select preference',style: TextStyle(fontSize: 13,fontFamily: UiConstants.segeoRegularFont),maxLines: 2,)),
                                                ),

                                                SimpleTooltip(
                                                  tooltipTap: () {
                                                    print("Tooltip tap");
                                                  },
                                                  animationDuration: Duration(milliseconds: 500),
                                                  show: show, backgroundColor: Colors.blueAccent, borderColor: Colors.transparent,
                                                  tooltipDirection: TooltipDirection.left, arrowTipDistance: 2,
                                                  child: IconButton(

                                                    icon:Icon(Icons.help, color: Colors.blueAccent,size: 40, ),
                                                    color: ColorConstants.primaryColor,
                                                  ),
                                                  content: Text(
                                                    "our system found you registered in multiple Clinic groups , please select the clinic group to manage your bookin",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 13, fontFamily: UiConstants.segeoLightFont,
                                                      decoration: TextDecoration.none,
                                                    ),
                                                  ),
                                                ),


                                              ],),
                                          ),
                                          onTap: (){
                                            setState(() {
                                              show=!show;
                                            });
                                          },
                                        )*/

                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 10,),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Contact",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),


                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20.0,0,130,0),
                                    child: Divider(color: Colors.grey, thickness: 0.5,),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                            width:300,
                                            child: Text("You can contact the clinic staff directly from ouraction button.",maxLines: 2,style: TextStyle(fontSize: 13, color:Colors.black,fontWeight: FontWeight.normal, fontFamily: UiConstants.segeoRegularFont),)),


                                      ],
                                    ),
                                  ),

                                ],
                              ),
                            ),

                            SizedBox(height: 25,),
                            Container(
                              height: 400,
                              child: ListView.builder(
                                  itemCount: 2,
                                  itemBuilder: (context, index) {
                                    return   GestureDetector(
                                      onTap: (){
                                       // Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                      },
                                     /* child: Padding(
                                        padding: const EdgeInsets.fromLTRB(8,8,8,0),
                                        child: Container(height: 160,
                                          padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                          decoration:BoxDecoration(
                                            color:Colors.white,
                                            border: Border.all(
                                              color: ColorConstants.actionsColor,
                                              width: 1,
                                            ),
                                            borderRadius: BorderRadius.circular(15),
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[
                                                  Padding(
                                                    padding: const EdgeInsets.all(0),
                                                    child: FittedBox(
                                                      child: Image.asset(
                                                        "assets/images/frontshop.png",
                                                      ),
                                                      fit: BoxFit.fitHeight,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Container(
                                                width: 205,
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(8,8,8,0),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Row(children: <Widget>[

                                                        Padding(
                                                          padding: const EdgeInsets.only(top:5,left: 5),
                                                          child: Container( width: 130, child: Text('Business A',style: TextStyle(fontSize: 14,fontFamily: UiConstants.segeoRegularFont,fontWeight:FontWeight.bold),maxLines: 2,)),
                                                        )
                                                      ],),
                                                      SizedBox(height: 5,),
                                                      Row(children: <Widget>[
                                                        Image.asset(
                                                          "assets/images/global.png", height: 18, width: 18,
                                                        ),
                                                        SizedBox(width: 5,),
                                                        Padding(
                                                          padding: const EdgeInsets.only(top:5),
                                                          child: Container( width: 130, child: Text('494 WHITEHOUSE ROAD SURREY HILLS VIC,3127 ',style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                        )
                                                      ],),
                                                      SizedBox(height: 5,),
                                                      Row(children: <Widget>[
                                                        Icon(Icons.mail_outline,color: ColorConstants.actionsColor,size: 18,),
                                                        SizedBox(width: 5,),
                                                        Container( width: 150, child: Text('info@MediClinic.com.au',style: TextStyle(fontSize: 10),maxLines: 2,))
                                                      ],),
                                                      SizedBox(height: 5,),
                                                      Row(children: <Widget>[
                                                        Icon(Icons.phone_in_talk,color: ColorConstants.actionsColor,size: 18,),
                                                        SizedBox(width: 5,),
                                                        Container( width: 150, child: Text('03 9947 1981',style: TextStyle(fontSize: 10),maxLines: 2,))
                                                      ],),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,

                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(0,10,0,0),
                                                            child: Container(
                                                              height: 30,
                                                              width:80,
                                                              child: RaisedButton(
                                                                color:Colors.blueAccent,
                                                                textColor: ColorConstants.primaryColor,
                                                                elevation: 1,
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius: new BorderRadius.circular(20.0),
                                                                    side: BorderSide(
                                                                        color: Colors.blueAccent)),
                                                                child: Text(
                                                                  "Select",
                                                                  style: TextStyle( color: Colors.white,
                                                                      fontFamily: UiConstants.segeoLightFont,
                                                                      fontSize: 15, fontWeight: FontWeight.normal
                                                                  ),
                                                                ),
                                                                onPressed: () {

                                                                  Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                                                },
                                                              ),
                                                            ),
                                                          )
                                                        ],),
                                                    ],
                                                  ),
                                                ),
                                              )


                                            ],
                                          ),

                                        ),
                                      ),*/

                                     child:  Padding(
                                       padding: const EdgeInsets.all(8.0),
                                       child: Container(
                                         decoration:BoxDecoration(
                                           color:Colors.white,
                                           border: Border.all(
                                             color: ColorConstants.actionsColor,
                                             width: 1,
                                           ),
                                           borderRadius: BorderRadius.circular(30),
                                         ),
                                         child: Column(
                                           children: <Widget>[
                                             Padding(
                                               padding: const EdgeInsets.all(5.0),
                                               child: Row(
                                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                 children: <Widget>[
                                                   Column(
                                                     children: <Widget>[
                                                       Row(
                                                         children: <Widget>[
                                                           Text('Healthy Life West Clinic',style: TextStyle(fontSize: 14,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),maxLines: 2,),

                                                         ],
                                                       ),
                                                       SizedBox(height: 10,),
                                                       Padding(
                                                         padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                         child: Row(
                                                           mainAxisAlignment: MainAxisAlignment.end,
                                                           children: <Widget>[
                                                             Row(children: <Widget>[
                                                               Icon(Icons.location_city,color: ColorConstants.actionsColor,size: 18,),
                                                               SizedBox(width: 5,),
                                                               Padding(
                                                                 padding: const EdgeInsets.only(top:5),
                                                                 child: Container( width: 130, child: Text('494 WHITEHOUSE ROAD SURREY HILLS VIC,3127 ',style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                               )
                                                             ],),

                                                           ],
                                                         ),
                                                       ),
                                                       SizedBox(height: 10,),
                                                       Padding(
                                                         padding: const EdgeInsets.fromLTRB(10,0,0,0),
                                                         child: Row(
                                                           mainAxisAlignment: MainAxisAlignment.end,
                                                           children: <Widget>[
                                                             Row(children: <Widget>[
                                                               Image.asset(
                                                                 "assets/images/global.png", height: 18, width: 18,
                                                               ),
                                                               SizedBox(width: 5,),
                                                               Padding(
                                                                 padding: const EdgeInsets.only(top:5),
                                                                 child: Container( width: 130, child: Text('www.mediclinic.com.au',style: TextStyle(fontSize: 10),maxLines: 2,)),
                                                               )
                                                             ],),

                                                           ],
                                                         ),
                                                       ),
                                                     ],
                                                   ),
                                                   Column(
                                                     children: <Widget>[

                                                       SizedBox(height: 30,),
                                                       Padding(
                                                         padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                         child: Row(
                                                           mainAxisAlignment: MainAxisAlignment.end,
                                                           children: <Widget>[

                                                             Row(children: <Widget>[
                                                               Icon(Icons.phone_in_talk,color: ColorConstants.actionsColor,size: 18,),
                                                               SizedBox(width: 5,),
                                                               Container( width: 120, child: Text('03 9947 1981',style: TextStyle(fontSize: 10),maxLines: 2,))
                                                             ],),

                                                             SizedBox(height: 5,),


                                                           ],
                                                         ),
                                                       ),
                                                       SizedBox(height: 15,),
                                                       Padding(
                                                         padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                                         child: Row(
                                                           mainAxisAlignment: MainAxisAlignment.end,
                                                           children: <Widget>[

                                                             Row(children: <Widget>[
                                                               Icon(Icons.mail_outline,color: ColorConstants.actionsColor,size: 18,),
                                                               SizedBox(width: 5,),
                                                               Container(width: 120, child: Text('info@MediClinic.com.au',style: TextStyle(fontSize: 10),maxLines: 2,))
                                                             ],),
                                                           ],
                                                         ),
                                                       ),
                                                     ],
                                                   ),


                                                 ],
                                               ),
                                             ),

                                             Padding(
                                               padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                               child: Divider(color: Colors.blueAccent, thickness: 0.5,),
                                             ),
                                             GestureDetector(
                                               onTap: (){

                                                 showModalBottomSheet(
                                                     context: context,
                                                     isScrollControlled:
                                                     true,
                                                     elevation: 1,
                                                     isDismissible: true,
                                                     backgroundColor:
                                                     Colors
                                                         .white,
                                                     builder: (builder) {

                                                       return Container(
                                                         height: 300,
                                                         child: Column(
                                                           children: <Widget>[
                                                             Padding(
                                                               padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                               child:           Column(
                                                                 children: <Widget>[

                                                                   Row(
                                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                     children: <Widget>[
                                                                       Padding(
                                                                         padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                         child: Text(
                                                                           "GetDirection",
                                                                           style: TextStyle( color: Colors.blueGrey[700],
                                                                               fontFamily: UiConstants.segeoLightFont,
                                                                               fontSize: 18, fontWeight: FontWeight.bold
                                                                           ),
                                                                         ),
                                                                       ),
                                                                       GestureDetector(
                                                                         onTap: (){Navigator.pop(context);},
                                                                           child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                     ],
                                                                   ),

                                                                   SizedBox(height: 5,),
                                                                   Padding(
                                                                     padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                     child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                   ),

                                                                   Row(
                                                                     children: <Widget>[
                                                                       Padding(
                                                                         padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                         child: Text(
                                                                           "Call Clinic",
                                                                           style: TextStyle( color: Colors.green,
                                                                               fontFamily: UiConstants.segeoLightFont,
                                                                               fontSize: 18, fontWeight: FontWeight.bold
                                                                           ),
                                                                         ),
                                                                       ),
                                                                     ],
                                                                   ),
                                                                   SizedBox(height: 5,),
                                                                   Padding(
                                                                     padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                     child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                   ),
                                                                   Row(
                                                                     children: <Widget>[
                                                                       Padding(
                                                                         padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                         child: Text(
                                                                           "SMS",
                                                                           style: TextStyle( color: Colors.red,
                                                                               fontFamily: UiConstants.segeoLightFont,
                                                                               fontSize: 18, fontWeight: FontWeight.bold
                                                                           ),
                                                                         ),
                                                                       ),
                                                                     ],
                                                                   ),
                                                                   SizedBox(height: 5,),
                                                                   Padding(
                                                                     padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                     child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                   ),
                                                                   Row(
                                                                     children: <Widget>[
                                                                       Padding(
                                                                         padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                         child: Text(
                                                                           "Email",
                                                                           style: TextStyle( color:ColorConstants.darkBluecolor,
                                                                               fontFamily: UiConstants.segeoLightFont,
                                                                               fontSize: 18, fontWeight: FontWeight.bold
                                                                           ),
                                                                         ),
                                                                       ),
                                                                     ],
                                                                   ),

                                                                 ],
                                                               ),
                                                             ),
                                                           ],
                                                         ),
                                                       );

                                                     }
                                                 );

                                               },
                                               child: Container(

                                                 child: Row(
                                                   mainAxisAlignment: MainAxisAlignment.center,
                                                   children: <Widget>[
                                                     Column(
                                                       mainAxisSize: MainAxisSize.min,
                                                       children: <Widget>[
                                                         Text("Actions ",style: TextStyle(fontSize: 15,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                                                         Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                       ],
                                                     )
                                                   ],
                                                 ),
                                               ),
                                             )
                                           ],
                                         ),

                                       ),
                                     ),
                                    );
                                  }),
                            ),

                            Divider(color: Colors.blueAccent, thickness: 2,),

                            GestureDetector(
                              onTap: (){
                                //patientBookingProvider.showDialog();
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    DefaultIconButton(
                                      labelText: 'Bookings',
                                      iconWidget: Image.asset(
                                        'assets/images/calender.png',width: 20, height: 23,),
                                      onPressed: () {
                                        Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Invoices',
                                      iconWidget: Image.asset(
                                        'assets/images/invoices2.png',width: 20, height: 23,),
                                      onPressed: () {
                                        // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Contact',
                                      iconWidget: Image.asset(
                                        'assets/images/comment.png',width: 20, height: 23,),
                                      onPressed: () async {
                                        //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                        //onNavigate(true);

                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Profile',
                                      iconWidget: Image.asset(
                                        'assets/images/user2.png',width: 20, height: 23,),
                                      onPressed: () {
                                         Navigator.pushNamed(context, RoutesConstants.patientProfileMainPage);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
