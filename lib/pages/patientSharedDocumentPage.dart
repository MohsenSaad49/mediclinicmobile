import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/patientSharedDocumentProvider.dart';
import 'package:mediclinic/providers/providerPatientProfileProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class PatientSharedDocumentPage extends StatefulWidget {
  @override
  PatientSharedDocumentPageState createState() {
    return PatientSharedDocumentPageState();
  }
}
class PatientSharedDocumentPageState extends  State<PatientSharedDocumentPage> {
  PatientSharedDocumentProvider  patientSharedDocumentProvider;
  String selectedCountry;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String date = "DD/MM/YYYY";


  @override
  Widget build(BuildContext context) {
    patientSharedDocumentProvider = Provider.of<PatientSharedDocumentProvider>(context, listen: false);
    return Form(
      key: _formKey,
      child: new Scaffold(

          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height-100,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            children: <Widget>[


                              Stack(
                                overflow: Overflow.visible,
                                children: <Widget>[
                                  Container(
                                    padding:EdgeInsets.fromLTRB(5,0,10,10),
                                    decoration:BoxDecoration(
                                      color: ColorConstants.mainContainersColor,
                                      border: Border.all(color: ColorConstants.actioncolor2, width: 2,),
                                      borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        /*       Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 30,),
                                          ),
                                        ],
                                      ),*/
                                        SizedBox(height: 5,),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: GestureDetector(
                                                child: Container(
                                                  width: 40, height: 40,
                                                  child: IconButton(
                                                    icon:Icon(Icons.arrow_back_ios,color: Colors.white,size: 25,),
                                                    color: Colors.transparent,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    color: Colors.blueAccent,
                                                    borderRadius: BorderRadius.circular(25),
                                                  ),
                                                ),
                                                onTap: (){
                                                  Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(40,5,10,10),
                                              child: Text("Shared Document",style: TextStyle(fontSize: 24,color: ColorConstants.darkBluecolor, fontWeight: FontWeight.bold),),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(40,0,140,10),
                                          child: Divider(height: 5,thickness: 1,color: Colors.grey,),
                                        ),

                                        SizedBox(height: 15,),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    bottom: -35,
                                    left: 30,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: GestureDetector(
                                              onTap: (){
                                               // Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                                              },
                                              child: Container(
                                                padding:EdgeInsets.fromLTRB(5, 5, 5, 5),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[

                                                    Container(
                                                      padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                      decoration: BoxDecoration(
                                                        color: ColorConstants.defaultBoxColor,
                                                        border: Border.all(
                                                          color: Colors.white,
                                                          width: 0,
                                                        ),
                                                        borderRadius: BorderRadius.circular(25),
                                                      ),
                                                      child: Image.asset(
                                                        'assets/images/cam.png',width: 16, height: 18,),


                                                    ),

                                                    SizedBox(width: 5,),
                                                    Text("Take Picture",style: TextStyle(fontSize: 13,color: Colors.white,fontFamily: UiConstants.segeoRegularFont),),

                                                  ],
                                                ),
                                                decoration: BoxDecoration(

                                                  color: Colors.blueAccent,

                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: GestureDetector(
                                              onTap: (){
                                               // Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                                              },
                                              child: Container(
                                                padding:EdgeInsets.fromLTRB(10, 10, 5, 10),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text("UPLOAD DOCUMENT",style: TextStyle(fontSize: 13,color: ColorConstants.actioncolor2,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                    /*SizedBox(width: 10,),
                                                    Container(
                                                      padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                      decoration: BoxDecoration(
                                                        color: ColorConstants.defaultBoxColor,
                                                        border: Border.all(
                                                          color: Colors.white,
                                                          width: 0,
                                                        ),
                                                        borderRadius: BorderRadius.circular(25),
                                                      ),
                                                      child: Image.asset(
                                                        'assets/images/calender.png',width: 16, height: 18,),


                                                    )*/
                                                  ],
                                                ),
                                                decoration: BoxDecoration(

                                                  color:ColorConstants.mainContainersColor,
                                                  border: Border.all(color: ColorConstants.actioncolor2, width: 1,),
                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                ],
                              ),



                              SizedBox(height: 40,),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width:250,
                                    decoration:BoxDecoration(
                                      color: Colors.blue,
                                      border: Border.all(
                                        color: Colors.blue,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                          child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                        ),
                                        Text("Your Document",style: TextStyle(color: Colors.white,fontSize: 15),)
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10,),
                              Container(
                                height: 300,
                                child: ListView.builder(
                                    itemCount: 2,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      return   GestureDetector(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(12,10,12,0),
                                          child: Container(
                                            decoration:BoxDecoration(
                                              color:Colors.white,
                                              border: Border.all(
                                                color: ColorConstants.actionsColor,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(0,10,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets.only(left:0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: <Widget>[

                                                            Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left:20),
                                                                  child:  Text("Patient ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                                ),
                                                              ],
                                                            ),
                                                            Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left:10),
                                                                  child:  Text("John Clark",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                                ),
                                                              ],
                                                            ),


                                                          ],
                                                        ),
                                                      ),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          Text("Added Date",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                          SizedBox(width: 5,),
                                                          Row(
                                                            children: <Widget>[

                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(8.0,0,10,0),
                                                                child: Text("28/05/2020",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,)),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(0,14,2,5),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[

                                                      Column(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(left:20),
                                                            child:  Text("File ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                          ),
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(left:10),
                                                            child:  Text(": Patient EPC.jpeg",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                          ),
                                                        ],
                                                      ),


                                                    ],
                                                  ),
                                                ),



                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                                  child: Divider(color: ColorConstants.actionsColor, thickness: 0.5,),
                                                ),
                                                GestureDetector(
                                                  onTap: (){
                                                    showModalBottomSheet(
                                                        context: context,
                                                        isScrollControlled:
                                                        true,
                                                        elevation: 1,
                                                        isDismissible: true,
                                                        backgroundColor:
                                                        Colors
                                                            .white,
                                                        builder: (builder) {

                                                          return Container(
                                                            height: 300,
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                                  child:           Column(
                                                                    children: <Widget>[

                                                                      Row(
                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Selected Patient EPC.Jpeg",
                                                                              style: TextStyle( color: Colors.blueGrey[700],
                                                                                  fontFamily: UiConstants.segeoLightFont, decoration: TextDecoration.underline,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          GestureDetector(
                                                                              onTap: (){Navigator.pop(context);},
                                                                              child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                        ],
                                                                      ),

                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),

                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Share with Patient",
                                                                              style: TextStyle( color: Colors.green,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),
                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Delete File",
                                                                              style: TextStyle( color: Colors.red,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),
                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Request document to my email",
                                                                              style: TextStyle( color:ColorConstants.darkBluecolor,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),

                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        }
                                                    );
                                                  },
                                                  child: Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Column(
                                                          mainAxisSize: MainAxisSize.max,
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(left: 8),
                                                              child: Text("Actions ",style: TextStyle(fontSize: 15,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont),),
                                                            ),
                                                            Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),

                                          ),
                                        ),
                                      );
                                    }
                                ),
                              ),


                            ],
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){


                      //  providerPatientProfileProvider.addPatient();
                        // patientBookingProvider.showDialog();
                      },
                      child: Container(
                        color: ColorConstants.lightgray,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,0,0,0),
                              child: Divider(height: 5,thickness: 1,color:ColorConstants.actionsColor,),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  DefaultIconButton(
                                    labelText: 'Bookings',
                                    iconWidget: Image.asset(
                                      'assets/images/calender.png',width: 20, height: 23,),
                                    onPressed: () {
                                      //Navigator.pushNamed(context, RoutesConstants.callLogPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Invoices',
                                    iconWidget: Image.asset(
                                      'assets/images/invoices2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Contact',
                                    iconWidget: Image.asset(
                                      'assets/images/comment.png',width: 20, height: 23,),
                                    onPressed: () async {
                                      //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                      //onNavigate(true);

                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Profile',
                                    iconWidget: Image.asset(
                                      'assets/images/user2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.transactionPage);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
    }
}
