import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:mediclinic/LogicUtilities.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/forms/appointmentClinicForm.dart';
import 'package:mediclinic/providers/patientContactProvider.dart';
import 'package:mediclinic/providers/patientInvoiceProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/patientProfileMainProvider.dart';
import 'package:mediclinic/providers/providerContactProvider.dart';
import 'package:mediclinic/providers/providerInvoiceProvider.dart';
import 'package:mediclinic/providers/providerMainMenuProvider.dart';
import 'package:mediclinic/providers/siteSelectionProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';
import 'package:simple_tooltip/simple_tooltip.dart';

class PatientInvoicePage extends StatefulWidget {
  @override
  PatientInvoicePageState createState() {
    return PatientInvoicePageState();
  }
}

class PatientInvoicePageState extends  State<PatientInvoicePage> {
  PatientInvoiceProvider    patientInvoiceProvider   ;
  bool show=false;
  int _value = 1;

  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  @override
  Widget build(BuildContext context) {
    patientInvoiceProvider = Provider.of<PatientInvoiceProvider>(context, listen: true);

    return Container(
      child:  AppStateHandlerWidget(

        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:ListView(
                    children: <Widget>[
                      Container(
                        decoration:BoxDecoration(
                          color: Colors.white30,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.fromLTRB(10,0,10,10),
                              decoration:BoxDecoration(
                                color: ColorConstants.mainContainersColor,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                              ),
                              child: Column(
                                children: <Widget>[

                                  SizedBox(height: 10,),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0.0,0,0,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(15,0,0,0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              GestureDetector(
                                                child:Container(
                                                  width: 35, height: 35,
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(0.0,0,20,0),

                                                    child:IconButton(
                                                      icon:Icon(Icons.arrow_back_ios, color: Colors.white,size: 20, ),
                                                      color: ColorConstants.primaryColor,
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    color: Colors.blueAccent,
                                                    borderRadius: BorderRadius.circular(25),
                                                  ),
                                                ),
                                                onTap: (){
                                                  Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                                },
                                              ),


                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20,),
                                        Text("Invoice Details",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),


                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(70.0,0,130,0),
                                    child: Divider(color: Colors.grey, thickness: 0.5,),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(70.0,0,8,0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                            width:250,
                                            child: Text("Invoice details are shown by private patientonly. If you want to see Medicare / DVA invoice, please contact the clinic staff.",maxLines: 2,style: TextStyle(fontSize: 13, color:Colors.black,fontWeight: FontWeight.normal, fontFamily: UiConstants.segeoRegularFont),)),


                                      ],
                                    ),
                                  ),

                                ],
                              ),
                            ),

                            SizedBox(height: 10,),
                            GestureDetector(
                              onTap: (){
                                showModalBottomSheet(
                                    context: context,
                                    isScrollControlled:
                                    true,
                                    elevation: 1,
                                    isDismissible: true,
                                    backgroundColor:
                                    Colors
                                        .white,
                                    builder: (builder) {

                                      return Container(
                                        height: 380,
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                              child:           Column(
                                                children: <Widget>[

                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                        child: Text(
                                                          "Sort by",
                                                          style: TextStyle( color: ColorConstants.darkBluecolor,
                                                              fontFamily: UiConstants.segeoLightFont,
                                                              fontSize: 24, fontWeight: FontWeight.bold
                                                          ),
                                                        ),
                                                      ),
                                                      GestureDetector(
                                                          onTap: (){Navigator.pop(context);},
                                                          child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                    ],
                                                  ),

                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                        child: Text(
                                                          "Filter by Clinic",
                                                          style: TextStyle( color: Colors.black,
                                                              fontFamily: UiConstants.segeoLightFont,
                                                              fontSize: 15, fontWeight: FontWeight.bold
                                                          ),
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(8.0,0,8,0),
                                                    child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                  ),

                                                  Container(
                                                    height: 150,
                                                    child:ListView(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(8.0,0,8,8),
                                                          child: RadioButtonGroup(
                                                            labels:patientInvoiceProvider.clinicFilter,
                                                            labelStyle: TextStyle( color: Colors.grey,
                                                                fontFamily: UiConstants.segeoLightFont,
                                                                fontSize: 12, fontWeight: FontWeight.bold
                                                            ),
                                                            onSelected: (String clinic) =>patientInvoiceProvider.selectedClinic=clinic,
                                                          ),
                                                        ),
                                                      ],

                                                    ),
                                                  ),

                                                  SizedBox(height: 20,),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      Container(
                                                        width: 160,
                                                        child: RaisedButton(
                                                          color:Colors.blueAccent,
                                                          textColor: ColorConstants.primaryColor,
                                                          elevation: 0.0,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: new BorderRadius.circular(30.0),
                                                              side: BorderSide(
                                                                  color: Colors.blueAccent)),

                                                          child: Text(
                                                            "Apply Filter",
                                                            style: TextStyle( color: Colors.white,
                                                                fontFamily: UiConstants.segeoRegularFont,
                                                                fontSize: 17, fontWeight: FontWeight.normal
                                                            ),
                                                          ),
                                                          onPressed: () {

                                                          },
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );

                                    }
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(15,10,15,0),
                                child: Container(

                                  child: Container(
                                      padding:EdgeInsets.fromLTRB(10, 5, 5, 5),

                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Text("Sort by ",style: TextStyle(fontSize: 14,color: ColorConstants.primaryBlueColors,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                          Image.asset(
                                            'assets/images/filter2.png',width: 10, height: 10,),

                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: ColorConstants.actionsColor,
                                          width: 1,
                                        ),
                                        borderRadius: BorderRadius.circular(30),
                                      )
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              height: 420,
                              child: AppStateHandlerWidget(
                                state: patientInvoiceProvider.loadingState,
                                child: ListView.builder(
                                    itemCount: patientInvoiceProvider.allInvoices==null?0:patientInvoiceProvider.allInvoices.length,
                                    itemBuilder: (context, index) {
                                      return   GestureDetector(
                                        onTap: (){
                                         // Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                        },

                                       child:  Padding(
                                         padding: const EdgeInsets.all(8.0),
                                         child: Container(
                                           decoration:BoxDecoration(
                                             color:Colors.white,
                                             border: Border.all(
                                               color: ColorConstants.actionsColor,
                                               width: 1,
                                             ),
                                             borderRadius: BorderRadius.circular(30),
                                           ),
                                           child: Column(
                                             children: <Widget>[
                                               Padding(
                                                 padding: const EdgeInsets.all(5.0),
                                                 child: Row(
                                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                   children: <Widget>[
                                                     Column(
                                                       crossAxisAlignment: CrossAxisAlignment.start,
                                                       children: <Widget>[
                                                         Padding(
                                                           padding: const EdgeInsets.fromLTRB(0,10,0,0),
                                                           child: Row(
                                                             children: <Widget>[
                                                               Column(
                                                                 children: <Widget>[
                                                                   Padding(
                                                                     padding: const EdgeInsets.fromLTRB(8.0,0,0,0),
                                                                     child: Text("Invoice date",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                   ),

                                                                 ],
                                                               ),
                                                               SizedBox(width: 20,),
                                                               Column(
                                                                 children: <Widget>[
                                                                   Text(patientInvoiceProvider.allInvoices[index].invoiceDateAdded,style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                                 ],
                                                               ),
                                                               SizedBox(width: 60,),
                                                               Column(
                                                                 children: <Widget>[
                                                                   Text("Invoice ID",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                                 ],
                                                               ),
                                                               SizedBox(width: 5,),

                                                               Column(
                                                                 children: <Widget>[
                                                                   Text(patientInvoiceProvider.allInvoices[index].invoiceId.toString(),style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                                 ],
                                                               ),
                                                             ],
                                                           ),
                                                         ),

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Patient name",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 10,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text(patientInvoiceProvider.allInvoices[index].patientFullName(),style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.fromLTRB(8.0,0,0,0),
                                                                   child: Text("Appointment",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 15,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text(patientInvoiceProvider.allInvoices[index].bookingDate,style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Debtor          ",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 12,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text("",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),

                                                     /*    Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.fromLTRB(8.0,0,0,0),
                                                                   child: Text("Billing          ",style: TextStyle(fontSize: 13,color: Colors.transparent,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 20,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text("**** **** **** 4568",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),*/

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Total Amout          ",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 10,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text( LogicUtilities.formatCurrency(patientInvoiceProvider.allInvoices[index].total),style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),
                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Paid          ",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 10,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text("",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),
                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Remaining          ",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 10,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Text("",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),

                                                               ],
                                                             ),

                                                           ],
                                                         ),

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.fromLTRB(8.0,0,0,0),
                                                                   child: Text("Items          ",style: TextStyle(fontSize: 13,color: ColorConstants.grayColor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                             SizedBox(width: 20,),
                                                             Column(
                                                               children: <Widget>[
                                                                 Row(
                                                                   children: <Widget>[
                                                                     Container(
                                                                         width:120,
                                                                         child: Text(patientInvoiceProvider.allInvoices[index].invoiceLines[0].serviceShortName+"*",

                                                                           softWrap: false, maxLines: 1,  overflow: TextOverflow.fade ,
                                                                           style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),)),
                                                                     Text(patientInvoiceProvider.allInvoices[index].invoiceLines[0].quantity.toString()+" = "+LogicUtilities.formatCurrency(patientInvoiceProvider.allInvoices[index].total),style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                   ],
                                                                 ),

                                                               ],
                                                             ),

                                                           ],
                                                         ),

                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: <Widget>[
                                                             Column(
                                                               children: <Widget>[
                                                                 Padding(
                                                                   padding: const EdgeInsets.all(8.0),
                                                                   child: Text("Invoice last Email :"+patientInvoiceProvider.allInvoices[index].lastDateEmailed,style: TextStyle(fontSize: 13,color: Colors.green,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                                 ),
                                                               ],
                                                             ),
                                                           ],
                                                         ),
                                                       ],
                                                     ),
                                                   ],
                                                 ),
                                               ),

                                               Padding(
                                                 padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                                 child: Divider(color: Colors.blueAccent, thickness: 0.5,),
                                               ),
                                               GestureDetector(
                                                 onTap: (){

                                                   showModalBottomSheet(
                                                       context: context,
                                                       isScrollControlled:
                                                       true,
                                                       elevation: 1,
                                                       isDismissible: true,
                                                       backgroundColor:
                                                       Colors
                                                           .white,
                                                       builder: (builder) {

                                                         return Container(
                                                           height: 110,
                                                           child: Column(
                                                             children: <Widget>[
                                                               Padding(
                                                                 padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                                 child:           Column(
                                                                   children: <Widget>[

                                                                     Row(
                                                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                       children: <Widget>[
                                                                         Padding(
                                                                           padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                           child: Text(
                                                                             "Request Invoice to my email",
                                                                             style: TextStyle( color: Colors.blueGrey[700],
                                                                                 fontFamily: UiConstants.segeoLightFont,
                                                                                 fontSize: 18, fontWeight: FontWeight.bold
                                                                             ),
                                                                           ),
                                                                         ),
                                                                         GestureDetector(
                                                                           onTap: (){Navigator.pop(context);},
                                                                             child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                       ],
                                                                     ),

                                                                     SizedBox(height: 5,),
                                                                     Padding(
                                                                       padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                       child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                     ),





                                                                   ],
                                                                 ),
                                                               ),
                                                             ],
                                                           ),
                                                         );

                                                       }
                                                   );

                                                 },
                                                 child: Container(

                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.center,
                                                     children: <Widget>[
                                                       Column(
                                                         mainAxisSize: MainAxisSize.min,
                                                         children: <Widget>[
                                                           Text("Actions ",style: TextStyle(fontSize: 15,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoLightFont,fontWeight: FontWeight.bold),),
                                                           Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                         ],
                                                       )
                                                     ],
                                                   ),
                                                 ),
                                               )
                                             ],
                                           ),

                                         ),
                                       ),
                                      );
                                    }),
                              ),
                            ),

                            Divider(color: Colors.blueAccent, thickness: 2,),

                            GestureDetector(
                              onTap: (){
                                //patientBookingProvider.showDialog();
                              },
                              child: Container(
                                height: 100,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      DefaultIconButton(
                                        labelText: 'Bookings',
                                        iconWidget: Image.asset(
                                          'assets/images/calender.png',width: 20, height: 23,),
                                        onPressed: () {
                                          Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                        },
                                      ),
                                      DefaultIconButton(
                                        labelText: 'Invoices',
                                        iconWidget: Image.asset(
                                          'assets/images/invoices2.png',width: 20, height: 23,),
                                        onPressed: () {
                                          // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                        },
                                      ),
                                      DefaultIconButton(
                                        labelText: 'Contact',
                                        iconWidget: Image.asset(
                                          'assets/images/comment.png',width: 20, height: 23,),
                                        onPressed: () async {
                                          //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                          //onNavigate(true);

                                        },
                                      ),
                                      DefaultIconButton(
                                        labelText: 'Profile',
                                        iconWidget: Image.asset(
                                          'assets/images/user2.png',width: 20, height: 23,),
                                        onPressed: () {
                                           Navigator.pushNamed(context, RoutesConstants.patientProfileMainPage);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
