import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/providerPatientProfileProvider.dart';
import 'package:mediclinic/providers/termsConditionsProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class TermsConditionsPage extends StatefulWidget {
  @override
  TermsConditionsPageState createState() {
    return TermsConditionsPageState();
  }
}
class TermsConditionsPageState extends  State<TermsConditionsPage> {
  TermsConditionsProvider  termsConditionsProvider;
  String selectedCountry;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String date = "DD/MM/YYYY";

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)

      setState(() {
        selectedDate = picked;
        date =selectedDate.year.toString()+"-"+ selectedDate.month.toString() +"-" +selectedDate.day.toString() ;
        termsConditionsProvider.dateOfBirth=date;
      });
  }

  @override
  Widget build(BuildContext context) {
    termsConditionsProvider = Provider.of<TermsConditionsProvider>(context, listen: false);
    return Form(
      key: _formKey,
      child: new Scaffold(

          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height-100,
                      child:   ListView(
                        children: <Widget>[
                          Column(
                            children: <Widget>[

                              Container(
                                padding:EdgeInsets.fromLTRB(5,0,10,10),
                                decoration:BoxDecoration(
                                  color: ColorConstants.mainContainersColor,
                                  border: Border.all(color: ColorConstants.actioncolor2, width: 2,),
                                  borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    /*       Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 30,),
                                            ),
                                          ],
                                        ),*/
                                    SizedBox(height: 5,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          child: Container(
                                            width: 40, height: 40,
                                            child: IconButton(
                                              icon:Icon(Icons.arrow_back_ios,color: Colors.blueAccent,size: 25,),
                                              color: Colors.transparent,
                                            ),
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              borderRadius: BorderRadius.circular(25),
                                            ),
                                          ),
                                          onTap: (){
                                            Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                          },
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(40,5,10,10),
                                          child: Text("Terms and Conditions",style: TextStyle(fontSize: 24,color: ColorConstants.darkBluecolor, fontWeight: FontWeight.bold),),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(40,0,120,10),
                                      child: Divider(height: 5,thickness: 1,color: Colors.grey,),
                                    ),


                                  ],
                                ),
                              ),




                              Column(

                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      maxLines: null, enabled: false, style: TextStyle(fontSize: 19,fontFamily: UiConstants.segeoRegularFont),
                                      keyboardType: TextInputType.multiline,
                                      initialValue:"Terms & Conditions Of Use",

                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      maxLines: null, enabled: false, style: TextStyle(fontSize: 14,fontFamily: UiConstants.segeoRegularFont),
                                      keyboardType: TextInputType.multiline,
                                      initialValue:"Please Agree To The Latest Terms & Conditions Of Use Before Continuing MEDICLINIC SOFTWARE AGREEMENT (End User Agreement EULA) Mediclinic Software is a Software As a Service. This agreement is governed under the laws of Germany where we have our Head Office. PLEASE READ THIS SOFTWARE AGREEMENT (AGREEMENT) CAREFULLY BEFORE USING MEDICLINIC SOFTWARE. BY USING MEDICLINIC SOFTWARE, YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS AGREEMENT. IF YOU DO NOT AGREE TO THE TERMS OF THIS AGREEMENT, DO NOT USE THE SOFTWARE. IF YOU DO NOT AGREE TO THE TERMS OF THE AGREEMENT, MEDICLINIC SOFTWARE WILL CEASE YOUR USE AND ACCESS. By use of the software, by your logging on and using the software you are deemed to agree to these terms and conditions in full. Where we use MEDICLINIC SOFTWARE this will refer to any company which may from time to time, own the name and business MEDICLINIC Software. Mediclinic Software is operational in many countries and abides by all laws of those countries at all times. The name Mediclinic Software will form part of the company name operating in various countries from time to time and therefore the agreement is with Mediclinic Software which will operate where you are based, and you and we will be bound by the laws of that country. Ownership of the software code and products is held by several of the Mediclinic Software companies equally. We promise that all data required to be stored under law in any country where data is not permitted to leave, will be stored in that country only. This means data required to be stored in Australia is always on Australian hosted servers at all times. Data stored under EU laws will be stored as per EU law. Data laws applicable to your location will be upheld and applied always. SAFELY stored means the use of highest available encryption software as defined by or exceeding, industry standards. Currently this is known as EV-A certificate encryption. We currently use DIGICERT. And many other security implementations. We are hosting data on the most secure Microsoft Secure Data centres as applicable. IMPORTANT NOTE: This software may not be used to reproduce materials. It is agreement for you only for reproduction of non-copyrighted materials, materials in which you own the copyright, or materials you are authorized or legally permitted to reproduce. This software may also be used for remote access to data files for listening between computers. Remote access of copyrighted data is only provided for lawful use or as otherwise legally permitted. If you are uncertain about your right to copy or permit access to any material you should contact your legal advisor. CONDITIONS OF USE We use COOKIES and you agree to us using COOKIES on your device when you use Mediclinic Software. Cookies enable us to make various parts of the software work for you. We do not pass this information to any third party nor do we use this information outside our program provided to you. PAYMENT of FEES You agree to pay the fees as charged to you from time to time that should you fail to pay the fee or fees requested in full including any third party access fees charged by Mediclinic Software to you upon issue of any invoice and within 24 hours of such issuance., otherwise Mediclinic Software has the right to disable your access to any server or computer or data file for update or any amendment whatsoever until full payment has been made and",

                                    ),
                                  ),

                                  SizedBox(height: 60,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                    Container(

                                      child: RaisedButton(
                                        color:Colors.blueAccent,
                                        textColor: ColorConstants.primaryColor,
                                        elevation: 0.0,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: new BorderRadius.circular(30.0),
                                            side: BorderSide(
                                                color: Colors.blueAccent)),

                                        child: Text(
                                          "Accept the Terms and Conditions",
                                          style: TextStyle( color: Colors.white,
                                              fontFamily: UiConstants.segeoRegularFont,
                                              fontSize: 14, fontWeight: FontWeight.normal
                                          ),
                                        ),
                                        onPressed: () {

                                          Navigator.pushNamed(
                                              context, RoutesConstants.patientLoginPage);
                                        },
                                      ),
                                    ),
                                  ],),

                                  SizedBox(height: 30,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(

                                        child: RaisedButton(
                                          color:Colors.transparent,
                                          textColor: Colors.transparent,
                                          elevation: 0.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: new BorderRadius.circular(30.0),
                                              side: BorderSide(
                                                  color: Colors.transparent)),

                                          child: Text(
                                            "Cancel",
                                            style: TextStyle( color: Colors.black.withOpacity(0.5),
                                                fontFamily: UiConstants.segeoRegularFont,
                                                fontSize: 19, fontWeight: FontWeight.normal
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, RoutesConstants.welcomePage);

                                          },
                                        ),
                                      ),
                                    ],)
                                ],
                              ),
                            ],
                          ),
                        ],

                      ),
                    ),
                    GestureDetector(
                      onTap: (){


                      //  providerPatientProfileProvider.addPatient();
                        // patientBookingProvider.showDialog();
                      },
                      child: Container(
                        color: ColorConstants.lightgray,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,0,0,0),
                              child: Divider(height: 5,thickness: 1,color:ColorConstants.actionsColor,),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  DefaultIconButton(
                                    labelText: 'Bookings',
                                    iconWidget: Image.asset(
                                      'assets/images/calender.png',width: 20, height: 23,),
                                    onPressed: () {
                                      //Navigator.pushNamed(context, RoutesConstants.callLogPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Invoices',
                                    iconWidget: Image.asset(
                                      'assets/images/invoices2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Contact',
                                    iconWidget: Image.asset(
                                      'assets/images/comment.png',width: 20, height: 23,),
                                    onPressed: () async {
                                      //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                      //onNavigate(true);

                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Profile',
                                    iconWidget: Image.asset(
                                      'assets/images/user2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.transactionPage);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
    }
}
