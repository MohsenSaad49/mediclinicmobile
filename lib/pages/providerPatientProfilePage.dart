import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/providerPatientProfileProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class ProviderPatientProfilePage extends StatefulWidget {
  @override
  ProviderPatientProfilePageState createState() {
    return ProviderPatientProfilePageState();
  }
}
class ProviderPatientProfilePageState extends  State<ProviderPatientProfilePage> {
  ProviderPatientProfileProvider  providerPatientProfileProvider;
  String selectedCountry;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate;
  String date = "DD/MM/YYYY";

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)

      setState(() {
        selectedDate = picked;
        date =selectedDate.year.toString()+"-"+ selectedDate.month.toString() +"-" +selectedDate.day.toString() ;
        providerPatientProfileProvider.dateOfBirth=date;
      });
  }

  @override
  Widget build(BuildContext context) {
    providerPatientProfileProvider = Provider.of<ProviderPatientProfileProvider>(context, listen: false);
    return Form(
      key: _formKey,
      child: new Scaffold(

          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                ),
                child:Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height-100,
                      child: ListView(
                        children: <Widget>[
                          Column(
                            children: <Widget>[


                              Stack(
                                overflow: Overflow.visible,
                                children: <Widget>[
                                  Container(
                                    padding:EdgeInsets.fromLTRB(5,0,10,10),
                                    decoration:BoxDecoration(
                                      color: ColorConstants.mainContainersColor,
                                      border: Border.all(color: ColorConstants.actioncolor2, width: 2,),
                                      borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        /*       Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 30,),
                                          ),
                                        ],
                                      ),*/
                                        SizedBox(height: 5,),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            GestureDetector(
                                              child: Container(
                                                width: 40, height: 40,
                                                child: IconButton(
                                                  icon:Icon(Icons.arrow_back_ios,color: Colors.blueAccent,size: 25,),
                                                  color: Colors.transparent,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: Colors.transparent,
                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                              onTap: (){
                                                Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                              },
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(40,5,10,10),
                                              child: Text("Patient Profile",style: TextStyle(fontSize: 24,color: ColorConstants.darkBluecolor, fontWeight: FontWeight.bold),),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(40,0,120,10),
                                          child: Divider(height: 5,thickness: 1,color: Colors.grey,),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(40,0,10,10),
                                                child: Text("You can edit patient details, register patient to clinic.",
                                                  overflow: TextOverflow.clip,
                                                  style: TextStyle(fontSize: 13,color: Colors.black45),),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 30,),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    bottom: -35,
                                    left: 30,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: GestureDetector(
                                              onTap: (){
                                               // Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                                              },
                                              child: Container(
                                                padding:EdgeInsets.fromLTRB(5, 5, 5, 5),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[

                                                    Container(
                                                      padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                      decoration: BoxDecoration(
                                                        color: ColorConstants.defaultBoxColor,
                                                        border: Border.all(
                                                          color: Colors.white,
                                                          width: 0,
                                                        ),
                                                        borderRadius: BorderRadius.circular(25),
                                                      ),
                                                      child: Image.asset(
                                                        'assets/images/cam.png',width: 16, height: 18,),


                                                    ),

                                                    SizedBox(width: 5,),
                                                    Text("Take Picture",style: TextStyle(fontSize: 13,color: Colors.white,fontFamily: UiConstants.segeoRegularFont),),

                                                  ],
                                                ),
                                                decoration: BoxDecoration(

                                                  color: Colors.blueAccent,

                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: GestureDetector(
                                              onTap: (){
                                               // Navigator.pushNamed(context, RoutesConstants.bookNewAppointmentPage);
                                              },
                                              child: Container(
                                                padding:EdgeInsets.fromLTRB(10, 10, 5, 10),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text("UPLOAD DOCUMENT",style: TextStyle(fontSize: 13,color: ColorConstants.actioncolor2,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
                                                    /*SizedBox(width: 10,),
                                                    Container(
                                                      padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                      decoration: BoxDecoration(
                                                        color: ColorConstants.defaultBoxColor,
                                                        border: Border.all(
                                                          color: Colors.white,
                                                          width: 0,
                                                        ),
                                                        borderRadius: BorderRadius.circular(25),
                                                      ),
                                                      child: Image.asset(
                                                        'assets/images/calender.png',width: 16, height: 18,),


                                                    )*/
                                                  ],
                                                ),
                                                decoration: BoxDecoration(

                                                  color:ColorConstants.mainContainersColor,
                                                  border: Border.all(color: ColorConstants.actioncolor2, width: 1,),
                                                  borderRadius: BorderRadius.circular(25),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                ],
                              ),


                              SizedBox(height: 50,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    width:200,
                                    decoration:BoxDecoration(
                                      color: Colors.blue,
                                      border: Border.all(
                                        color: Colors.blue,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                          child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                        ),
                                        Text("Basic Details",style: TextStyle(color: Colors.white,fontSize: 15),)
                                      ],
                                    ),

                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right:12.0),
                                    child: Container(
                                      width: 100,
                                      padding:EdgeInsets.fromLTRB(10, 5, 5, 5),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text("Edit",style: TextStyle(fontSize: 15,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont),),

                                        ],
                                      ),
                                      decoration: BoxDecoration(

                                        color: Colors.white,
                                        border: Border.all(color: ColorConstants.actioncolor2, width: 1,),
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 15,),
                              Container(

                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,0,15,0),
                                        child: Container(


                                          child: new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.username = val,
                                            onSaved: (val) => providerPatientProfileProvider.username = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'First name can not be blank';
                                              }

                                              return null;
                                            },
                                            decoration: new InputDecoration(
                                                labelText: "First name",
                                                enabled: true,

                                                border: new OutlineInputBorder( gapPadding: 0.5,
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),

                                                filled: false, alignLabelWithHint: false, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: UiConstants.hintTextStyle,
                                                hintText: "First name",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,20,15,0),
                                        child: Container(

                                          child: new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.surname = val,
                                            onSaved: (val) => providerPatientProfileProvider.surname = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'surname can not be blank';
                                              }

                                              return null;
                                            },
                                            decoration: new InputDecoration(
                                                labelText: "Surname",
                                                focusColor:    Colors.white,
                                                hoverColor: Colors.white,
                                                border: new OutlineInputBorder(
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),
                                                filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                                hintText: "Surname",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: () => _selectDate(context),
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(15,20,15,0),
                                              child: Container(

                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    color: Colors.black45,
                                                    width: 1,
                                                  ),
                                                  borderRadius: BorderRadius.circular(30),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(15, 15, 50, 15),
                                                      child: Text(date,
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 16,
                                                            fontFamily: UiConstants.segeoLightFont,
                                                            fontWeight: FontWeight.normal,
                                                            decorationStyle: TextDecorationStyle.wavy,
                                                          )),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.all(2.0),
                                                      child: Container(

                                                        decoration: BoxDecoration(

                                                          color: Colors.blueAccent,

                                                          borderRadius: BorderRadius.circular(25),
                                                        ),
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(8.0),
                                                          child: Image.asset(
                                                            'assets/images/calenderw.png',width: 20, height: 20,)
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,20,15,0),
                                        child: Container(

                                          child:new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.mobileNumber = val,
                                            onSaved: (val) => providerPatientProfileProvider.mobileNumber = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Mobile number can not be blank';
                                              }
                                              if ( !isNumeric(value)) {
                                                return 'phone number must be supplied';
                                              }
                                              return null;
                                            },
                                            keyboardType: TextInputType.phone,
                                            decoration: new InputDecoration(
                                                labelText: "Mobile number",

                                                border: new OutlineInputBorder(
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),
                                                filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                                hintText: "Mobile number",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,20,15,20),
                                        child: Container(

                                          child: new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.email = val,
                                            onSaved: (val) => providerPatientProfileProvider.email = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Email can not be blank';
                                              }
                                              if (!isEmail(value) ) {
                                                return 'Email must be supplied';
                                              }
                                              return null;
                                            },
                                            decoration: new InputDecoration(
                                                labelText: "Email",

                                                border: new OutlineInputBorder(
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),
                                                filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                                hintText: "Email",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),


                                  ],

                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width:250,
                                    decoration:BoxDecoration(
                                      color: Colors.blue,
                                      border: Border.all(
                                        color: Colors.blue,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                          child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                        ),
                                        Text("Address",style: TextStyle(color: Colors.white,fontSize: 15),)
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Container(

                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,0,15,0),
                                        child: Container(


                                          child: new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.username = val,
                                            onSaved: (val) => providerPatientProfileProvider.username = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Address 1 can not be blank';
                                              }

                                              return null;
                                            },
                                            decoration: new InputDecoration(
                                                labelText: "Address 1",
                                                enabled: true,

                                                border: new OutlineInputBorder( gapPadding: 0.5,
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),

                                                filled: false, alignLabelWithHint: false, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: UiConstants.hintTextStyle,
                                                hintText: "Address 1",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,20,15,0),
                                        child: Container(

                                          child: new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.surname = val,
                                            onSaved: (val) => providerPatientProfileProvider.surname = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Address 2 can not be blank';
                                              }

                                              return null;
                                            },
                                            decoration: new InputDecoration(
                                                labelText: "Address 2",
                                                focusColor:    Colors.white,
                                                hoverColor: Colors.white,
                                                border: new OutlineInputBorder(
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),
                                                filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                                hintText: "Address 2",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),

                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15,20,15,0),
                                        child: Container(

                                          child:new TextFormField(
                                            onChanged: (val) => providerPatientProfileProvider.mobileNumber = val,
                                            onSaved: (val) => providerPatientProfileProvider.mobileNumber = val,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Postcoode can not be blank';
                                              }
                                              if ( !isNumeric(value)) {
                                                return 'Postcoode must be supplied';
                                              }
                                              return null;
                                            },
                                            keyboardType: TextInputType.phone,
                                            decoration: new InputDecoration(
                                                labelText: "Postcoode",

                                                border: new OutlineInputBorder(
                                                  borderRadius: const BorderRadius.all(
                                                    const Radius.circular(30.0),
                                                  ),
                                                ),
                                                filled: true, alignLabelWithHint: true, contentPadding:EdgeInsets.fromLTRB(10,10,0,0) ,
                                                hintStyle: new TextStyle(color: ColorConstants.graylightcolor),
                                                hintText: "Postcoode",
                                                fillColor: ColorConstants.lightgray),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 15,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context).size.width-30,
                                          padding:  const EdgeInsets.fromLTRB(10,0,5,0),
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            border: Border.all(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                            borderRadius: BorderRadius.circular(30),
                                          ),
                                          child: DropdownButtonHideUnderline(
                                            child: new DropdownButton<String>(
                                              value: selectedCountry,
                                              icon:  Icon(Icons.keyboard_arrow_down,size: 35, color: Colors.blueAccent,),
                                              hint: Row(
                                                children: <Widget>[
                                                  Text('Select Country                 '),
                                                  SizedBox(width: 95,)
                                                ],
                                              ),
                                              items: <String>['Germany', 'Egypt', 'USA',].map((String value) {
                                                return new DropdownMenuItem<String>(
                                                  value: value,
                                                  child: new Text(value),
                                                );
                                              }).toList(),
                                              onChanged: (value) {
                                                setState(() {
                                                  selectedCountry=value;
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 15,),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width:250,
                                    decoration:BoxDecoration(
                                      color: Colors.blue,
                                      border: Border.all(
                                        color: Colors.blue,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                          child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                        ),
                                        Text("Registered Clinics",style: TextStyle(color: Colors.white,fontSize: 15),)
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Column(
                                children: <Widget>[

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20,10,0,10),
                                    child: Row(
                                      children: <Widget>[
                                        Text("Register to",style: TextStyle(color: Colors.black,fontSize: 17,fontWeight: FontWeight.bold),),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 200,
                                    child: AppStateHandlerWidget(
                                      state: providerPatientProfileProvider.loadingState,
                                      child: ListView.builder(
                                          padding: EdgeInsets.only(top: 0,bottom: 0),
                                          itemCount: 3,
                                          physics: NeverScrollableScrollPhysics(),
                                          itemBuilder: (context, index) {
                                            return Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(20,0,0,0),
                                                  child: ListTile(
                                                    contentPadding: EdgeInsets.only(bottom: 5),
                                                /*    leading: Container( width: 30, height: 30, decoration: UiConstants.defaultBoxDecoration, child: Image.asset(
                                                      'assets/images/clinic.png',
                                                    )),*/
                                                    title: Text("Healthy Life Clinic East",
                                                      style: TextStyle(fontSize: 16, fontFamily: UiConstants.segeoRegularFont),
                                                    ),
                                                  ),
                                                ),
                                                Divider(height: 1,)
                                              ],
                                            );
                                          }),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width:250,
                                    decoration:BoxDecoration(
                                      color: Colors.blue,
                                      border: Border.all(
                                        color: Colors.blue,
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                          child: Icon(Icons.radio_button_checked,color: Colors.white, size: 20,),
                                        ),
                                        Text("Patient File",style: TextStyle(color: Colors.white,fontSize: 15),)
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10,),
                              Container(
                                height: 300,
                                child: ListView.builder(
                                    itemCount: 2,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      return   GestureDetector(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(12,10,12,0),
                                          child: Container(
                                            decoration:BoxDecoration(
                                              color:Colors.white,
                                              border: Border.all(
                                                color: ColorConstants.actionsColor,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(0,10,0,0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets.only(left:0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: <Widget>[

                                                            Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left:20),
                                                                  child:  Text("Patient ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                                ),
                                                              ],
                                                            ),
                                                            Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left:10),
                                                                  child:  Text("John Clark",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                                ),
                                                              ],
                                                            ),


                                                          ],
                                                        ),
                                                      ),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          Text("Added Date",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                          SizedBox(width: 5,),
                                                          Row(
                                                            children: <Widget>[

                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(8.0,0,10,0),
                                                                child: Text("28/05/2020",style: TextStyle(fontSize: 13,color: Colors.black,fontFamily: UiConstants.segeoRegularFont,)),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(0,14,2,5),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[

                                                      Column(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(left:20),
                                                            child:  Text("File ",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoLightFont),),
                                                          ),
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(left:10),
                                                            child:  Text(": Patient EPC.jpeg",style: TextStyle(fontSize: 12,color: Colors.black,fontFamily: UiConstants.segeoRegularFont),),
                                                          ),
                                                        ],
                                                      ),


                                                    ],
                                                  ),
                                                ),



                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(20,0,20,0),
                                                  child: Divider(color: ColorConstants.actionsColor, thickness: 0.5,),
                                                ),
                                                GestureDetector(
                                                  onTap: (){
                                                    showModalBottomSheet(
                                                        context: context,
                                                        isScrollControlled:
                                                        true,
                                                        elevation: 1,
                                                        isDismissible: true,
                                                        backgroundColor:
                                                        Colors
                                                            .white,
                                                        builder: (builder) {

                                                          return Container(
                                                            height: 300,
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets.only(left: 15,right: 15,top: 6,bottom: 6),
                                                                  child:           Column(
                                                                    children: <Widget>[

                                                                      Row(
                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Selected Patient EPC.Jpeg",
                                                                              style: TextStyle( color: Colors.blueGrey[700],
                                                                                  fontFamily: UiConstants.segeoLightFont, decoration: TextDecoration.underline,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          GestureDetector(
                                                                              onTap: (){Navigator.pop(context);},
                                                                              child: Icon(Icons.cancel,color: Colors.red, size: 30,))
                                                                        ],
                                                                      ),

                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),

                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Share with Patient",
                                                                              style: TextStyle( color: Colors.green,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),
                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Delete File",
                                                                              style: TextStyle( color: Colors.red,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(height: 5,),
                                                                      Padding(
                                                                        padding: const EdgeInsets.fromLTRB(8.0,0,60,0),
                                                                        child: Divider(color: ColorConstants.graylightcolor,thickness: 0.5,),
                                                                      ),
                                                                      Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(8.0,20,8,8),
                                                                            child: Text(
                                                                              "Request document to my email",
                                                                              style: TextStyle( color:ColorConstants.darkBluecolor,
                                                                                  fontFamily: UiConstants.segeoLightFont,
                                                                                  fontSize: 18, fontWeight: FontWeight.bold
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),

                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        }
                                                    );
                                                  },
                                                  child: Container(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Column(
                                                          mainAxisSize: MainAxisSize.max,
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: const EdgeInsets.only(left: 8),
                                                              child: Text("Actions ",style: TextStyle(fontSize: 15,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont),),
                                                            ),
                                                            Icon(Icons.more_horiz,color: ColorConstants.actionsColor,)
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),

                                          ),
                                        ),
                                      );
                                    }
                                ),
                              ),
                               /*Center(
                                      child: Container(

                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0,5,5,20),
                                          child:   RaisedButton(
                                            color:Colors.blue,
                                            textColor: ColorConstants.primaryColor,
                                            elevation: 0.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(30.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),

                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(30,10,30,10),
                                              child: Text(
                                                "Add Patient ",
                                                style: TextStyle( color: Colors.white,
                                                    fontFamily: UiConstants.segeoLightFont,
                                                    fontSize: 14, fontWeight: FontWeight.bold
                                                ),
                                              ),
                                            ),
                                            onPressed: () {


                                              _formKey.currentState.save();
                                              if (_formKey.currentState.validate()) {
                                                showModalBottomSheet(
                                                    context: context,
                                                    isScrollControlled:
                                                    true,
                                                    elevation: 1,
                                                    isDismissible: true,
                                                    backgroundColor:
                                                    Colors
                                                        .transparent,
                                                    builder: (builder) {
                                                      return GestureDetector(
                                                        behavior:
                                                        HitTestBehavior.opaque,
                                                        onTap: () {
                                                          FocusScope.of(
                                                              context)
                                                              .requestFocus(
                                                              new FocusNode());
                                                        },
                                                        child:
                                                        SingleChildScrollView(
                                                          child:
                                                          ConstrainedBox(
                                                            constraints: BoxConstraints(
                                                                maxHeight: MediaQuery
                                                                    .of(
                                                                    context)
                                                                    .size
                                                                    .height),
                                                            child:
                                                            Container(
                                                              height: 650,
                                                              decoration: BoxDecoration(
                                                                  color: Colors
                                                                      .white,
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      20)),
                                                              child:
                                                              Container(
                                                                height: MediaQuery.of(context).size.height,
                                                                child: Material(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: ListView(
                                                                      children: <Widget>[
                                                                        Column(
                                                                          children: <
                                                                              Widget>[
                                                                            SizedBox(height: 40,),
                                                                            Row(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  width:250,
                                                                                  decoration:BoxDecoration(
                                                                                    color: Colors.blue,
                                                                                    border: Border.all(
                                                                                      color: Colors.blue,
                                                                                      width: 2,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                                                                                  ),
                                                                                  child: Row(
                                                                                    children: <Widget>[
                                                                                      Padding(
                                                                                        padding: const EdgeInsets.fromLTRB(8.0,5,8,5),
                                                                                        child: Icon(Icons.radio_button_checked,color: Colors.white, size: 25,),
                                                                                      ),
                                                                                      Text("Account Details",style: TextStyle(color: Colors.white,fontSize: 20),)
                                                                                    ],
                                                                                  ),

                                                                                ),
                                                                              ],
                                                                            ),
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: const EdgeInsets.only(left:40.0,top: 50),
                                                                                  child: Container(
                                                                                      width:300,
                                                                                      child: Text("New Patient Details added to the system ",style: TextStyle(fontSize: 30,color: ColorConstants.darkBluecolor,fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),)),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            SizedBox(height: 50,),
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(left: 20),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                children: <Widget>[
                                                                                  Container(
                                                                                    width:60,height: 60,
                                                                                    decoration: BoxDecoration(

                                                                                      color: Colors.green,

                                                                                      borderRadius: BorderRadius.circular(30),
                                                                                    ),
                                                                                    child: Padding(
                                                                                        padding: const EdgeInsets.all(8.0),
                                                                                        child: Icon(Icons.done,color: Colors.white,)
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                            SizedBox(height: 20,),
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(left: 35,right: 15),
                                                                              child:  Divider(thickness: 1,color:ColorConstants.actionsColor,),
                                                                            ),
                                                                            SizedBox(height: 20,),
                                                                            Padding(
                                                                                padding: const EdgeInsets.only(left: 35,right: 15),
                                                                                child:  Center(
                                                                                  child: Padding(
                                                                                    padding: const EdgeInsets.all(20),
                                                                                    child: Container(
                                                                                      padding:EdgeInsets.fromLTRB(10, 5, 5, 5),
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        mainAxisSize: MainAxisSize.min,
                                                                                        children: <Widget>[
                                                                                          Text("Book New appointment",style: TextStyle(fontSize: 15,color: ColorConstants.actionsColor,fontFamily: UiConstants.segeoRegularFont),),
                                                                                          SizedBox(width: 10,),
                                                                                          Container(
                                                                                            padding:EdgeInsets.fromLTRB(5,5,5,5),
                                                                                            decoration: BoxDecoration(
                                                                                              color: Colors.blue,
                                                                                              border: Border.all(
                                                                                                color: Colors.blue,
                                                                                                width: 0,
                                                                                              ),
                                                                                              borderRadius: BorderRadius.circular(25),
                                                                                            ),
                                                                                            child: Image.asset(
                                                                                              'assets/images/calenderw.png',width: 16, height: 18,),


                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                      decoration: BoxDecoration(

                                                                                        color: Colors.transparent,
                                                                                        border: Border.all(color: Colors.blueAccent, width: 1,),
                                                                                        borderRadius: BorderRadius.circular(25),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                            ),
                                                                            SizedBox(height: 0,),
                                                                            Padding(
                                                                                padding: const EdgeInsets.only(left: 35,right: 15),
                                                                                child:  Center(
                                                                                  child: Padding(
                                                                                    padding: const EdgeInsets.all(20),
                                                                                    child: Center(
                                                                                      child: Container(

                                                                                        child: Padding(
                                                                                          padding: const EdgeInsets.fromLTRB(0,5,5,20),
                                                                                          child:   RaisedButton(
                                                                                            color:Colors.blue,
                                                                                            textColor: ColorConstants.primaryColor,
                                                                                            elevation: 0.0,
                                                                                            shape: RoundedRectangleBorder(
                                                                                                borderRadius: new BorderRadius.circular(30.0),
                                                                                                side: BorderSide(
                                                                                                    color: Colors.blue)),

                                                                                            child: Padding(
                                                                                              padding: const EdgeInsets.fromLTRB(30,10,30,10),
                                                                                              child: Text(
                                                                                                "Main Menu ",
                                                                                                style: TextStyle( color: Colors.white,
                                                                                                    fontFamily: UiConstants.segeoLightFont,
                                                                                                    fontSize: 14, fontWeight: FontWeight.bold
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                            onPressed: () {
                                                                                              Navigator.pop(context);
                                                                                              Navigator.pushNamed(context, RoutesConstants.providerMainMenuPage);

                                                                                            },
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ],

                                                                    )),
                                                                margin: EdgeInsets
                                                                    .only(
                                                                    bottom:
                                                                    0,
                                                                    left:
                                                                    0,
                                                                    right:
                                                                    0),
                                                                decoration:
                                                                BoxDecoration(
                                                                  color: Colors
                                                                      .white,
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      20),
                                                                ),
                                                              ),
                                                              padding:
                                                              EdgeInsets
                                                                  .fromLTRB(0,20,20,20),
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    });
                                              }

                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                              SizedBox(height: 20,),*/

                            ],
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){


                      //  providerPatientProfileProvider.addPatient();
                        // patientBookingProvider.showDialog();
                      },
                      child: Container(
                        color: ColorConstants.lightgray,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,0,0,0),
                              child: Divider(height: 5,thickness: 1,color:ColorConstants.actionsColor,),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  DefaultIconButton(
                                    labelText: 'Bookings',
                                    iconWidget: Image.asset(
                                      'assets/images/calender.png',width: 20, height: 23,),
                                    onPressed: () {
                                      //Navigator.pushNamed(context, RoutesConstants.callLogPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Invoices',
                                    iconWidget: Image.asset(
                                      'assets/images/invoices2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Contact',
                                    iconWidget: Image.asset(
                                      'assets/images/comment.png',width: 20, height: 23,),
                                    onPressed: () async {
                                      //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                      //onNavigate(true);

                                    },
                                  ),
                                  DefaultIconButton(
                                    labelText: 'Profile',
                                    iconWidget: Image.asset(
                                      'assets/images/user2.png',width: 20, height: 23,),
                                    onPressed: () {
                                      // Navigator.pushNamed(context, RoutesConstants.transactionPage);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
    }
}
