import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/forms/appointmentBookingDetailsForm.dart';
import 'package:mediclinic/forms/appointmentClinicForm.dart';
import 'package:mediclinic/forms/appointmentConfirmForm.dart';
import 'package:mediclinic/forms/appointmentDateForm.dart';
import 'package:mediclinic/forms/appointmentProviderForm.dart';
import 'package:mediclinic/forms/appointmentServiceForm.dart';
import 'package:mediclinic/forms/appointmentTimeForm.dart';
import 'package:mediclinic/providers/bookNewAppointmentProvider.dart';
import 'package:mediclinic/providers/patientBookingProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/customTimeline.dart';
import 'package:mediclinic/widgets/statefull/defaultIconButton.dart';
import 'package:mediclinic/widgets/timeline.dart';
import 'package:provider/provider.dart';

class BookNewAppointmentPage extends StatefulWidget {
  @override
  BookNewAppointmentPageState createState() {
    return BookNewAppointmentPageState();
  }
}
class BookNewAppointmentPageState extends  State<BookNewAppointmentPage> {
  BookNewAppointmentProvider  bookNewAppointmentProvider   ;
  List<Padding>  indicators=new List<Padding>();
  List<Widget>  tiles=new List<Widget>();

  void initState() {
      tiles = new List<Widget>();
      tiles.add(new Row(
        children: <Widget>[
           Text("Patient",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont,),),
           SizedBox(width: 15,),
           Text(":"+(bookNewAppointmentProvider!=null&&bookNewAppointmentProvider.settingService!=null?bookNewAppointmentProvider.settingService.fullName():""),style: TextStyle(fontSize: 14,color: Colors.black54, fontFamily: UiConstants.segeoRegularFont,fontWeight: FontWeight.bold),),
        ],

      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Service",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont),),
          SizedBox(width: 14,),
          Text(bookNewAppointmentProvider==null||bookNewAppointmentProvider.service.isEmpty?"":": "+bookNewAppointmentProvider.service,style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Clinic",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont),),
          SizedBox(width: 27,),
          Text(bookNewAppointmentProvider==null||bookNewAppointmentProvider.clinic.isEmpty?"":": "+bookNewAppointmentProvider.clinic,style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Provider",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont,),),
          SizedBox(width: 5,),
          Text(bookNewAppointmentProvider==null||bookNewAppointmentProvider.provider.isEmpty?"":": "+bookNewAppointmentProvider.provider,style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));
      tiles.add(new Row(
        children: <Widget>[
          Text("Time",style: TextStyle(fontSize: 16,fontFamily: UiConstants.segeoRegularFont),),
          SizedBox(width: 28,),
          Text(bookNewAppointmentProvider==null||bookNewAppointmentProvider.formatDate2.isEmpty?"":": "+bookNewAppointmentProvider.formatDate2+" - "+bookNewAppointmentProvider.time, style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,color: Colors.black54,fontFamily: UiConstants.segeoRegularFont),),
        ],
      ));

      indicators = new List<Padding>();
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Icon(Icons.done,size:20,color:Colors.blueAccent),

        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Icon(Icons.done,size:20,color: bookNewAppointmentProvider!=null && bookNewAppointmentProvider.service.isNotEmpty?Colors.blueAccent:Colors.transparent,),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Icon(Icons.done,size: 20,color:  bookNewAppointmentProvider!=null && bookNewAppointmentProvider.clinic.isNotEmpty?Colors.blueAccent:Colors.transparent,),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Icon(Icons.done,size: 20,color:  bookNewAppointmentProvider!=null && bookNewAppointmentProvider.provider.isNotEmpty?Colors.blueAccent:Colors.transparent,),
        ));
        indicators.add(Padding(
          padding: const EdgeInsets.all(0.0),
          child: Icon(Icons.done,size: 20,color:  bookNewAppointmentProvider!=null && bookNewAppointmentProvider.time.isNotEmpty?Colors.blueAccent:Colors.transparent,),
        ));
  }

  @override
  Widget build(BuildContext context) {
    bookNewAppointmentProvider = Provider.of<BookNewAppointmentProvider>(context, listen: true);
    initState();
    return Container(
      color: ColorConstants.graylightcolor,
      child:  AppStateHandlerWidget(
        state: bookNewAppointmentProvider.loadingState,
        child: Scaffold(
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child:Column(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height-100,
                        child: ListView(
                          children: <Widget>[
                            Container(
                              decoration:BoxDecoration(
                               // color: ColorConstants.graylightcolor.withOpacity(0.1),
                                color: Colors.white
                              ),
                              child: Column(
                                children: <Widget>[
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: <Widget>[
                                      Container(
                                        padding:EdgeInsets.fromLTRB(12,20,10,10),
                                        decoration:BoxDecoration(
                                          color: ColorConstants.mainContainersColor,
                                          border: Border.all(
                                            color: Colors.blue,
                                            width: 2,
                                          ),
                                          borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(30, 40)),
                                        ),
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0.0,0,8,8),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  GestureDetector(
                                                    child: Container(

                                                      child: IconButton(
                                                        icon:Icon(Icons.arrow_back_ios,color: Colors.white,size: 25,),
                                                        color: ColorConstants.primaryColor,
                                                      ),
                                                      decoration: BoxDecoration(
                                                        color: Colors.blueAccent,
                                                        borderRadius: BorderRadius.circular(25),
                                                      ),
                                                    ),
                                                    onTap: (){
                                                      Navigator.pop(context, RoutesConstants.patientMainMenuPage);
                                                    },
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(18,0,0,0),
                                                    child: Text("Appointment Summery",style: TextStyle(fontSize: 24, color:ColorConstants.darkBluecolor,fontWeight: FontWeight.bold, fontFamily: UiConstants.segeoRegularFont),),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            CustomTimeline(
                                              padding:EdgeInsets.fromLTRB(60, 0, 0, 0),
                                              children:tiles,
                                              indicators: indicators,
                                            ),
                                            SizedBox(height: 10,),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  getCurrentView(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),


                      GestureDetector(
                        onTap: (){
                         // createPatientProfileProvider.addPatient();
                          // patientBookingProvider.showDialog();
                        },
                        child: Container(
                          color: ColorConstants.lightgray,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0,0,0,0),
                                child: Divider(height: 5,thickness: 1,color:ColorConstants.actionsColor,),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    DefaultIconButton(
                                      labelText: 'Bookings',
                                      iconWidget: Image.asset(
                                        'assets/images/calender.png',width: 20, height: 23,),
                                      onPressed: () {
                                        Navigator.pushNamed(context, RoutesConstants.patientBookingPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Invoices',
                                      iconWidget: Image.asset(
                                        'assets/images/invoices2.png',width: 20, height: 23,),
                                      onPressed: () {
                                        // Navigator.pushNamed(context, RoutesConstants.contactsPage);
                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Contact',
                                      iconWidget: Image.asset(
                                        'assets/images/comment.png',width: 20, height: 23,),
                                      onPressed: () async {
                                        //await Navigator.pushNamed(context, RoutesConstants.minutePricingPage);
                                        //onNavigate(true);

                                      },
                                    ),
                                    DefaultIconButton(
                                      labelText: 'Profile',
                                      iconWidget: Image.asset(
                                        'assets/images/user2.png',width: 20, height: 23,),
                                      onPressed: () {
                                        // Navigator.pushNamed(context, RoutesConstants.transactionPage);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }


Widget getCurrentView(){



  if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.confirmed) {
    return AppointmentBookingDetailsForm(bookNewAppointmentProvider,onMainMenuSelected: (navigate){
      if(navigate)
      {

        Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
      }
    },);
  }

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.time.isNotEmpty) {
      return AppointmentConfirmForm(onConfirmSelected: (done){
        if(done)
          {
            bookNewAppointmentProvider.selectConfirmed(done);
          }
      },);
    }

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.date.isNotEmpty)
      return AppointmentTimeForm(bookNewAppointmentProvider,onTimeSelected: (time){
        bookNewAppointmentProvider.selectTime(time);
      },);

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.provider.isNotEmpty)
      return AppointmentDateForm(onDateSelected: (date,formatedDate,formatedDate2){
        bookNewAppointmentProvider.selectDate(date,formatedDate,formatedDate2);
      },);

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.clinic.isNotEmpty)
      return AppointmentProviderForm(onProviderSelected: (provider){
        bookNewAppointmentProvider.selectProvider(provider);
      },);

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.subService.isNotEmpty)
      return AppointmentClinicsForm(onClinicSelected: (clinic){
        bookNewAppointmentProvider.selectClinic(clinic);
      },);

    if(bookNewAppointmentProvider!=null && bookNewAppointmentProvider.patient.isNotEmpty)
      return AppointmentServiceForm(bookNewAppointmentProvider,onServiceSelected: (service){
        bookNewAppointmentProvider.selectService(service);
      },onSubServiceSelected: (subService){
        bookNewAppointmentProvider.selectSubService(subService);
  },);

  return AppointmentServiceForm(bookNewAppointmentProvider,onServiceSelected: (service){
    bookNewAppointmentProvider.selectService(service);
  },onSubServiceSelected: (subService){
    bookNewAppointmentProvider.selectSubService(subService);
  },);
}

}
