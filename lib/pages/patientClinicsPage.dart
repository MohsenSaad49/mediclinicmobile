import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/loginProvider.dart';
import 'package:mediclinic/providers/patientClinicsProvider.dart';
import 'package:mediclinic/widgets/appStates/appStateHandlerWidget.dart';
import 'package:mediclinic/widgets/statefull/AppBarWidget.dart';
import 'package:provider/provider.dart';

class PatientClinicsPage extends StatefulWidget {
  @override
  PatientClinicsPageState createState() {
    return PatientClinicsPageState();
  }
}
class PatientClinicsPageState extends  State<PatientClinicsPage> {
  PatientClinicsProvider patientClinicsProvider;
  @override
  Widget build(BuildContext context) {
    patientClinicsProvider = Provider.of<PatientClinicsProvider>(context, listen: true);
    return new Scaffold(
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height,
              ),
              child:ListView(
                children: <Widget>[
                  Container(
                    decoration:BoxDecoration(
                      color: Colors.white30,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding:EdgeInsets.fromLTRB(10,20,10,10),
                          decoration:BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(
                              color: Colors.blue,

                              width: 2,
                            ),
                            borderRadius: BorderRadius.only(bottomLeft: new Radius.elliptical(50, 80)),
                          ),
                          child: Column(
                            children: <Widget>[
                              /*       Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 30,),
                                ),
                              ],
                            ),*/
                              SizedBox(height: 30,),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(40,0,10,10),
                                    child: Text("Mediclinic",style: TextStyle(fontSize: 30,color: Colors.blueGrey, fontWeight: FontWeight.bold),),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(40,0,90,10),
                                child: Divider(height: 5,thickness: 2,color: Colors.blue,),
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width:300,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(40,0,10,10),
                                      child: Text("We will need to search our database for your clinic. Please fill in information below so we may assist you ",
                                        overflow: TextOverflow.clip,
                                        style: TextStyle(fontSize: 12,color: Colors.black45),),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 25,),
                        Row(
                          children: <Widget>[
                            Container(
                              width:300,
                              decoration:BoxDecoration(
                                color: Colors.blue,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.only(bottomRight: new Radius.elliptical(50, 80)),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(Icons.radio_button_checked,color: Colors.white,),
                                  ),
                                  Text("Account Details",style: TextStyle(color: Colors.white,fontSize: 20),)
                                ],
                              ),

                            ),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text("We found your details in the followings clinics",style: TextStyle(color: Colors.blueAccent,fontFamily: UiConstants.segeoLightFont,fontSize: 18),),
                            ),
                          ],
                        ),

                        Container(
                          height: 300,
                          child: AppStateHandlerWidget(
                            state: patientClinicsProvider.loadingState,
                            child: ListView.builder(
                                padding: EdgeInsets.only(top: 10,bottom: 10),
                                itemCount: patientClinicsProvider?.allClinics?.length?? 0,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: ListTile(
                                          contentPadding: EdgeInsets.only(bottom: 20),
                                          leading: SvgPicture.asset(
                                            "assets/iconfinder-icon.svg", width: 50, height: 50,
                                          ),
                                          title: Text(patientClinicsProvider?.allClinics[index].name,
                                            style: TextStyle(fontSize: 16, fontFamily: UiConstants.segeoRegularFont),
                                          ),
                                        ),
                                      ),
                                      Divider(height: 1,)
                                    ],
                                  );
                                }),
                          ),
                        ),

                        Center(
                          child: Container(
                            width: 200,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0,20,5,20),
                              child:   RaisedButton(

                                color:Colors.blue,
                                textColor: ColorConstants.primaryColor,
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(30.0),
                                    side: BorderSide(
                                        color: Colors.blue)),

                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "GO TO MAIN MENU",
                                      style: TextStyle( color: Colors.white,
                                          fontFamily: UiConstants.segeoLightFont,
                                          fontSize: 18, fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ],
                                ),
                                onPressed: () {
                                  patientClinicsProvider.navService.navigateTo(RoutesConstants.patientMainMenuPage);
                                //  Navigator.pushNamed(context, RoutesConstants.patientMainMenuPage);
                                },
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ],

              ),
            ),
          ),
        ));
  }
  Widget _getList() {
    return Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
                child:ListView.builder(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    itemCount: patientClinicsProvider?.allClinics?.length?? 0,
                    itemBuilder: (context, index) {
                      return Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                              contentPadding: EdgeInsets.only(bottom: 20),
                              leading: Container( width: 50, height: 50, decoration: UiConstants.defaultBoxDecoration, child: Image.asset(
                                'assets/images/clinic.png',
                              )),
                              title: Text(patientClinicsProvider?.allClinics[index].name,
                                style: TextStyle(fontSize: 16, fontFamily: UiConstants.segeoRegularFont),
                              ),
                            ),
                          ),
                          Divider(height: 1,)
                        ],
                      );
                    }))
          ],
        ));
  }

  Widget buildBody(context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
          padding: EdgeInsets.only(top: 130),
          decoration: UiConstants.heroBoxDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.fromLTRB(20,0,0,30),
                child: Text("We found your details in the followings clinics",style: TextStyle(color: Colors.white,fontFamily: UiConstants.segeoLightFont,fontSize: 20),),
              ),
              Expanded(
                  child: Container(
                    decoration: UiConstants.mainBoxDecoration,
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                          ],
                        ),
                        Expanded(
                            child: AppStateHandlerWidget(
                                state: patientClinicsProvider.loadingState,
                                child: _getList()))
                      ],
                    ),
                  )),
            ],
          )),
    );
  }

}
