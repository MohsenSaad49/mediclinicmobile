
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/patientUpcomingBooking.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';

import '../appEnums.dart';

class RegisterPatientWithoutDateProvider extends AbstractProvider {

  final patientService = locator<PatientService>();

  String patientId;
  String deviceId;
  String xDbName;
  Booking patientUpcomingBooking;

  RegisterPatientWithoutDateProvider() {

  }


}
