
import 'package:map_launcher/map_launcher.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/patientContacts.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';
import 'package:url_launcher/url_launcher.dart';


class PatientContactProvider extends AbstractProvider {

  final patientService = locator<PatientService>();
  List<Clinic> allClinics;
  List<PatientClinic> allContacts;
  String patientId;
  String deviceId;
  PatientContactProvider() {
    getPatientClinics();
  }

  getPatientClinics() async{
    loadingState=LoadingState.waiting;
    var contacts= await patientService.getPatientContacts();
    if(contacts.isSuccess)
      allContacts=contacts.data;
    loadingState=LoadingState.done;
    notifyListeners();
  }


  getDirection(double lat,double long,String clinicName) async {
    final availableMaps = await MapLauncher.installedMaps;
    await availableMaps.first.showMarker(
      coords: Coords(lat, long),
      title: clinicName
    );
  }


  callClinic(String phone) async {
    var url = "tel:"+phone;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not call  $url';
    }
  }

  sendSms(String phone) async {
    var url = "sms:"+phone;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not send sms to  $url';
    }
  }

  sendEmail(String email) async {
    var url = "mailto:"+email;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not send email to  $url';
    }
  }


}
