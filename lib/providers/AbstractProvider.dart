import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/appServices/dialogService.dart';
import 'package:mediclinic/services/appServices/loadingService.dart';
import 'package:mediclinic/services/appServices/navigationService.dart';
import 'package:mediclinic/services/appServices/settingsService.dart';


class AbstractProvider extends ChangeNotifier {
  final navService = locator<NavigationService>();
  final loadingService = locator<LoadingService>();
  final settingService = locator<SettingsService>();
  final DialogService dialogService = locator<DialogService>();
  LoadingState loadingState;
}
