
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/patientUpcomingBooking.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';

import '../appEnums.dart';

class PatientMainMenuProvider extends AbstractProvider {

  final patientService = locator<PatientService>();

  String patientId;
  String deviceId;
  String xDbName="";
  Booking patientUpcomingBooking;

  PatientMainMenuProvider() {
    var params = navService.params as Map;
    if (params.containsKey("DBNAME")) {
      if (params['DBNAME'] is String) {
        xDbName=params['DBNAME'];
      }
    }
    settingService.setDBName(xDbName);

    getUpcomingBooking();
  }

  getUpcomingBooking() async{
    loadingState=LoadingState.waiting;
    var booking= await patientService.getPatientUpcomingBooking();
    if(booking.isSuccess)
      patientUpcomingBooking=booking.data;
    loadingState=LoadingState.done;
    notifyListeners();
  }


  showDialog(){
    dialogService.showSuccess("Coming Soon ;)");
  }

/*



  intialize()async{
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    patientId=  prefs.get(ApiConstants.patientID);
    deviceId= prefs.get(ApiConstants.patientDeviceID);
    getPatientClinic();
  }

  Future<void> getPatientClinic() async {
    try {
      loadingState = LoadingState.waiting;

      var clinics = await patientService.getPatientClinics(patientId, "123456789");
      allClinics=clinics.clinics;
      allClinics!=null&& allClinics.length>0?  loadingState = LoadingState.done:  loadingState = LoadingState.empty;
      notifyListeners();
    } catch (ex) {
      loadingState = LoadingState.error;
      dialogService.showError(ex.toString());
    }
  }
*/

}
