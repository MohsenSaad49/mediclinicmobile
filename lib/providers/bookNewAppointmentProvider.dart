
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';

class BookNewAppointmentProvider extends AbstractProvider {
  final patientService = locator<PatientService>();
  List<Clinic> allClinics;
  String patient="";
  String service="";
  String subService="";
  String clinic="";
  String provider="";
  String date="";
  String formatDate="";
  String formatDate2="";
  bool expandSubService=false;
  String time="";
  bool confirmed=false;
  String deviceId;
  bookNewAppointmentProvider() {
    patient="john smith";
    notifyListeners();
  }

  showDialog(){
    dialogService.showSuccess("Coming Soon ;)");
  }

  selectService(String selectedService ){
    service=selectedService;
    expandSubService=true;
    notifyListeners();
  }
  selectSubService(String selectedService ){
    subService=selectedService;
    notifyListeners();
  }

  selectClinic(String selectedClinic ){
    clinic=selectedClinic;
    notifyListeners();
  }

  selectProvider(String selectedProvider){
     provider=selectedProvider;
     notifyListeners();
  }

  selectDate(String selectedDate,formatedDate,formatedDate2){
       date=selectedDate;
       formatDate=formatedDate;
       formatDate2=formatedDate2;
       notifyListeners();
  }

  selectTime(String selectedTime){
     time=selectedTime;
     notifyListeners();
  }


  selectConfirmed(bool isConfirmed){
    confirmed=isConfirmed;
    notifyListeners();
  }

}
