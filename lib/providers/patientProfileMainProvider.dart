import 'dart:io';
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientProfileMainProvider extends AbstractProvider {

  final patientService = locator<PatientService>();
  List<Clinic> allClinics;
  String patientId;
  String deviceId;
  bool fromUnRegisteredUser=false;
  PatientProfileMainProvider() {
    var params = navService.params as Map;
    if (params!=null&& params.containsKey("fromUnRegisteredUser")) {
      if (params['fromUnRegisteredUser'] is bool) {
        fromUnRegisteredUser=params['fromUnRegisteredUser'];
      }
    }
  }
  showDialog(){
    dialogService.showSuccess("Coming Soon ;)");
  }

}
