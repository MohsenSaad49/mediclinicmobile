import 'dart:io';
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientClinicsProvider extends AbstractProvider {

  final patientService = locator<PatientService>();
  List<Clinic> allClinics;
  String patientId;
  String deviceId;
  PatientClinicsProvider() {
    loadingState = LoadingState.waiting;

    intialize();
  }

  intialize()async{
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    patientId=  prefs.get(ApiConstants.patientToken);
    deviceId= prefs.get(ApiConstants.patientDeviceID);
    getPatientClinic();
  }

  Future<void> getPatientClinic() async {
    try {
      loadingState = LoadingState.waiting;

      var clinics = await patientService.getPatientClinics(patientId, "123456789");
      allClinics=clinics.clinics;
      allClinics!=null&& allClinics.length>0?  loadingState = LoadingState.done:  loadingState = LoadingState.empty;
      notifyListeners();
    } catch (ex) {
      loadingState = LoadingState.error;
      dialogService.showError(ex.toString());
    }
  }

}
