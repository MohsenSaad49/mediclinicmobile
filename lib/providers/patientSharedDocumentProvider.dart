
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PatientSharedDocumentProvider extends AbstractProvider {

  final authService = locator<AuthService>();
  String username;
  String surname;
  String dateOfBirth;
  String mobileNumber;
  String email;
  bool patientAdded=false;


  Future<void> addPatient() async {
    patientAdded=true;
    notifyListeners();
  }

}
