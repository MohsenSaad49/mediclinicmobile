
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ProviderPatientProfileProvider extends AbstractProvider {

  final authService = locator<AuthService>();
  String username;
  String surname;
  String dateOfBirth;
  String mobileNumber;
  String email;
  bool patientAdded=false;


  Future<void> addPatient() async {
    patientAdded=true;
    notifyListeners();
  }

/*  Future<void> patientLogin() async {
    try {
       var request= new PatientLoginRequest();
       request.email=email;
       request.firstname=username;
       request.surname=surname;
       request.device_id="123456789";
       request.mobile_phone=mobileNumber;
       request.dob=dateOfBirth;

       if(request.firstname==null||request.firstname.isEmpty) {
         dialogService.showError("Please enter first name");
         return;
       }
       if(request.surname==null||request.surname.isEmpty) {
         dialogService.showError("Please enter surname");
         return;
       }

     *//*  if(request.mobile_phone==null||request.mobile_phone.isEmpty) {
         dialogService.showError("Please enter  Mobile number");
         return;
       }*//*

       if(request.dob==null||request.dob == "Date of Birth") {
         dialogService.showError("Please select Date Of Birth");
         return;
       }
       if(request.email==null||request.email.isEmpty) {
         dialogService.showError("Please enter your email");
         return;
       }

      loadingService.showLoading("");
      var auth = await authService.patientLogin(request);
      if(auth.patientSites!=null && auth.patientSites.length>0) {
        await savePatientCredentails(auth.patientSites.first.iD, request.device_id);
        loadingService.hideLoading();
        navService.navigateTo(RoutesConstants.patientClinicsPage);
      }else{
        loadingService.hideLoading();
        dialogService.showError("Incorrect Account details!");
      }

    } catch (ex) {
      loadingService.hideLoading();
      dialogService.showError(ex.toString());
    }
  }

  Future<bool> savePatientCredentails(String patientId,String deviceId) async {
    try {
      final prefs = await SharedPreferences.getInstance();


      if(prefs.get(ApiConstants.patientID)==null) {
        prefs.setString(ApiConstants.patientID, patientId);
        prefs.setString(ApiConstants.patientDeviceID, deviceId);
      }
    }catch(ex)
    {

    }

  }*/
}
