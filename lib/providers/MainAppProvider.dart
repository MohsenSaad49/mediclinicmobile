import 'dart:async';
import 'dart:collection';
import 'package:async/async.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';


class MainAppProvider extends AbstractProvider {

  MainAppProvider() {


    navService.navServiceStream.stream.listen((onData) {
      notifyListeners();
    });
  }



  Future<void> logout() async {
    try {
     /* loadingService.showLoading("");
      await authService
          .userLogout()
          .whenComplete(() => loadingService.hideLoading());
      settingsService.clearState();*/
    } catch (ex) {
     // dialogService.showError(ex.toString());
    } finally {
     // navService.navigateTo(RoutesConstants.loginPage);
    }
  }



  void didResume() {
   // webSocketService.reconnect();
  }

}
