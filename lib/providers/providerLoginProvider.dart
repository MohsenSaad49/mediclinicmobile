import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ProviderLoginProvider extends AbstractProvider {

  final authService = locator<AuthService>();
  String username;
  String surname;
  String dateOfBirth;
  String mobileNumber;
  String email;

  Future<void> patientLogin() async {
    try {
       var request= new PatientLoginRequest();
       request.email=email;
       request.firstname=username;
       request.surname=surname;
       request.device_id="1234567892";
       request.mobile_phone=mobileNumber;
       request.dob=dateOfBirth;
       if(request.dob==null||request.dob == "Date of Birth") {
         dialogService.showError("Please select Date Of Birth");
         return;
       }

      loadingService.showLoading("");
      var auth = await authService.patientLogin(request);
       await savePatientCredentails(auth.data.accessToken,request.device_id);
      loadingService.hideLoading();
      navService.navigateTo(RoutesConstants.patientClinicsPage);
    } catch (ex) {
      loadingService.hideLoading();
      dialogService.showError(ex.toString());
    }
  }

  Future<bool> savePatientCredentails(String patientId,String deviceId) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString(ApiConstants.patientToken, patientId);
      prefs.setString(ApiConstants.patientDeviceID, deviceId);
    }catch(ex)
    {
    }
  }
}
