
import 'package:mediclinic/models/patientAllUpcomingAppointment.dart';
import 'package:mediclinic/models/patientClinicsModel.dart';
import 'package:mediclinic/models/patientLastBooking.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/patientService.dart';
import 'package:url_launcher/url_launcher.dart';

import '../appEnums.dart';

class PatientBookingProvider extends AbstractProvider {

  final patientService = locator<PatientService>();
  List<Clinic> allClinics;
  String patientId;
  String deviceId;
  Booking patientLastBooking;
  List<UpcomingBooking> patientAllBooking;
  List<String> clinicFilter;
  List<String> providerFilter;
  LoadingState titleLoadingstate;

  String selectedClinic="";
  String selectedProvider="";

  bool futureSelected=true;
  bool previousSelected=false;

  PatientBookingProvider()  {
    getLastBooking();
  }


  removeFilter()async{
   if(futureSelected)
     {
       var booking= await patientService.getPatientAllUpcommingBooking();
       if(booking.isSuccess)
         patientAllBooking=booking.data;
     }else{
     var booking= await patientService.getPatientAllPreviousBooking();
     if(booking.isSuccess)
       patientAllBooking=booking.data;
   }

    notifyListeners();
  }

  applyFilter ()async
  {
    if(selectedClinic.isNotEmpty) {
       patientAllBooking = patientAllBooking =
          patientAllBooking.where((x) => x.clinicName.contains(selectedClinic))
              .toList();

    }

    if(selectedProvider.isNotEmpty) {
      if(selectedClinic.isEmpty){
        patientAllBooking = patientAllBooking.where((x) =>
            x.providerFullName().contains(selectedProvider)).toList();
      }else{
        var result2 = patientAllBooking.where((x) =>
            x.providerFullName().contains(selectedProvider)).toList();
        patientAllBooking.addAll(result2);
      }
    }
    selectedProvider="";
    selectedClinic="";
    notifyListeners();
  }

  selectFuture() async{
    try {
      futureSelected = true;
      previousSelected = false;
      loadingService.showLoading("");
      await getAllUpcomingBooking();
      loadingService.hideLoading();
      notifyListeners();
    }catch(ex){
      loadingService.hideLoading();
    }
  }


  launchURL() async {
    const url = 'tel:+1 555 010 999';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  selectPrevious() async{
    futureSelected=false;
    previousSelected=true;

    loadingService.showLoading("");
    await getAllPreviousBooking();
    loadingService.hideLoading();
    notifyListeners();
  }

  getLastBooking() async{
    loadingState=LoadingState.waiting;
    var booking= await patientService.getPatientLastBooking();
    if(booking.isSuccess)
      patientLastBooking=booking.data;
    loadingState=LoadingState.done;
    notifyListeners();
    selectFuture();
  }

  getAllUpcomingBooking() async{
    loadingState=LoadingState.waiting;
    var booking= await patientService.getPatientAllUpcommingBooking();
    if(booking.isSuccess)
      patientAllBooking=booking.data;
      clinicFilter=new List();
    providerFilter=new List();
    for(var i in patientAllBooking)
      {
        if(!providerFilter.contains(i.providerFullName())) {
          providerFilter.add(i.providerFullName());
        }

        if(!clinicFilter.contains(i.clinicName)) {
          clinicFilter.add(i.clinicName);
        }
      }
    loadingState=LoadingState.done;
    notifyListeners();
  }



  getAllPreviousBooking() async{
    loadingState=LoadingState.waiting;
    var booking= await patientService.getPatientAllPreviousBooking();
    if(booking.isSuccess)
      patientAllBooking=booking.data;

    providerFilter=new List();
    clinicFilter=new List();
    for(var i in patientAllBooking)
    {
      if(!providerFilter.contains(i.providerFullName())) {
        providerFilter.add(i.providerFullName());
      }
      if(!clinicFilter.contains(i.clinicName)) {
        clinicFilter.add(i.clinicName);
      }
    }

    loadingState=LoadingState.done;
    notifyListeners();
  }



}
