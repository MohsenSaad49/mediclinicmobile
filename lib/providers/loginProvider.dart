
import 'package:mediclinic/ApiConstants.dart';
import 'package:mediclinic/RoutesConstants.dart';
import 'package:mediclinic/models/requestModel/patientLoginRequest.dart';
import 'package:mediclinic/providers/AbstractProvider.dart';
import 'package:mediclinic/serviceLocator.dart';
import 'package:mediclinic/services/apiServices/authService.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginProvider extends AbstractProvider {

  final authService = locator<AuthService>();
  String username;
  String surname;
  String dateOfBirth;
  String mobileNumber;
  String homeNumber;
  String email;

  Future<void> patientLogin() async {
    try {
       var request= new PatientLoginRequest();
       request.email=email;
       request.firstname=username;
       request.surname=surname;
       request.device_id="123456789";
       request.mobile_phone=mobileNumber;
       request.dob=dateOfBirth;
       request.home_phone=homeNumber;

       if(request.firstname==null||request.firstname.isEmpty) {
         dialogService.showError("Please enter first name");
         return;
       }
       if(request.surname==null||request.surname.isEmpty) {
         dialogService.showError("Please enter surname");
         return;
       }

     /*  if(request.mobile_phone==null||request.mobile_phone.isEmpty) {
         dialogService.showError("Please enter  Mobile number");
         return;
       }*/

       if(request.dob==null||request.dob == "Date of Birth") {
         dialogService.showError("Please select Date Of Birth");
         return;
       }
       if(request.email==null||request.email.isEmpty) {
         dialogService.showError("Please enter your email");
         return;
       }

      loadingService.showLoading("");
      var auth = await authService.patientLogin(request);
      if( auth.isSuccess&& auth.data!=null && auth.data.accessToken.length>0) {
        await savePatientCredentails(auth.data.accessToken, request.device_id);
        loadingService.hideLoading();
        settingService.setUser(username, surname);
        settingService.setPatientToken(auth.data.accessToken);
        navService.navigateTo(RoutesConstants.siteSelectionPage, argument: {"fromProvider":false});
      }else{
        loadingService.hideLoading();
        dialogService.showError("Incorrect Account details!");
        navService.navigateTo(RoutesConstants.appRegConfirmPage);
      }

    } catch (ex) {
      loadingService.hideLoading();
      dialogService.showError(ex.toString());
    }
  }

  Future<bool> savePatientCredentails(String patientId,String deviceId) async {
    try {
      final prefs = await SharedPreferences.getInstance();

      prefs.setString(ApiConstants.patientToken, patientId);
      prefs.setString(ApiConstants.patientDeviceID, deviceId);
    }catch(ex)
    {

    }

  }
}
