class RoutesConstants {
  static final providerLoginPage = "/providerLoginPage";
  static final patientLoginPage = "/patientLoginPage";
  static final welcomePage = "/welcomePage";
  static final patientClinicsPage = "/patientClinicsPage";
  static final createAccountPage = "/create-account";
  static final patientMainMenuPage = "/patientMainMenuPage";
  static final providerMainMenuPage = "/providerMainMenuPage";
  static final patientBookingPage = "/patientBookingPage";
  static final createPatientProfilePage = "/createPatientProfilePage";
  static final patientProfileMainPage = "/PatientProfileMainPage";
  static final bookNewAppointmentPage = "/BookNewAppointmentPage";
  static final siteSelectionPage = "/siteSelectionPage";
  static final patientContactPage = "/patientContactPage";
  static final providerContactPage = "/providerContactPage";
  static final providerPatientProfilePage = "/providerPatientProfilePage";
  static final termsConditionsPage = "/termsConditionsPage";
  static final providerProfileMainPage = "/providerProfileMainPage";
  static final providerInvoicePage = "/providerInvoicePage";
  static final patientInvoicePage = "/patientInvoicePage";
  static final registerNewPatientPage = "/registerNewPatientPage";
  static final appRegConfirmPage = "/appRegConfirmPage";
  static final registerPatientWithoutDataPage = "/registerPatientWithoutDataPage";
  static final patientSharedDocumentPage = "/patientSharedDocumentPage";
}
