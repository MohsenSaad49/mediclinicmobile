import 'package:flutter/material.dart';
import 'package:mediclinic/shimmers/shimmerListLayout.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatelessWidget {
  final Widget shimmerLayout;
  ShimmerWidget({this.shimmerLayout});
  @override
  Widget build(BuildContext context) {
    int offset = 0;
    int time = 1000;

    return Container(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
      child: ListView.builder(
        padding: EdgeInsets.all(0),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          offset += 5;
          time = 800 + offset;
          return Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Shimmer.fromColors(
                highlightColor: Colors.white,
                baseColor: Colors.grey[350],
                child:
                    shimmerLayout == null ? ShimmerListLayout() : shimmerLayout,
                period: Duration(milliseconds: time),
              ));
        },
      ),
    );
  }
}
