import 'package:flutter/material.dart';

class ShimmerDashboardLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double containerWidth = MediaQuery.of(context).size.width - 100;
    double containerHeight = 15;

    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.symmetric(vertical: 7.5),
      child: Container(height: 100, width: containerWidth,decoration: BoxDecoration(color:Colors.grey,borderRadius: BorderRadius.circular(5)),),
    );
  }
}
