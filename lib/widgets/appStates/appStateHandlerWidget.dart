import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mediclinic/appEnums.dart';
import 'package:mediclinic/shimmers/shimmerWidget.dart';
import 'package:mediclinic/widgets/appStates/emptyStateWidget.dart';
import 'package:mediclinic/widgets/appStates/errorStateWidget.dart';

import 'notConnectionStateWidget.dart';

class AppStateHandlerWidget extends StatelessWidget {
  final Function onRetry;
  final Widget child;
  final LoadingState state;
  final Widget shimmerLayout;
  AppStateHandlerWidget(
      {this.child, this.state, this.onRetry, this.shimmerLayout});
  @override
  Widget build(BuildContext context) {
    if (state == LoadingState.waiting) {
      return Center(
        child: ShimmerWidget(shimmerLayout: shimmerLayout),
      );
    }
    if (state == LoadingState.error) {
      return ErrorrStateWidget(onRetry: onRetry);
    }
    if (state == LoadingState.empty) {
      return EmptyStateWidget(onRetry: onRetry);
    }
    if (state == LoadingState.noInternet) {
      return NotConnectionStateWidget(onRetry: onRetry);
    }
    return child;
  }
}
