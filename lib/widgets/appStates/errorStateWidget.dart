import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ErrorrStateWidget extends StatelessWidget {
  final Function onRetry;
  ErrorrStateWidget({this.onRetry});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/states/error.png",
              height: 200,
            ),
            SizedBox(
              height: 20,
            ),
            RichText(
              text: TextSpan(
                text: 'Ooops!',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 32),
                children: <TextSpan>[
                  TextSpan(
                      text: ' seems like there is an error',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "We are sorry that you experianced this, please try again later. ",
              style: TextStyle(fontSize: 14),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            Visibility(
              visible: onRetry != null,
              child: RaisedButton(
                onPressed: onRetry,
                child: Text("Retry"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
