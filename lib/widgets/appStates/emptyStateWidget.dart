import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class EmptyStateWidget extends StatelessWidget {
  final Function onRetry;
  EmptyStateWidget({this.onRetry});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/states/notifyEmpty.png",
              height: 200,
            ),
            SizedBox(
              height: 20,
            ),
            RichText(
              text: TextSpan(
                text: 'Huh!',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 32),
                children: <TextSpan>[
                  TextSpan(
                      text: ' all clear for now',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Come back later for more actions =) ",
              style: TextStyle(fontSize: 14),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            Visibility(
              visible: onRetry != null,
              child: RaisedButton(
                onPressed: onRetry,
                child: Text("Retry"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
