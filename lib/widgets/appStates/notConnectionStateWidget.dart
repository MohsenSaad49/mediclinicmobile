import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NotConnectionStateWidget extends StatelessWidget {
  final Function onRetry;
  NotConnectionStateWidget({this.onRetry});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/connection.png",
              height: 200,
            ),
            SizedBox(
              height: 20,
            ),
            RichText(
              text: TextSpan(
                text: 'Ooops!',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 32),
                children: <TextSpan>[
                  TextSpan(
                      text: ' seems like no internet',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Please makke sure that the interet is up and running and everything and the try again",
              style: TextStyle(fontSize: 14),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            Visibility(
              visible: onRetry != null,
              child: RaisedButton(
                onPressed: onRetry,
                child: Text("Retry"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
