import 'package:flutter/material.dart';
import 'package:mediclinic/colorConstants.dart';
import 'package:mediclinic/providers/MainAppProvider.dart';
import 'package:mediclinic/uiHelpers/uiConstants.dart';
import 'package:provider/provider.dart';

class AppBarWidget extends StatefulWidget implements PreferredSizeWidget {
  const AppBarWidget(
      {this.titleText,
      this.hasBackButton,
      this.isAlternate: false,
      this.backButtonFunction});

  final String titleText;
  final bool hasBackButton;
  final Function backButtonFunction;
  final bool isAlternate;

  @override
  AppBarWidgeteState createState() => AppBarWidgeteState();

  @override
  Size get preferredSize => new Size(300, 60);
}

class AppBarWidgeteState extends State<AppBarWidget> {
  Widget _getIcon() {
    return Consumer<MainAppProvider>(builder: (context, val, _) {
      return IconButton(
        icon: Stack(
          children: <Widget>[
 /*           Icon(Icons.menu,
                color: widget.isAlternate ? Colors.black : Colors.white)*/

          ],
        ),
        onPressed: () {
          Scaffold.of(context).openEndDrawer();
        },
      );
    });
  }

  Widget _getButton() {
    if (Navigator.canPop(context)) {
      return Center(
          child: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: widget.isAlternate ? Colors.black : Colors.white,
              ),
              color: Colors.white,
              onPressed: () {
                if (widget.backButtonFunction != null) {
                  widget.backButtonFunction();
                } else {
                  Navigator.pop(context);
                }
              }));
    } else {
      return Padding(
        padding: const EdgeInsets.fromLTRB(12, 12, 0, 0),
        child: Center(
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(40))),
            child: IconButton(
              icon: Icon(
                Icons.search,
                color: ColorConstants.primaryColor,
                size: 20,
              ),
              color: Colors.white,
              onPressed: () {
                showGeneralDialog(
                  barrierLabel: "Label",
                  barrierDismissible: true,
                  barrierColor: Colors.black.withOpacity(0.5),
                  transitionDuration: Duration(milliseconds: 100),
                  context: context,
                  
                  pageBuilder: (context, anim1, anim2) {
                    return Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: double.infinity,
                        child: Material(
                            color: Colors.transparent,
                            child: new Container()),
                        margin: EdgeInsets.only(bottom: 0, left: 0, right: 0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                  transitionBuilder: (context, anim1, anim2, child) {
                    return SlideTransition(
                      position: Tween(begin: Offset(0, -1), end: Offset(0, 0))
                          .animate(anim1),
                      child: child,
                    );
                  },
                );
              },
            ),
          ),
        ),
      );
    }
  }

  Widget _getBackground() {
    return widget.isAlternate
        ? null
        : Container(decoration: UiConstants.heroBoxDecoration);
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            widget.titleText,
            style: TextStyle(
                color: widget.isAlternate
                    ? ColorConstants.primaryColor
                    : Colors.white,
                fontFamily: UiConstants.segeoRegularFont),
          ),
        ),
        backgroundColor: widget.isAlternate ? Colors.white : Colors.transparent,
        brightness: widget.isAlternate ? Brightness.light : Brightness.dark,
        flexibleSpace: _getBackground(),
        elevation: widget.isAlternate ? 5 : 0,
        leading: _getButton(),
        centerTitle: true,
        actions: <Widget>[_getIcon()]);
  }
}
