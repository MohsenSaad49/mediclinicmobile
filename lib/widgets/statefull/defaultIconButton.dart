import 'package:flutter/material.dart';
import 'package:mediclinic/UiConstants.dart';
import 'package:mediclinic/colorConstants.dart';

class DefaultIconButton extends StatelessWidget {
  const DefaultIconButton({
    this.iconWidget,
    this.labelText,
    this.onPressed,
  });

  final Widget iconWidget;
  final String labelText;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Column(
        children: <Widget>[
          Container(

            padding: EdgeInsets.fromLTRB(14, 10, 14, 10),
            decoration:BoxDecoration(
              color: ColorConstants.mainContainersColor,
              border: Border.all(
                color: Colors.transparent,
                width: 0,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: iconWidget,
          ),
          Padding(
            padding: EdgeInsets.only(top: 3,left: 3),
            child: Text(
              labelText,
              style: TextStyle(
                fontFamily: UiConstants.segeoLightFont,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
